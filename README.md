
# Structured Pruning and Quantization for Learned Image Compression

## Overview

This repository contains the official PyTorch implementation of the ICIP 2024 paper "Structured Pruning and Quantization for Learned Image Compression" - Md Adnan Faisal Hossain, Fengqing Zhu.


## Evaluation

Pretrained model checkpoints can be downloaded from [checkpoints](https://drive.google.com/drive/folders/1h-XdjTjVj6jsw5uNqPmC75Q-SCxJowMG?usp=sharing).




