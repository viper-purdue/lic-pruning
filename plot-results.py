import json
import os
import matplotlib.pyplot as plt
from pathlib import Path
import argparse
# Importing libraries
import numpy as np
import math
import re
from utils import bd_rate

# from delta_accuracy import bd_accuracy


default_font = {
    'size': 22,
}

def plot(stat, color, marker='.', linestyle='-', ax=None):
    x = stat['bpp']
    # y = [f*100 for f in stat['PSNR']]
    y = stat['PSNR']
    label = stat['name']
    module = ax or plt
    p = module.plot(x, y, label=label, linestyle=linestyle,
        marker=marker, markersize=10, linewidth=1.6, color=color
    )
    return p


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--results_dir',  type=str, default='results')
    args = parser.parse_args()

    fig1, ax = plt.subplots(figsize=(8,8))

    results_dir = Path(args.results_dir)
    all_methods_results = []
    for fpath in results_dir.rglob('*.json'):
        # print(fpath)
        with open(fpath, 'r') as f:
            results = json.load(f)
        results['name'] = fpath.stem
        print(results['name'])
        if results['name'] == "mean_scale_hyperprior_kodak":
        # if results['name'] == "cheng_anchor_entropy_bpp_kodak":
        # if results['name'] == "mean_scale_hyperprior_entropy_bpp_kodak":
        # if results['name'] == "mean_scale_hyperprior_full_entropy_bpp_kodak":
        # if results['name'] == "mean_scale_hyperprior_kodak":
        # if results['name'] == "scale_hyperprior_kodak":
        # if results['name'] == "scale_hyperprior_entropy_bpp_kodak":
        # if results['name'] == "scale_hyperprior_clic":
        # if results['name'] == "scale_hyperprior_tecnick":
        # if results['name'] == "mean_scale_hyperprior_clic":
        # if results['name'] == "mean_scale_hyperprior_tecnick":
        # if results['name'] == "cheng_anchor_entropy_bpp_tecnick":
        # if results['name'] == "cheng_anchor_entropy_bpp_clic":
        # if results['name'] == "ScaleHyperprior(fp32)":
            baseline_rate = results['bpp']
            baseline_psnr = results['PSNR']
        all_methods_results.append(results)
    all_methods_results = sorted(all_methods_results, key=lambda d: d['name'])

    colors = ['blue', 'red', 'brown', 'orange', 'green', 'cyan', 'indigo', 'b', 'b', 'b', 'w', 'b', 'c', 'y', 'c', 'k', 'w', 'b', 'c', 'y', 'c', 'w', 'b', 'c', 'y', 'c', 'w', 'b', 'c', 'y', 'c']
    markers = ['.', '.', '.', '.', '.', '.', '.', '.', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.', 'v', 's', '.']
    styles = ['-', '-', '-', '-', '-', '-', '-', '-', '-', '--', '--', '-', '--', '--', '-', '--', '--', '-', '--', '--', '-', '--', '--', '-', '--', '--', '-']
    i = 0
    for results in all_methods_results:
        BD_RATE = bd_rate(baseline_rate, baseline_psnr, results['bpp'], results['PSNR'])
        # BD_RATE = bd_rate(results['bpp'], results['PSNR'], baseline_rate, baseline_psnr)
        print(f"{results['name']}: BD-rate = {BD_RATE}")
        plot(results, marker=markers[i], linestyle=styles[i], color=colors[i], ax=ax)
        i+=1


    plt.title('Rate-Distortion trade-off on KODAK', fontdict=default_font)
    plt.grid(True, alpha=0.32)
    handles, labels = plt.gca().get_legend_handles_labels()
    order = [0, 1, 2, 3]
    # # order = [5, 0, 1, 2, 3, 4]
    # #add legend to plot
    # plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order]) 
    # labels = ["CHIP 39.312% param. pruned - extra data", "CHIP 39.252% param. pruned - last layer unpruned", "CHIP 39.312% param. pruned", "L1/L2 Norm 39.30% param. pruned", "Uncompressed Scale Hyperprior Model"]
    # labels = ["Cheng Anchor 2020 (fp32)", "Cheng Anchor 2020 (FPQ-int8)", "Cheng Anchor 2020 (FMPQ)"]
    # labels = ["Full-precision model", r"FMPQ ($CR_{Target} = 0.94$)", r"FMPQ ($CR_{Target} = 0.75$)", r"FMPQ ($CR_{Target} = 1.0$)", r"FMPQ ($CR_{Target} = 0.60$)"]
    # labels = ["Mean Scale Hyperprior (FPQ-int8)", "Mean Scale Hyperprior (FMPQ)", "Mean Scale Hyperprior (fp32)"]
    labels = ["Mean Scale Hyperprior (full-precision)", "Mean Scale Hyperprior (pruned-20% + Quantized-8_bits)", "Mean Scale Hyperprior (pruned-35% + Quantized-8_bits)", "Mean Scale Hyperprior (pruned-80%)"]
    plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order], loc='lower right', prop={'size': 13})
    
    # # plt.legend(["19.93% param. pruned", "29.40% param. pruned", "39.30% param. pruned", "49.33% param. pruned", "59.36% param. pruned", "uncompressed"])
    # plt.legend(loc='lower right', prop={'size': 11})
    plt.xlabel('Bits per pixel (bpp)', fontdict=default_font)
    plt.ylabel('PSNR (dB)', fontdict=default_font)
    x_ticks = [(i) / 10 for i in range(0, 9, 1)]
    # x_ticks = [(i) / 10 for i in range(0, 11, 1)]
    plt.xticks(x_ticks)
    y_ticks = [i for i in range(24, 37, 1)]
    # y_ticks = [i for i in range(19, 38, 1)]
    plt.yticks(y_ticks)
    # plt.xlim(min(x_ticks), max(x_ticks))
    # plt.ylim(min(y_ticks), max(y_ticks))
    plt.xlim(0.08, 0.8)
    plt.ylim(26.5, 34.5)
    plt.tight_layout()
    plt.savefig("images_ICIP/paper.jpg")
    # plt.savefig("images/Exp4.jpg")
    plt.show(block=True)


if __name__ == '__main__':
    main()
