import torch
import torch.nn as nn

from .base import BaseQuantizer

# range_shape = self.get_reshape_range(x)
#         s_scale = s_scale.reshape(range_shape)
#         # print(s_scale)
        
#         # s_zero_point = grad_scale(self.zero_point, s_grad_scale)
#         s_zero_point = self.zero_point
#         s_zero_point = s_zero_point.reshape(range_shape)
#         if use_bias:
#             x = x / s_scale + s_zero_point
#         else:
#             self.zero_point.detach()
#         # x = x / s_scale
#         # print(x.shape)
#         # x = round_pass(x)
#         # print(x.shape)
#         if STE:
#             x = round_pass(x) # + s_zero_point
#             x = torch.clamp(x, self.bit_type.lower_bound, self.bit_type.upper_bound)
#             # x = round_pass(x)
#         else:
#             x = noise_pass(x)
#         # print(len(torch.unique(x)))
#         # print(x.shape)
#         if use_bias:
#             output = (x - s_zero_point) * s_scale
#         else:
#             output = x * s_scale
def box_grad(x, low, high):
    mask = (x >= low) & (x <= high)
    out = torch.zeros_like(x)
    out[mask] = 1
    
    return out

def box_grad_weights(x, low, high):
    mask = (x >= low) & (x <= high)
    out = torch.ones_like(x) * 0.01
    out[mask] = 1
    
    return out
    # out = torch.zeros(len(x.reshape(-1)))
    # for i in range(x.reshape(-1)):
    #     if low <= x[i] <= high:
    #         out[i] = 1
    #     else:
    #         out[i] = 0
    # if low <= x <= high:
    #     return 1
    # else:
    #     return 0   

class ScaledRound(torch.autograd.Function):
    """
    x_in: continuous inputs within the range of [0,1]
    num_levels: number of discrete levels
    scaling_factor: backward scaling factor
    x_out: discretized version of x_in within the range of [0,1]
    """
    @staticmethod
    def forward(ctx, x: torch.Tensor, scale: torch.Tensor, zp: torch.Tensor, lr, Qn, Qp):
        # out = x.round()
        ctx._lr = lr
        ctx._Qp = Qp
        ctx._Qn = Qn
        
        x_hat = x/scale + zp
        out = (torch.clamp(x_hat, Qn, Qp).round() - zp) * scale        
        ctx.save_for_backward(x, x_hat, scale, zp, out)
        # x = x_in * (num_levels - 1)
        # x = torch.round(x)
        # x_out = x / (num_levels - 1)
        
        # ctx._scaling_factor = scaling_factor
        # ctx.save_for_backward(x_in-x_out)
        return out
    
    @staticmethod
    def backward(ctx, g):
        lr = ctx._lr
        Qp = ctx._Qp 
        Qn = ctx._Qn
        x, x_hat, scale, zp, out = ctx.saved_tensors
        clip_grad = box_grad(x_hat, Qn, Qp)
        clip_grad_weights = box_grad_weights(x_hat, Qn, Qp)
        # e = (out - x) / lr
        # dx = g * (1 - e/g) * clip_grad
        dx = g * clip_grad_weights #(g - e) * clip_grad
        dscale = g * (torch.clamp(x_hat, Qn, Qp).round() - clip_grad*(x/scale) - zp)
        dzp = g * (scale*clip_grad - scale)
        # diff = ctx.saved_tensors[0]
        # delta = ctx._scaling_factor
        # scale = 1 + delta * torch.sign(g)*diff
        return dx, dscale, dzp, None, None, None

def grad_scale(x, scale):
    y = x
    y_grad = x * scale
    return (y - y_grad).detach() + y_grad

def round_pass(x):
    y = x.round()
    y_grad = x
    return (y - y_grad).detach() + y_grad

def round_ste(x: torch.Tensor):
    """
    Implement Straight-Through Estimator for rounding operation.
    """
    return (x.round() - x).detach() + x

def noise_pass(x):
    half = float(0.5)
    noise = torch.empty_like(x).uniform_(-half, half)
    x = x + noise
    return x

class ScaledLsqQuantizer(BaseQuantizer):
    
    def __init__(self, bit_type, observer, if_tconv, is_act):
        super(ScaledLsqQuantizer, self).__init__(bit_type, observer, if_tconv, is_act)
        self.scale = nn.Parameter(torch.ones((1)))
        self.zero_point = nn.Parameter(torch.ones((1)))
    
    def init_from(self, x):
        # print(x.shape)
        range_shape = self.get_reshape_range(x)
        x = x.detach()
        if self.observer.calibration_mode == "channel_wise":
            if self.if_tconv:
                x = x.permute(1, 0, 2, 3)
            temp_scale = x.abs().mean(dim=list(range(1, x.dim())), keepdim=True) * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
            # flag = 0
            # print(self.scale.shape)
        else:
            if self.if_tconv:
                x = x.permute(1, 0, 2, 3)
            temp_scale = x.abs().mean() * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
            # flag = 1
        self.zero_point = nn.Parameter(torch.zeros_like(temp_scale))
        # if self.scale.shape[0] > 1:
        #     print(scale)
        
    def update_quantization_params(self, *args, **kwargs):
        scale, zero_point = self.observer.get_quantization_params(
            *args, **kwargs)
        self.scale = nn.Parameter(scale)
        self.zero_point = nn.Parameter(zero_point)

    def quant(self, x, lr, scale=None, zero_point=None):
        s_grad_scale = 1.0 / ((self.bit_type.upper_bound * x.numel()) ** 0.5)
        # s_scale = grad_scale(self.scale, s_grad_scale)
        s_scale = self.scale
        range_shape = self.get_reshape_range(x)
        s_scale = s_scale.reshape(range_shape)
        # print(s_scale)
        
        # s_zero_point = grad_scale(self.zero_point, s_grad_scale)
        s_zero_point = self.zero_point
        s_zero_point = s_zero_point.reshape(range_shape)
        
        output = ScaledRound.apply(x, s_scale, s_zero_point, lr, self.bit_type.lower_bound, self.bit_type.upper_bound)
        
        # if use_bias:
        #     x = x / s_scale + s_zero_point
        # else:
        #     self.zero_point.detach()
        #     x = x / s_scale
        # # print(x.shape)
        # # x = round_pass(x)
        # # print(x.shape)
        # if STE:
        #     x = round_pass(x) # + s_zero_point
        #     x = torch.clamp(x, self.bit_type.lower_bound, self.bit_type.upper_bound)
        #     # x = round_pass(x)
        # else:
        #     x = noise_pass(x)
        # # print(len(torch.unique(x)))
        # # print(x.shape)
        # if use_bias:
        #     output = (x - s_zero_point) * s_scale
        # else:
        #     output = x * s_scale
            
        # range_shape = self.get_reshape_range(x)
        # scale = self.scale.reshape(range_shape)
        # zero_point = self.zero_point.reshape(range_shape)
        # scale = self.scale
        # zero_point = self.zero_point
        # n_levels = 2 ** 8
            
        # x_int = round_ste(x / scale) + zero_point
        # x_quant = torch.clamp(x_int, 0, n_levels - 1)
        # output = (x_quant - zero_point) * scale
        
        return output

    def dequantize(self, x, scale=None, zero_point=None):
        output = x
        return output
    
    def forward(self, inputs, lr):
        # print("BEFORE", inputs.shape)
        outputs = self.quant(inputs, lr)
        outputs = self.dequantize(outputs)
        # print("AFTER", outputs.shape)
        return outputs