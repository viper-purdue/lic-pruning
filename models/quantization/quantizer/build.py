# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
from .log2 import Log2Quantizer
from .uniform import UniformQuantizer
from .lsq import LsqQuantizer
from .adaround import AdaRoundQuantizer
from .quant_noise_lsq import QuantNoiseLsqQuantizer
from .scaled_lsq import ScaledLsqQuantizer

str2quantizer = {'uniform': UniformQuantizer, 'log2': Log2Quantizer, 'lsq': LsqQuantizer, 'adaround': AdaRoundQuantizer, 'quant-noise-lsq': QuantNoiseLsqQuantizer, 'scaled-lsq': ScaledLsqQuantizer}


def build_quantizer(quantizer_type, bit_type, observer, if_tconv, is_act=False, lr=None):
    quantizer = str2quantizer[quantizer_type]
    # if quantizer_type == 'scaled-lsq':
    #     return quantizer(bit_type, observer, if_tconv, is_act, lr)
    # else:
    return quantizer(bit_type, observer, if_tconv, is_act)
