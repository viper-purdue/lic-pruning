import time
import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F


def Handle_Parameter(param, b_w=8):
    
    eps = torch.tensor(1e-6, dtype=torch.float32)
    bit_range = 2 ** b_w - 1
    zero_point = param.min()
    param_new = param - zero_point
        
    range_float = param_new.abs().max()
    range_int01 = range_float
    range_int01 = torch.max(range_float, eps)

    param_01 = torch.clamp(param_new / range_int01, -1, 1)
    param_int = torch.round(param_01 * bit_range)
    param_fixed = (param_int / bit_range) * range_int01 + zero_point

    return param_fixed      

        
def ActQuantizer(x: torch.Tensor, b_w=8):
    x_clone = x.clone().detach()

    if len(x_clone.shape) == 4: 
        for i in range(x_clone.shape[1]):
            x_clone[:,i,:,:] = Handle_Parameter(x_clone[:,i,:,:], b_w)

    elif len(x_clone.shape) == 3:
        for i in range(x_clone.shape[2]):
            x_clone[:,:,i] = Handle_Parameter(x_clone[:,:,i], b_w)

    elif len(x.shape) == 2:
        for i in range(x_clone.shape[1]):
            x_clone[:,i] = Handle_Parameter(x_clone[:,i], b_w)

    else: 
        x_clone = Handle_Parameter(x_clone)
    # x_clone = Handle_Parameter(x_clone)
    return x_clone

