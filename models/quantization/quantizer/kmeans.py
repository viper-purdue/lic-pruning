import os
import random
import time
import torch
from matplotlib import pyplot as plt
from pykeops.torch import LazyTensor
from tqdm import tqdm
from collections import Counter

import torch
import torch.nn as nn
import torch.nn.functional as F

# use_cuda = torch.cuda.is_available()
# dtype = torch.float32 if use_cuda else torch.float64
# device_id = "cuda:0" if use_cuda else "cpu"

# class UniformQuantizer(BaseQuantizer):

#     def __init__(self, bit_type, observer, module_type):
#         super(UniformQuantizer, self).__init__(bit_type, observer, module_type)
#         self.scale = None
#         self.zero_point = None


def KMeans(x, K=10, Niter=10, verbose=True):
    """Implements Lloyd's algorithm for the Euclidean metric."""

    orig = x.clone()
    device = x.get_device()
    start = time.time()
    N, D = x.shape  # Number of samples, dimension of the ambient space

    K = min(K, N)
    c = x[:K, :].clone()  # Simplistic initialization for the centroids

    x_i = LazyTensor(x.view(N, 1, D))  # (N, 1, D) samples
    c_j = LazyTensor(c.view(1, K, D))  # (1, K, D) centroids

    # K-means loop:
    # - x  is the (N, D) point cloud,
    # - cl is the (N,) vector of class labels
    # - c  is the (K, D) cloud of cluster centroids
    # for i in tqdm(range(Niter), desc="K-Means Clustering:"):
    for i in range(Niter):
        print(f"Iteration: {i}")
        # E step: assign points to the closest cluster -------------------------
        D_ij = ((x_i - c_j) ** 2).sum(-1)  # (N, K) symbolic squared distances
        cl = D_ij.argmin(dim=1).long().view(-1)  # Points -> Nearest cluster

        # M step: update the centroids to the normalized cluster average: ------
        # Compute the sum of points per cluster:
        c.zero_()
        c.scatter_add_(0, cl[:, None].repeat(1, D), x)

        # Divide by the number of points per cluster:
        Ncl = torch.bincount(cl, minlength=K).type_as(c).view(K, 1)
        c /= Ncl  # in-place division to compute the average
        
        # if i % 10 == 0:
        #     out = torch.gather(c, 0, cl.view(N,1).repeat(1,D))
        #     MSE = F.mse_loss(orig, out)
        #     print(f"MSE loss after iteration {i}: {MSE:.8f}")

    if verbose:  # Fancy display -----------------------------------------------
        if device != -1:
            torch.cuda.synchronize(device=device)
        end = time.time()
        print(
            f"K-means for the Euclidean metric with {N:,} points in dimension {D:,}, K = {K:,}:"
        )
        print(
            "Timing for {} iterations: {:.5f}s = {} x {:.5f}s\n".format(
                Niter, end - start, Niter, (end - start) / Niter
            )
        )

    return cl, c


class ComputeDistances(nn.Module):
    """ Computes distances as described in the file em.py using the map/reduce paradigm.

    Args:
        - M: weight matrix
        - centroids: centroids used to compute the distance

    Remarks:
        - We split the input activations into up to 8 GPUs by relying on DataParallel.
        - The computation of distances is done per GPU with its chunk of activations (map)
        - The distances are then aggregated (reduce)

    """
    def __init__(self, M, centroids):
        super(ComputeDistances, self).__init__()
        self.distances = Distances(M, centroids)
        # self.reduce = Reduce()

    def forward(self, in_activations):
        return self.distances(in_activations)

    def update_centroids(self, centroids):
        self.distances.centroids.data = centroids


class Distances(nn.Module):
    """
    Computes distances using broadcasting (map step). This layer automatically chunks the
    centroids and the weight matrix M so that the computation fits into the GPU

    Remarks:
        - The dimensions of the centroids and the weight matrix must be "chunkable enough" since
          we divide them by two until ths computation fits on the GPU
        - For debuging purposes, we advise the programmer to use only one GPU by setting
          CUDA_VISIBLE_DEVICES=1
    """

    def __init__(self,  M, centroids):
        super(Distances, self).__init__()
        self.M = nn.Parameter(M.t(), requires_grad=False)                       
        self.centroids = nn.Parameter(centroids, requires_grad=False)       # (K, D)

    def forward(self, in_activations):
        # distances = torch.cat([
        #                     torch.cat([
        #                         torch.matmul(
        #                             in_activations[None, :, :],
        #                             M_c[None, :, :] - centroids_c[:, :, None]
        #                         ).norm(p=2, dim=1).pow(2)
        #                         for centroids_c in self.centroids.chunk(nb_centroids_chunks, dim=0)
        #                     ], dim=0)
        #                     for M_c in self.M.chunk(nb_M_chunks, dim=1)
        #                 ], dim=1)
        in_features, out_features = self.M.size()
        # print(in_activations.shape)
        # print(in_features)
        # indices = torch.randint(low=0, high=in_features*out_features, size=(K,)).long()
        # self.centroids = self.M.reshape((-1, 1)) [indices, :]
        
        # if in_features * out_features >= 100000000000000000:
        # distances = torch.zeros((self.centroids.shape[0], self.M.shape[0]*self.M.shape[1]))       
        # # print(in_activations.shape)
        # # print(self.M.shape)
        # for i in tqdm(range(self.M.shape[0]), desc="Calculating distances"):
        #     for j in range(self.M.shape[1]):
        #         param = self.M[i, j]
        #         x_hat = in_activations[:, i]
        #         for k, centroid in enumerate(self.centroids.reshape(-1)):
        #             distances[k, i*self.M.shape[1]+j] = (x_hat*(centroid - param)).norm(p=2).pow(2)
        # else:
        # vectorized method
        # in_activations = in_activations.reshape(in_activations.shape[0], in_features, 1, 1)
        # M = M.reshape(1, in_features, out_features, 1)
        # centroids = centroids.reshape(1, 1, 1, -1)
        # distances = in_activations * (M - centroids) # (B, in_features, out_features, K)
        # distances = distances.pow(2).sum(0) # (in_features, out_features, K)
        # # to the shape of (K, in_features*out_features)
        # distances = distances.flatten(0, 1).transpose(0, 1)
        
        # vectorized method
        distance_list = []
        in_activations = in_activations.reshape(in_activations.shape[0], in_features, 1)
        M = self.M.reshape(1, in_features, out_features)
        # centroids   # # (K, 1)
        for k in tqdm(range(self.centroids.shape[0]), desc="Calculating distances"):
            centroid = self.centroids[k,:].reshape(1, 1, 1)
            distances = in_activations * (M - centroid) # (B, in_features, out_features, 1)
            distances = distances.pow(2).sum(0) # (in_features, out_features, 1)
            distance_list.append(distances)
        distances = torch.stack(distance_list, dim=-1)  # # (in_features, out_features, K)
        # to the shape of (K, in_features*out_features)
        distances = distances.flatten(0, 1).transpose(0, 1)
        
        # in_activations = in_activations[:,:,None,None]
        # M = self.M[None,:,:,None]
        # centroids = self.centroids[None,None,None,:]
        # distances = in_activations * (M - centroids) # (B, in_features, out_features, K)
        # distances = distances.pow(2).sum(0) # (in_features, out_features, K)
        # # to the shape of (K, in_features*out_features)
        # distances = distances.flatten(0, 1).transpose(0, 1)
        
        # in_activations = in_activations.view(in_activations.shape[0], in_features, 1, 1)
        # M = self.M.view(1, in_features, out_features, 1)
        # centroids = self.centroids.view(1, 1, 1, -1)
        # distances = in_activations * (M - centroids) # (B, in_features, out_features, K)
        # distances = distances.pow(2).sum(0) # (in_features, out_features, K)
        # # to the shape of (K, in_features*out_features)
        # distances = distances.flatten(0, 1).transpose(0, 1)
        
        # in_activations = in_activations[:, None, None, :]  # (B, 1, 1, in_features)
        # M = self.M[None, None, :, :]  # (1, 1, out_features, K)
        # centroids = self.centroids[None, None, None, :]

        # # Calculate distance term
        # distance_term = in_activations * (M - centroids)

        # # Calculate squared L2 norm and sum along the appropriate dimensions
        # distances = distance_term.pow(2).sum(dim=1)  # (B, out_features, K)
        
        # # Flatten and transpose distances
        # distances = distances.view(-1, distances.size(-1)).t()

        
        # for k, centroid in enumerate(self.centroids.reshape(-1)):
        #     centroid_diff = centroid - self.M
        #     distance_terms = in_activations * centroid_diff
        #     distances[k, :] = torch.sum(distance_terms**2, dim=1)
        # Initialize distances tensor
        # distances = torch.zeros((self.centroids.numel(), self.M.numel()))

        # # Calculate distances using vectorized operations
        # for k, centroid in enumerate(self.centroids.reshape(-1)):
        #     centroid_diff = centroid - self.M
        #     distance_terms = in_activations[:,:,None] * centroid_diff[None,:,:]
        #     distances[k, :] = distance_terms.norm(p=2, dim=1).pow(2)
            # distances[k, :] = torch.sum(distance_terms ** 2, dim=1)
        
        # x_hats = in_activations.unsqueeze(2)                                        # Add a singleton dimension
        # expanded_centroids = self.centroids.reshape(-1, 1, 1, out_features)         # Expand centroids for efficient computation
        # expanded_M = self.M.unsqueeze(0).unsqueeze(3)                               # Expand weight matrix for efficient computation
        # centroid_weight_diff = x_hats * (expanded_centroids - expanded_M)           # Calculate the weighted centroid difference efficiently
        # squared_L2_norms = centroid_weight_diff.norm(p=2, dim=3).pow(2)             # Calculate squared L2 norms along dimension 3
        # distances = squared_L2_norms.view(K, in_features * out_features).t()        # Reshape and transpose to get the final distances tensor
                
        return distances  # (K x in_features*out_features)


def reshape_weight(weight):
    """
    C_out x C_in x k x k -> (C_in x k x k) x C_out.
    """

    if len(weight.size()) == 4:
        C_out, C_in, k, k = weight.size()
        return weight.view(C_out, C_in * k * k).t()
    else:
        return weight.t()


def reshape_activations(activations, k=3, stride=(1, 1), padding=(1, 1), groups=1):
    """
    N x C_in x H x W -> (N x H x W) x C_in.
    """

    if len(activations.size()) == 4:
        # gather activations
        a_padded = F.pad(activations, (padding[1], padding[1], padding[0], padding[0]))
        N, C, H, W = a_padded.size()
        # print(N, C, H, W)
        a_stacked = []

        for i in range(0, H - k + 1, stride[0]):
            for j in range(0, W - k + 1, stride[1]):
                a_stacked.append(a_padded[:, :, i:i + k, j:j + k])

        # reshape according to weight
        # print(torch.cat(a_stacked, dim=0).shape)
        # a_reshaped = reshape_weight(torch.cat(a_stacked, dim=0))
        a_reshaped = reshape_weight(torch.cat(a_stacked, dim=0)).t()

        # group convolutions (e.g. depthwise convolutions)
        a_reshaped_groups = torch.cat(a_reshaped.chunk(groups, dim=1), dim=0)

        return a_reshaped_groups

    else:
        return activations


def KMeans_activations(x_in, W, module_type, K=10, Niter=10, conv_properties=None, verbose=True):
    """Implements Lloyd's algorithm for the Euclidean metric.

    Args:
        x_in (tensor): input tensor (Batch, in_features)
        W (tensor): weight tensor (in_features, out_features)
        K (int, optional): number of centroids. Defaults to 10.
        Niter (int, optional): number of iterations of KMeans. Defaults to 10.
        verbose (bool, optional): Defaults to True.

    Returns:
        _type_: _description_
    """    

    if module_type == 'conv_weight':
        W = reshape_weight(W).t()
        k, s, pad, groups = conv_properties["kernel_size"], conv_properties["stride"], conv_properties["padding"], conv_properties["groups"]
        x_in = reshape_activations(x_in, k=k, stride=(s, s), padding=(pad, pad), groups=groups)
    
    # orig = W.clone()
    device = W.get_device()
    start = time.time()
    # N, D = x.shape  # Number of samples, dimension of the ambient space
    
    # Initialize centroids
    eps=1e-2
    in_features, out_features = W.size()
    N = in_features*out_features
    K = min(K, N//4)
    indices = torch.randint(low=0, high=in_features*out_features, size=(K,)).long()
    centroids = W.reshape((-1, 1)) [indices, :]     # (K, 1)
    # print("YESSS", centroids[0, 0].shape)
    # print("YESSS", centroids[0, :].shape)
    
    print("\n")
    print(f"SHAPE OF WEIGHT: {W.shape}")
    print(f"SHAPE OF ACTIVATION: {x_in.shape}")
    for i in range(Niter):
        print(f"Iteration: {i}")
        compute_distances_parallel = ComputeDistances(W, centroids)
        # compute_distances_parallel.update_centroids(centroids)
        distances = compute_distances_parallel(x_in)        # (K x in_features*out_features)
        assignments = torch.argmin(distances, dim=0)        # (in_features*out_features)
        
        # empty clusters
        counts = Counter(map(lambda x: x.item(), assignments))
        empty_clusters = set(range(K)) - set(counts.keys())
        n_empty_clusters = len(empty_clusters)

        i = 0
        while len(empty_clusters) > 0:
            i+=1
            # given an empty cluster, find most populated cluster and split it into two
            k = random.choice(list(empty_clusters))
            m = counts.most_common(1)[0][0]
            e = torch.randn_like(centroids[m])*eps
            centroids[k] = centroids[m].clone()
            centroids[k] += e
            centroids[m] -= e
            print(k, len(empty_clusters), "###########")

            # recompute assignments
            distances = compute_distances_parallel(x_in)        # (K x in_features*out_features)
            assignments = torch.argmin(distances, dim=0)

            # check for empty clusters
            counts = Counter(map(lambda x: x.item(), assignments))
            empty_clusters = set(range(K)) - set(counts.keys())
            if i >= 50:
                exit()
        
        # centroids (M-step)
        for k in range(K):
            W_k = W.reshape((-1))[assignments==k]
            x_in_k = x_in.reshape((-1, 1))
            # print(torch.pinverse(x_in_k).shape)
            # print(x_in_k.shape)
            # print(torch.matmul(torch.pinverse(x_in_k), x_in_k).shape)
            # print(W_k.sum().shape)
            centroids[k,:] = (torch.matmul(torch.pinverse(x_in_k), x_in_k) * W_k.sum() / W_k.size(0)).reshape(-1)

        # book-keeping
        # n_samples_eval = 128
        # in_activations_eval = in_activations_eval[:n_samples_eval]
        # normalize = np.sqrt(n_samples_eval * len(self.assignments))  # np.sqrt(out_activations.numel())
        # obj = (in_activations_eval.mm(self.centroids[self.assignments].t() - M)).norm(p=2).div(normalize).item()  # (n_samples x in_features).mm((out_features x in_features).t()) -> (n_samples x out_features) -> 1
        # self.objective.append(obj)
        # if self.verbose: print("Iteration: {},\t objective: {:.6f},\t resolved empty clusters: {}".format(i, obj, n_empty_clusters))
        
    c = centroids.reshape(-1, 1).to(device)
    cl = assignments.reshape(-1).to(device)
    
    

    # K = min(K, N)
    # c = x[:K, :].clone()  # Simplistic initialization for the centroids

    # x_i = LazyTensor(x.view(N, 1, D))  # (N, 1, D) samples
    # c_j = LazyTensor(c.view(1, K, D))  # (1, K, D) centroids

    # # K-means loop:
    # # - x  is the (N, D) point cloud,
    # # - cl is the (N,) vector of class labels
    # # - c  is the (K, D) cloud of cluster centroids
    # # for i in tqdm(range(Niter), desc="K-Means Clustering:"):
    # for i in range(Niter):
    #     # E step: assign points to the closest cluster -------------------------
    #     D_ij = ((x_i - c_j) ** 2).sum(-1)  # (N, K) symbolic squared distances
    #     cl = D_ij.argmin(dim=1).long().view(-1)  # Points -> Nearest cluster

    #     # M step: update the centroids to the normalized cluster average: ------
    #     # Compute the sum of points per cluster:
    #     c.zero_()
    #     c.scatter_add_(0, cl[:, None].repeat(1, D), x)

    #     # Divide by the number of points per cluster:
    #     Ncl = torch.bincount(cl, minlength=K).type_as(c).view(K, 1)
    #     c /= Ncl  # in-place division to compute the average
        
    #     # if i % 10 == 0:
    #     #     out = torch.gather(c, 0, cl.view(N,1).repeat(1,D))
    #     #     MSE = F.mse_loss(orig, out)
    #     #     print(f"MSE loss after iteration {i}: {MSE:.8f}")

    D = 1
    if verbose:  # Fancy display -----------------------------------------------
        if device != -1:
            torch.cuda.synchronize(device=device)
        end = time.time()
        print(
            f"K-means for the Euclidean metric with {N:,} points in dimension {D:,}, K = {K:,}:"
        )
        print(
            "Timing for {} iterations: {:.5f}s = {} x {:.5f}s\n".format(
                Niter, end - start, Niter, (end - start) / Niter
            )
        )

    return cl, c