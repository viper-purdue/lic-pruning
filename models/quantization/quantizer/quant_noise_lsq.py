import torch
import torch.nn as nn

from .base import BaseQuantizer


# def grad_scale(x, scale):
#     y = x
#     y_grad = x * scale
#     return (y - y_grad).detach() + y_grad

# def round_pass(x):
#     y = x.round()
#     y_grad = x
#     return (y - y_grad).detach() + y_grad

class QuantNoiseLsqQuantizer(BaseQuantizer):
    
    def __init__(self, bit_type, observer, module_type):
        super(QuantNoiseLsqQuantizer, self).__init__(bit_type, observer, module_type)
        self.scale = nn.Parameter(torch.ones((1)))
        self.zero_point = nn.Parameter(torch.ones((1)))
    
    def init_from(self, x):
        # print(x.shape)
        range_shape = self.get_reshape_range(x)
        if self.observer.calibration_mode == "channel_wise":
            temp_scale = x.detach().abs().mean(dim=list(range(1, x.dim())), keepdim=True) * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
            # flag = 0
            # print(self.scale.shape)
        else:
            temp_scale = x.detach().abs().mean() * 2 / (self.bit_type.upper_bound ** 0.5)
            self.scale = nn.Parameter(temp_scale.reshape(range_shape))
            # flag = 1
        self.zero_point = nn.Parameter(torch.zeros_like(temp_scale))
        # if self.scale.shape[0] > 1:
        #     print(scale)
        
    def update_quantization_params(self, *args, **kwargs):
        scale, zero_point = self.observer.get_quantization_params(
            *args, **kwargs)
        self.scale = nn.Parameter(scale)
        self.zero_point = nn.Parameter(zero_point)

    def quant(self, x, p=0.5, scale=None, zero_point=None, use_bias=True):
        # print(self.module_type)
        # s_grad_scale = 1.0 / ((self.bit_type.upper_bound * x.numel()) ** 0.5)
        # s_scale = grad_scale(self.scale, s_grad_scale)
        range_shape = self.get_reshape_range(x)
        s_scale = self.scale.reshape(range_shape)
        # print(s_scale)
        
        # s_zero_point = grad_scale(self.zero_point, s_grad_scale)
        s_zero_point = self.zero_point.reshape(range_shape)
        
        if use_bias:
            out = x.detach() / s_scale + s_zero_point
        else:
            self.zero_point.detach()
            out = x.detach() / s_scale
        # print(x.shape)
        
        out = out.round()
        out = torch.clamp(out, self.bit_type.lower_bound, self.bit_type.upper_bound)

        # print(x.shape)
        # out = out.round()
        # print(len(torch.unique(x)))
        # print(x.shape)
        if use_bias:
            out = (out - s_zero_point) * s_scale
        else:
            out = out * s_scale
            
        mask = torch.zeros_like(x)
        mask.bernoulli_(1 - p)
        noise = (out - x).masked_fill(mask.bool(), 0)
        
        output = x + noise.detach()
        
        # clamp_low = -self.scale.mean() * self.zero_point.mean()
        # clamp_high = self.scale.mean() * (2**self.bit_type.bits - 1 - self.zero_point.mean())
        # output = (
        #     torch.clamp(x, clamp_low.item(), clamp_high.item())
        #     + noise.detach()
        # )
        
        
        return output

    def dequantize(self, x, scale=None, zero_point=None):
        out = x
        return out