# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
import torch
import torch.nn as nn


class BaseQuantizer(nn.Module):

    def __init__(self, bit_type, observer, if_tconv, is_act):
        super(BaseQuantizer, self).__init__()
        self.bit_type = bit_type
        self.observer = observer
        self.if_tconv = if_tconv
        self.is_act = is_act

    def get_reshape_range(self, inputs):
        range_shape = None
        if self.if_tconv:
            range_shape = (1, -1, 1, 1)
        elif self.is_act:
            if len(inputs.shape) == 2:
                range_shape = (1, -1)
            elif len(inputs.shape) == 3:
                range_shape = (1, 1, -1)
            elif len(inputs.shape) == 4:
                range_shape = (1, -1, 1, 1)
            else:
                raise NotImplementedError
        elif len(inputs.shape) == 4:
            range_shape = (-1, 1, 1, 1)
        elif len(inputs.shape) == 2:
            range_shape = (-1, 1)
        else:
            raise NotImplementedError

        return range_shape

    def update_quantization_params(self, *args, **kwargs):
        pass

    def quant(self, inputs, scale=None, zero_point=None):
        raise NotImplementedError

    def dequantize(self, inputs, scale=None, zero_point=None):
        raise NotImplementedError

    def forward(self, inputs):
        # print("BEFORE", inputs.shape)
        outputs = self.quant(inputs)
        outputs = self.dequantize(outputs)
        # print("AFTER", outputs.shape)
        return outputs
