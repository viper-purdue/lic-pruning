import torch
import torch.nn as nn

from .base import BaseQuantizer


def grad_scale(x, scale):
    y = x
    y_grad = x * scale
    return (y - y_grad).detach() + y_grad

def round_pass(x):
    y = x.round()
    y_grad = x
    return (y - y_grad).detach() + y_grad

class AdaRoundQuantizer(BaseQuantizer):
    
    def __init__(self, bit_type, observer, module_type):
        super(AdaRoundQuantizer, self).__init__(bit_type, observer, module_type)
        # self.scale = nn.Parameter(torch.ones((1)))
        # self.zero_point = nn.Parameter(torch.ones((1)))
        self.alpha = nn.Parameter(torch.ones((1)))
        self.soft_targets = False

        # params for sigmoid function
        self.gamma, self.zeta = -0.1, 1.1
    
    # def init_from(self, x):
    #     # print(x.shape)
    #     # range_shape = self.get_reshape_range(x)
    #     # if self.observer.calibration_mode == "channel_wise":
    #     #     temp_scale = x.detach().abs().mean(dim=list(range(1, x.dim())), keepdim=True) * 2 / (self.bit_type.upper_bound ** 0.5)
    #     #     self.scale = nn.Parameter(temp_scale.reshape(range_shape))
    #     #     # flag = 0
    #     #     # print(self.scale.shape)
    #     # else:
    #     #     temp_scale = x.detach().abs().mean() * 2 / (self.bit_type.upper_bound ** 0.5)
    #     #     self.scale = nn.Parameter(temp_scale.reshape(range_shape))
    #     #     # flag = 1
    #     # self.zero_point = nn.Parameter(torch.zeros_like(temp_scale))
    #     # print(self.module_type)
    #     # print(x.shape)
    #     # print(self.scale.shape)
    #     self.init_alpha(x)
        
    def init_alpha(self, x):
        range_shape = self.get_reshape_range(x)
        temp_scale = self.scale.reshape(range_shape)
        x_floor = torch.floor(x / temp_scale)
        rest = (x / temp_scale) - x_floor  # rest of rounding [0, 1)
        alpha = -torch.log((self.zeta - self.gamma) / (rest - self.gamma) - 1)  # => sigmoid(alpha) = rest
        self.alpha = nn.Parameter(alpha)            # V
    
    def update_quantization_params(self, *args, **kwargs):
        scale, zero_point = self.observer.get_quantization_params(
            *args, **kwargs)
        self.scale = scale
        self.zero_point = zero_point
    
    def rectified_sigmoid(self):
        """generate rounding mask.
        """
        return torch.clamp(torch.sigmoid(self.alpha) * (self.zeta - self.gamma) + self.gamma, 0, 1)

    def quant(self, x, scale=None, zero_point=None, use_bias=True):
        # s_grad_scale = 1.0 / ((self.bit_type.upper_bound * x.numel()) ** 0.5)
        # s_scale = grad_scale(self.scale, s_grad_scale)
        range_shape = self.get_reshape_range(x)
        scale = self.scale.reshape(range_shape)
        
        # s_zero_point = grad_scale(self.zero_point, s_grad_scale)
        zero_point = self.zero_point.reshape(range_shape)
        
        x = torch.floor(x / scale)
        x = x + self.rectified_sigmoid()
        # x = x + (self.alpha >= 0).float()
        
        if use_bias:
            x = x + zero_point
        else:
            self.zero_point.detach()
        x = torch.clamp(x, self.bit_type.lower_bound, self.bit_type.upper_bound)
        # x = round_pass(x)
        if use_bias:
            output = (x - zero_point) * scale
        else:
            output = x * scale
        
        return output

    def dequantize(self, x, scale=None, zero_point=None):
        output = x
        return output