import torch
import torch.nn as nn
import torch.nn.functional as F

from typing import Union

# from compressai.layers.gdn import GDN
from compressai.layers import GDN, MaskedConv2d
# from quantization.quantizer import round_ste, lp_loss, UniformAffineQuantizer

from .observer import build_observer
from .quantizer import build_quantizer, ActQuantizer


class WeightScaler(nn.Module):
    def __init__(self, weight: torch.Tensor, org_module_type: str):
        super().__init__()
        
        self.org_module_type = org_module_type
        
        # if org_module_type == "Conv2d" or org_module_type == "Linear" or org_module_type == "MaskedConv2d":
        #     # max_values = torch.max(weight.reshape(weight.shape[0], -1), dim=1)[0]
        #     min_values = torch.min(weight.reshape(weight.shape[0], -1), dim=1)[0]
        #     alpha_shape = weight.shape[0]
        # elif org_module_type == "ConvTranspose2d":
        #     # max_values = torch.max(weight.reshape(weight.shape[1], -1), dim=1)[0]
        #     min_values = torch.min(weight.reshape(weight.shape[1], -1) , dim=1)[0]
        #     alpha_shape = weight.shape[1]
        
        if org_module_type == "Conv2d" or org_module_type == "MaskedConv2d":
            self.reshape_range = (-1, 1, 1, 1)
            self.alpha_shape = weight.shape[0]
        elif org_module_type == "ConvTranspose2d":
            self.reshape_range = (1, -1, 1, 1)
            self.alpha_shape = weight.shape[1]
        elif org_module_type == "Linear":
            self.reshape_range = (-1, 1)
            self.alpha_shape = weight.shape[0]
            
        # self.alpha = nn.Parameter(2*torch.ones(alpha_shape), requires_grad=False)
        # self.bias = nn.Parameter(torch.Tensor(min_values), requires_grad=False)
        # self.register_buffer('alpha', 2*torch.ones(alpha_shape).reshape(self.reshape_range))
        # self.register_buffer('bias', torch.Tensor(min_values).reshape(self.reshape_range))

    def forward(self, x):
        device = x.device
        if self.org_module_type == "Conv2d" or self.org_module_type == "Linear" or self.org_module_type == "MaskedConv2d":
            # max_values = torch.max(weight.reshape(weight.shape[0], -1), dim=1)[0]
            min_values = torch.min(x.reshape(x.shape[0], -1), dim=1)[0]
        elif self.org_module_type == "ConvTranspose2d":
            # max_values = torch.max(weight.reshape(weight.shape[1], -1), dim=1)[0]
            min_values = torch.min(x.reshape(x.shape[1], -1) , dim=1)[0]
        
        alpha = 3*torch.ones(self.alpha_shape).to(device)
        bias = min_values.to(device)
        
        x = x - bias.reshape(self.reshape_range)
        x = 0.5 * (torch.abs(x) - torch.abs(x - alpha.reshape(self.reshape_range)) + alpha.reshape(self.reshape_range))
        x = x + bias.reshape(self.reshape_range)
        
        # x = x - self.bias
        # x = 0.5 * (torch.abs(x) - torch.abs(x - self.alpha) + self.alpha)
        # x = x + self.bias
        
        return x
    
class StraightThrough(nn.Module):
    def __init__(self, channel_num: int = 1):
        super().__init__()
        
    def forward(self, input):
        return input

def round_ste(x: torch.Tensor):
    """
    Implement Straight-Through Estimator for rounding operation.
    """
    return (x.round() - x).detach() + x


class QuantModule(nn.Module):
    r"""
        Convert module to quantmodule.
    """

    def __init__(self, org_module: Union[nn.Conv2d, nn.ConvTranspose2d, nn.LayerNorm, nn.Linear, GDN, nn.PixelShuffle, MaskedConv2d], weight_quant_params: dict = {},
                 act_quant_params: dict = {},  disable_act_quant: bool = False):
        super(QuantModule, self).__init__()

        self.if_tconv = False
        self.org_module_type = None
        self.scale_weights = weight_quant_params['scale_weights']
        self.train_qe_loss = weight_quant_params['use_qe_loss']
        self.lr = None
        
        if isinstance(org_module, MaskedConv2d):
            # torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=0, 
            # dilation=1, groups=1, bias=True, padding_mode='zeros', device=None, dtype=None)
            self.fwd_kwargs = dict(stride=org_module.stride, padding=org_module.padding,
                                   dilation=org_module.dilation, groups=org_module.groups)
            self.fwd_func = F.conv2d
            self.org_module_type = "MaskedConv2d"
        
        elif isinstance(org_module, nn.Conv2d):
            # torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=0, 
            # dilation=1, groups=1, bias=True, padding_mode='zeros', device=None, dtype=None)
            self.fwd_kwargs = dict(stride=org_module.stride, padding=org_module.padding,
                                   dilation=org_module.dilation, groups=org_module.groups)
            self.fwd_func = F.conv2d
            self.org_module_type = "Conv2d"

        elif isinstance(org_module, nn.ConvTranspose2d):
            # torch.nn.ConvTranspose2d(in_channels, out_channels, kernel_size, stride=1, padding=0, 
            # output_padding=0, groups=1, bias=True, dilation=1, padding_mode='zeros', device=None, dtype=None)
            self.fwd_kwargs = dict(stride=org_module.stride, padding=org_module.padding, 
                                   output_padding=org_module.output_padding,
                                   dilation=org_module.dilation, groups=org_module.groups)
            self.fwd_func = F.conv_transpose2d
            self.if_tconv = True
            self.org_module_type = "ConvTranspose2d"

        elif isinstance(org_module, nn.Linear):
            # torch.nn.Linear(in_features, out_features, bias=True, device=None, dtype=None)
            self.fwd_kwargs = dict()
            self.fwd_func = F.linear
            self.org_module_type = "Linear"

        elif isinstance(org_module, nn.LayerNorm):
            # torch.nn.LayerNorm(normalized_shape, eps=1e-05, elementwise_affine=True, device=None, dtype=None)
            self.fwd_kwargs = dict(normalized_shape = org_module.normalized_shape)
            # self.fwd_kwargs = dict()
            self.fwd_func = F.layer_norm
            self.org_module_type = "LayerNorm"
            self.scale_weights = False
        
        elif isinstance(org_module, GDN):
            # torch.nn.LayerNorm(normalized_shape, eps=1e-05, elementwise_affine=True, device=None, dtype=None)
            self.fwd_kwargs = dict(inverse = org_module.inverse, gamma_reparam = org_module.gamma_reparam, 
                                   beta_reparam = org_module.beta_reparam)
            # self.fwd_kwargs = dict()
            self.fwd_func = f_gdn
            self.org_module_type = "GDN"
            self.scale_weights = False
        
        elif isinstance(org_module, nn.PixelShuffle):
            self.fwd_kwargs = org_module.upscale_factor
            self.fwd_func = F.pixel_shuffle
            self.org_module_type = "PixelShuffle"
            self.scale_weights = False
        
        else:
            raise ValueError('Not supported modules: {}'.format(org_module))
        
        if isinstance(org_module, GDN):
            self.weight = org_module.gamma
            self.org_weight = org_module.gamma.data.clone()
            if org_module.beta is not None:
                self.bias = org_module.beta
                self.org_bias = org_module.beta.data.clone()
            else:
                self.bias = None
                self.org_bias = None
        elif self.org_module_type == "PixelShuffle":
            self.weight = None
            self.org_weight = None
            self.bias = None
            self.org_bias = None
        else:
            if isinstance(org_module, MaskedConv2d):
                self.mask = org_module.mask
            self.weight = org_module.weight
            self.org_weight = org_module.weight.data.clone()
            if org_module.bias is not None:
                self.bias = org_module.bias
                self.org_bias = org_module.bias.data.clone()
            else:
                self.bias = None
                self.org_bias = None
        
        if self.scale_weights:
            if self.org_module_type == "Conv2d" or self.org_module_type == "ConvTranspose2d" or self.org_module_type == "Linear" or self.org_module_type == "MaskedConv2d":
            #     max_values = torch.max(self.weight.reshape(self.weight.shape[0], -1) , dim=1)[0]
            # elif self.org_module_type == "ConvTranspose2d":
            #     max_values = torch.max(self.weight.reshape(self.weight.shape[1], -1) , dim=1)[0]
                self.weight_scaler = WeightScaler(self.weight, self.org_module_type)
            
        # de-activate the quantized forward default
        self.use_weight_quant = False
        self.use_act_quant = False
        self.disable_act_quant = disable_act_quant
        
        self.scale_method = weight_quant_params['scale_method']
        self.bit_type = weight_quant_params['bit_type']
        self.calibration_mode = weight_quant_params['calibration_mode']
        self.quantizer_type = weight_quant_params['quantizer_type']
        
        # needs changing so that these are not initialized for PixelShuffle
        self.weight_observer = build_observer(self.scale_method, self.bit_type, self.calibration_mode, self.if_tconv)
        self.weight_quantizer = build_quantizer(self.quantizer_type, self.bit_type, self.weight_observer, self.if_tconv)
        # self.weight_quantizer = UniformAffineQuantizer(tconv=self.if_tconv, n_bits=8, channel_wise=True, scale_method='max')
        
        self.dynamic_act = act_quant_params['dynamic']
        self.act_bit_type = act_quant_params['bit_type']
        if not self.dynamic_act:
            self.act_scale_method = act_quant_params['scale_method']
            self.act_calibration_mode = act_quant_params['calibration_mode']
            self.act_quantizer_type = act_quant_params['quantizer_type']
            if self.org_module_type == "Linear":
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, "layer_wise", if_tconv=False, is_act=True)
            else:
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, self.act_calibration_mode, if_tconv=False, is_act=True)
            self.act_quantizer = build_quantizer(self.act_quantizer_type, self.act_bit_type, self.act_observer, if_tconv=False, is_act=True)

        # initialize quantizer
        # self.weight_quantizer = UniformAffineQuantizer(tconv=self.if_tconv, **weight_quant_params)
        # self.act_quantizer = UniformAffineQuantizer(tconv=self.if_tconv,**act_quant_params)

        self.activation_function = nn.LeakyReLU(inplace=True) if self.org_module_type=="PixelShuffle" else StraightThrough()
        self.ignore_reconstruction = False

        self.extra_repr = org_module.extra_repr
        self.trained = True
        self.calibrate_weight = False
        self.calibrate_act = False
        
        if self.train_qe_loss:
            self.weight_loss = 0
            self.act_loss = 0
    
    def forward(self, input: torch.Tensor):
        if self.org_module_type == "PixelShuffle":
            out = self.fwd_func(input, self.fwd_kwargs)
            out = self.activation_function(out)
            return out
        if self.calibrate_weight:
            # try:
            self.weight_quantizer.observer.update(self.weight)
            self.weight_quantizer.update_quantization_params()
            # print("Successful calibration:  ", type(self.org_module))
            # except:
            #     print("Error in calibration:  ", type(self.org_module))
            self.calibrate_weight = False
                    
        if self.use_weight_quant: # and self.org_module_type != "GDN":
            # if self.scale_weights:
            #     if self.org_module_type == "Conv2d" or self.org_module_type == "ConvTranspose2d" or self.org_module_type == "Linear" or self.org_module_type == "MaskedConv2d":
            #         weight = self.weight_scaler(self.weight)
            #         weight = self.weight_quantizer(weight)
            #     else:
            #         weight = self.weight_quantizer(self.weight)
            # else:
            if self.quantizer_type == 'scaled-lsq':
                weight = self.weight_quantizer(self.weight, self.lr)
            else:
                weight = self.weight_quantizer(self.weight)
                # self.weight.data = self.weight_quantizer(self.weight)
                # weight = self.weight.data
            bias = self.bias
            if self.train_qe_loss:
                self.weight_loss = F.mse_loss(weight, self.weight, reduction='sum')
        else:
            # if not self.org_module_type == "GDN":
            weight = self.weight
            bias = self.bias
            # else:
            #     weight = self.org_weight
            #     bias = self.org_bias
        
        if self.org_module_type == "LayerNorm":
            out = self.fwd_func(input, weight=weight, bias=bias, **self.fwd_kwargs)
            # if self.train_qe_loss:
            #     out_real = self.fwd_func(input.detach(), weight=self.org_weight, bias=self.org_bias, **self.fwd_kwargs)
            #     self.act_loss = F.mse_loss(out, out_real, reduction='sum')
        elif self.org_module_type == "MaskedConv2d":
            weight.data *= self.mask
            out = self.fwd_func(input, weight, bias, **self.fwd_kwargs)
            # if self.train_qe_loss:
            #     out_real = self.fwd_func(input.detach(), self.org_weight, self.org_bias, **self.fwd_kwargs)
            #     self.act_loss = F.mse_loss(out, out_real, reduction='sum')
        else:
            out = self.fwd_func(input, weight, bias, **self.fwd_kwargs)
            # if self.train_qe_loss:
            #     out_real = self.fwd_func(input.detach(), self.org_weight, self.org_bias, **self.fwd_kwargs)
            #     self.act_loss = F.mse_loss(out, out_real, reduction='sum')
        
        # disable act quantization is designed for convolution before elemental-wise operation,
        # in that case, we apply activation function and quantization after ele-wise op.
        out = self.activation_function(out)
        
        if self.calibrate_act and not self.dynamic_act:
            self.act_quantizer.observer.update(out)
            self.act_quantizer.update_quantization_params()
            
        # out = ActQuantizer(out)
        if self.disable_act_quant:
            return out
        if self.use_act_quant and self.trained:
            if not self.dynamic_act:
                out = self.act_quantizer(out, self.lr)
            else:
                out = ActQuantizer(out, b_w=self.act_bit_type.bits)
        return out
    
    def init_MPQ(self, bit_type, act_bit_type=None):
        self.bit_type = bit_type
        
        self.weight_observer = build_observer(self.scale_method, self.bit_type, self.calibration_mode, self.if_tconv)
        self.weight_quantizer = build_quantizer(self.quantizer_type, self.bit_type, self.weight_observer, self.if_tconv)
        # self.weight_quantizer = UniformAffineQuantizer(tconv=self.if_tconv, n_bits=8, channel_wise=True, scale_method='max')
        
        if act_bit_type is not None:
            self.act_bit_type = act_bit_type
            
        if not self.dynamic_act:
            if self.org_module_type == "Linear":
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, "layer_wise", if_tconv=False, is_act=True)
            else:
                self.act_observer = build_observer(self.act_scale_method, self.act_bit_type, self.act_calibration_mode, if_tconv=False, is_act=True)
            self.act_quantizer = build_quantizer(self.act_quantizer_type, self.act_bit_type, self.act_observer, if_tconv=False, is_act=True)
    
    def set_quant_state(self, weight_quant: bool = False, act_quant: bool = False):
        self.use_weight_quant = weight_quant
        self.use_act_quant = act_quant
        
    def set_calibrate(self, calibrate_weight: bool = False, calibrate_act: bool = False):
        self.calibrate_weight = calibrate_weight
        self.calibrate_act = calibrate_act


def f_gdn(x, gamma, beta, inverse, gamma_reparam, beta_reparam):
    _, C, _, _ = x.size()
    gamma = gamma_reparam(gamma)
    beta = beta_reparam(beta)
    gamma = gamma.reshape(C, C, 1, 1)
    norm = F.conv2d(x**2, gamma, beta)
    if inverse:
        norm = torch.sqrt(norm)
    else:
        norm = torch.rsqrt(norm)
    
    out = x * norm
    return out
        
        
        
        
        
        
        
        
        
        
        
        
        
