# Copyright (c) MEGVII Inc. and its affiliates. All Rights Reserved.
import torch

from .base import BaseObserver


class MinmaxObserver(BaseObserver):

    def __init__(self, bit_type, calibration_mode, if_tconv, is_act):
        super(MinmaxObserver, self).__init__(bit_type, calibration_mode, if_tconv, is_act)
        # self.symmetric = self.bit_type.signed
        self.symmetric = False

    def update(self, v):
        # # self.inp_type = type()
        # v = self.reshape_tensor(v)          # reshaped weight tensor/ reshaped activation tensor
        # cur_max = v.max(axis=1).values      # contains the maximum value in each of the convolution filters
        # # print("CUR", cur_max.shape)
        # # cur_max1 = v.max(dim=1).values
        # # print("CUR1", cur_max1.shape)
        # # print(cur_max == cur_max1)
        # if self.max_val is None:
        #     self.max_val = cur_max          # contains the maximum value in each of the convolution filters
        # else:
        #     self.max_val = torch.max(cur_max, self.max_val)
        # cur_min = v.min(axis=1).values
        # if self.min_val is None:
        #     self.min_val = cur_min
        # else:
        #     self.min_val = torch.min(cur_min, self.min_val)

        # self.max_val = torch.maximum(self.max_val, torch.zeros_like(self.max_val))
        # self.min_val = torch.minimum(self.min_val, torch.zeros_like(self.min_val))

        # if self.calibration_mode == 'layer_wise':
        #     # print("FIRST    ", self.max_val.shape)
        #     self.max_val = self.max_val.max()
        #     self.min_val = self.min_val.min()
        #     # print("SECOND    ", self.max_val.shape)
        
        self.n_levels = self.bit_type.upper_bound - self.bit_type.lower_bound
        channel_wise = False
        if self.calibration_mode == "channel_wise":
            channel_wise = True
        self.scale, self.zero_point = self.init_quantization_scale(v, channel_wise)

    def get_quantization_params(self, *args, **kwargs):
        # max_val = self.max_val
        # min_val = self.min_val
        # # print("MAX SHAPE", max_val.shape)
        # # print("MIN SHAPE", min_val.shape)
        # qmax = self.bit_type.upper_bound            # 255/127
        # qmin = self.bit_type.lower_bound            # 0/-128
        # n_levels = self.bit_type.upper_bound - self.bit_type.lower_bound

        # scale = torch.ones_like(max_val, dtype=torch.float32)
        # zero_point = torch.zeros_like(max_val, dtype=torch.float32)
        # self.eps = self.eps.to(scale.device)
        # # print(scale.device)
        # # print(self.eps.device)

        # if self.symmetric:
        #     max_val = torch.max(-min_val, max_val)
        #     scale = max_val / (float(qmax - qmin) / 2)
        #     scale.clamp_(self.eps)                                  # clamps to the lower bound of self.eps
        #     zero_point = torch.zeros_like(max_val, dtype=torch.float32)
        # else:
        #     scale = torch.tensor((max_val - min_val) / (n_levels - 1))
        #     scale = torch.max(scale, self.eps)
        #     zero_point = (- min_val / scale).round()
        #     scale = torch.tensor(scale) # .type_as(x)
        #     zero_point = torch.tensor(zero_point)# .type_as(x)
            
        # else:
        #     if max_val.mean() == min_val.mean():
        #         scale = max_val
        #         zero_point = torch.zeros_like(max_val, dtype=torch.float32)
        #     else:
        #         scale = (max_val - min_val) / float(qmax - qmin)
        #         scale.clamp_(self.eps)                                  # clamps to the lower bound of self.eps
        #         # print("SCALE:   ", scale)
        #         zero_point = qmin - torch.round(min_val / scale)
        #         # zero_point.clamp_(qmin, qmax)
        
        scale = self.scale
        zero_point = self.zero_point

        return scale, zero_point


    def init_quantization_scale(self, x: torch.Tensor, channel_wise: bool = False):
        delta, zero_point = None, None
        if channel_wise:
            x_clone = x.clone().detach()
            if self.if_tconv or self.is_act:
                n_channels = x_clone.shape[1]
            else:
                n_channels = x_clone.shape[0] 

            if len(x.shape) == 4:
                if self.if_tconv or self.is_act:
                    x_max = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=0)[0]       # x_clone.reshape(x.shape[1], -1).max(dim=1)[0]
                else:
                    x_max = x_clone.abs().max(dim=-1)[0].max(dim=-1)[0].max(dim=-1)[0]      # x_clone.reshape(x.shape[0], -1).max(dim=1)[0] 
                
            elif len(x.shape) == 1:
                x_max = x_clone.abs().max()

            else: 
                x_max = x_clone.abs().max(dim=-1)[0]

            delta = x_max.clone()
            zero_point = x_max.clone()

            if len(x.shape) == 1:
                delta, zero_point = self.init_quantization_scale(x_clone, channel_wise=False)
            else:
                if self.if_tconv or self.is_act:
                    for c in range(n_channels):
                        delta[c], zero_point[c] = self.init_quantization_scale(x_clone[:, c], channel_wise=False)
                else:
                    for c in range(n_channels):
                        delta[c], zero_point[c] = self.init_quantization_scale(x_clone[c], channel_wise=False)

            if len(x.shape) == 4:
                if self.if_tconv or self.is_act:
                    delta = delta.view(1, -1, 1, 1)
                    zero_point = zero_point.view(1, -1, 1, 1)
                else:
                    delta = delta.view(-1, 1, 1, 1)
                    zero_point = zero_point.view(-1, 1, 1, 1)
            elif len(x.shape) == 1:
                delta = delta.view(-1)
                zero_point = zero_point.view(-1)
            else:
                delta = delta.view(-1, 1)
                zero_point = zero_point.view(-1, 1)
        else:
            x_min = min(x.min().item(), 0)
            x_max = max(x.max().item(), 0)
            # if 'scale' in self.scale_method:
            #     x_min = x_min * (self.n_bits + 2) / 8
            #     x_max = x_max * (self.n_bits + 2) / 8

            # x_absmax = max(abs(x_min), x_max)
            # if self.sym:
            #     x_min, x_max = -x_absmax if x_min < 0 else 0, x_absmax

            delta = torch.tensor((x_max - x_min) / (self.n_levels - 1))
            delta = torch.max(delta, self.eps)

            # zero_point = round(-x_min / delta)
            zero_point = (- x_min / delta).round()
            delta = torch.tensor(delta).type_as(x)
            zero_point = torch.tensor(zero_point).type_as(x)
        
        return delta, zero_point