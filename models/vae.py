from pathlib import Path
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor
import warnings
import yaml

from torch.hub import load_state_dict_from_url
from compressai.models import CompressionModel
from compressai.models.utils import conv, deconv
from compressai.ans import BufferedRansEncoder, RansDecoder
from compressai.entropy_models import EntropyBottleneck, GaussianConditional
from compressai.layers import GDN, MaskedConv2d
from compressai.zoo.pretrained import load_pretrained
# from compressai.layers.layers import ResidualBlockWithStride, ResidualBlock, ResidualBlockUpsample, subpel_conv3x3, conv1x1, conv3x3
from .quantization import ResidualBlockWithStride, ResidualBlockUpsample, ResidualBlock, subpel_conv3x3, conv1x1, conv3x3

from .registry import register_model
from .quantization.quant_model import QuantModel
from .nic_cvt import NIC, TinyLIC

import random  
import math


def conv(in_channels, out_channels, kernel_size=5, stride=2):
    return nn.Conv2d(
        in_channels,
        out_channels,
        kernel_size=kernel_size,
        stride=stride,
        padding=kernel_size // 2,
    )


def deconv(in_channels, out_channels, kernel_size=5, stride=2):
    return nn.ConvTranspose2d(
        in_channels,
        out_channels,
        kernel_size=kernel_size,
        stride=stride,
        output_padding=stride - 1,
        padding=kernel_size // 2,
    )


def get_config(config_path):
    with open(config_path, 'r') as stream:
        config = yaml.load(stream, Loader=yaml.FullLoader)
        return config


class FactorizedPrior(CompressionModel):
    r"""Factorized Prior model from J. Balle, D. Minnen, S. Singh, S.J. Hwang,
    N. Johnston: `"Variational Image Compression with a Scale Hyperprior"
    <https://arxiv.org/abs/1802.01436>`_, Int Conf. on Learning Representations
    (ICLR), 2018.

    .. code-block:: none

                  ┌───┐    y
            x ──►─┤g_a├──►─┐
                  └───┘    │
                           ▼
                         ┌─┴─┐
                         │ Q │
                         └─┬─┘
                           │
                     y_hat ▼
                           │
                           ·
                        EB :
                           ·
                           │
                     y_hat ▼
                           │
                  ┌───┐    │
        x_hat ──◄─┤g_s├────┘
                  └───┘

        EB = Entropy bottleneck

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    """

    def __init__(self, N, M, bpp_lmb, **kwargs):
        super().__init__(**kwargs)

        self.entropy_bottleneck = EntropyBottleneck(M)

        self.g_a = nn.Sequential(
            conv(3, N),
            GDN(N),
            conv(N, N),
            GDN(N),
            conv(N, N),
            GDN(N),
            conv(N, M),
        )

        self.g_s = nn.Sequential(
            deconv(M, N),
            GDN(N, inverse=True),
            deconv(N, N),
            GDN(N, inverse=True),
            deconv(N, N),
            GDN(N, inverse=True),
            deconv(N, 3),
        )

        self.N = N
        self.M = M
        self.bpp_lmb = bpp_lmb

    @property
    def downsampling_factor(self) -> int:
        return int(2**4)

    def forward(self, x):
        nB, _, imH, imW = x.shape
        device = x.device
        # bpp_lmb = torch.tensor([self.bpp_lmb]).expand(nB).view(-1, 1).to(device)
        bpp_lmb = self.bpp_lmb
        y = self.g_a(x)
        y_hat, y_likelihoods = self.entropy_bottleneck(y)
        x_hat = self.g_s(y_hat)

        return {
            "x_hat": x_hat,
            "likelihoods": {
                "y": y_likelihoods,
            },
            "lmb": bpp_lmb
        }

    @classmethod
    def from_state_dict(cls, state_dict):
        """Return a new model instance from `state_dict`."""
        N = state_dict["g_a.0.weight"].size(0)
        M = state_dict["g_a.6.weight"].size(0)
        net = cls(N, M)
        net.load_state_dict(state_dict)
        return net

    @torch.no_grad()
    def compress(self, x):
        y = self.g_a(x)
        compressed_y = self.entropy_bottleneck.compress(y)
        compressed_obj = {"strings": compressed_y, "shape": tuple(y.shape[2:])}
        return compressed_obj

    @torch.no_grad()
    def decompress(self, compressed_obj):
        bitstreams, latent_shape = compressed_obj["strings"], compressed_obj["shape"]
        y_quantized = self.entropy_bottleneck.decompress(bitstreams, latent_shape)
        x_hat = self.g_s(y_quantized).clamp_(0, 1)
        return {"x_hat": x_hat}


class ScaleHyperprior(CompressionModel):
    r"""Scale Hyperprior model from J. Balle, D. Minnen, S. Singh, S.J. Hwang,
    N. Johnston: `"Variational Image Compression with a Scale Hyperprior"
    <https://arxiv.org/abs/1802.01436>`_ Int. Conf. on Learning Representations
    (ICLR), 2018.

    .. code-block:: none

                  ┌───┐    y     ┌───┐  z  ┌───┐ z_hat      z_hat ┌───┐
            x ──►─┤g_a├──►─┬──►──┤h_a├──►──┤ Q ├───►───·⋯⋯·───►───┤h_s├─┐
                  └───┘    │     └───┘     └───┘        EB        └───┘ │
                           ▼                                            │
                         ┌─┴─┐                                          │
                         │ Q │                                          ▼
                         └─┬─┘                                          │
                           │                                            │
                     y_hat ▼                                            │
                           │                                            │
                           ·                                            │
                        GC : ◄─────────────────────◄────────────────────┘
                           ·                 scales_hat
                           │
                     y_hat ▼
                           │
                  ┌───┐    │
        x_hat ──◄─┤g_s├────┘
                  └───┘

        EB = Entropy bottleneck
        GC = Gaussian conditional

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    """

    def __init__(self, N, M, bpp_lmb, **kwargs):
        super().__init__(**kwargs)

        self.entropy_bottleneck = EntropyBottleneck(N)

        self.g_a = nn.Sequential(
            conv(3, N),
            GDN(N),
            conv(N, N),
            GDN(N),
            conv(N, N),
            GDN(N),
            conv(N, M),
        )

        self.g_s = nn.Sequential(
            deconv(M, N),
            GDN(N, inverse=True),
            deconv(N, N),
            GDN(N, inverse=True),
            deconv(N, N),
            GDN(N, inverse=True),
            deconv(N, 3),
        )

        self.h_a = nn.Sequential(
            conv(M, N, stride=1, kernel_size=3),
            nn.ReLU(inplace=True),
            conv(N, N),
            nn.ReLU(inplace=True),
            conv(N, N),
        )

        self.h_s = nn.Sequential(
            deconv(N, N),
            nn.ReLU(inplace=True),
            deconv(N, N),
            nn.ReLU(inplace=True),
            conv(N, M, stride=1, kernel_size=3),
            nn.ReLU(inplace=True),
        )

        self.gaussian_conditional = GaussianConditional(None)
        self.N = int(N)
        self.M = int(M)
        self.bpp_lmb = bpp_lmb
        self.name = "scale_hyperprior"

    @property
    def downsampling_factor(self) -> int:
        return int(2 ** (4 + 2))

    def forward(self, x):
        nB, _, imH, imW = x.shape
        device = x.device
        # bpp_lmb = torch.tensor([self.bpp_lmb]).expand(nB).view(-1, 1).to(device)
        bpp_lmb = self.bpp_lmb
        y = self.g_a(x)
        z = self.h_a(torch.abs(y))
        z_hat, z_likelihoods = self.entropy_bottleneck(z)
        scales_hat = self.h_s(z_hat)
        y_hat, y_likelihoods = self.gaussian_conditional(y, scales_hat)
        x_hat = self.g_s(y_hat)

        return {
            "x_hat": x_hat,
            "likelihoods": {"y": y_likelihoods, "z": z_likelihoods},
            "lmb": bpp_lmb
        }

    @torch.no_grad()
    def compress(self, x):
        y = self.g_a(x)
        z = self.h_a(torch.abs(y))

        compressed_z = self.entropy_bottleneck.compress(z)
        z_hat = self.entropy_bottleneck.decompress(compressed_z, z.size()[-2:])

        scales_hat = self.h_s(z_hat)
        indexes = self.gaussian_conditional.build_indexes(scales_hat)
        compressed_y = self.gaussian_conditional.compress(y, indexes)
        compressed_obj = {"strings": [compressed_y, compressed_z], "shape": tuple(z.shape[2:])}
        return compressed_obj

    @torch.no_grad()
    def decompress(self, compressed_obj):
        bitstreams, latent_shape = compressed_obj["strings"], compressed_obj["shape"]
        z_quantized = self.entropy_bottleneck.decompress(bitstreams[1], latent_shape)
        scales_hat = self.h_s(z_quantized)
        indexes = self.gaussian_conditional.build_indexes(scales_hat)
        y_quantized = self.gaussian_conditional.decompress(bitstreams[0], indexes, z_quantized.dtype)
        x_hat = self.g_s(y_quantized).clamp_(0, 1)
        return {"x_hat": x_hat}


class MeanScaleHyperprior(ScaleHyperprior):
    r"""Scale Hyperprior with non zero-mean Gaussian conditionals from D.
    Minnen, J. Balle, G.D. Toderici: `"Joint Autoregressive and Hierarchical
    Priors for Learned Image Compression" <https://arxiv.org/abs/1809.02736>`_,
    Adv. in Neural Information Processing Systems 31 (NeurIPS 2018).

    .. code-block:: none

                  ┌───┐    y     ┌───┐  z  ┌───┐ z_hat      z_hat ┌───┐
            x ──►─┤g_a├──►─┬──►──┤h_a├──►──┤ Q ├───►───·⋯⋯·───►───┤h_s├─┐
                  └───┘    │     └───┘     └───┘        EB        └───┘ │
                           ▼                                            │
                         ┌─┴─┐                                          │
                         │ Q │                                          ▼
                         └─┬─┘                                          │
                           │                                            │
                     y_hat ▼                                            │
                           │                                            │
                           ·                                            │
                        GC : ◄─────────────────────◄────────────────────┘
                           ·                 scales_hat
                           │                 means_hat
                     y_hat ▼
                           │
                  ┌───┐    │
        x_hat ──◄─┤g_s├────┘
                  └───┘

        EB = Entropy bottleneck
        GC = Gaussian conditional

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    """

    def __init__(self, N, M,  bpp_lmb, **kwargs):
        super().__init__(N=N, M=M, bpp_lmb=bpp_lmb, **kwargs)
        
        self.g_a = nn.Sequential(
            conv(3, N),
            nn.ReLU(inplace=True),
            conv(N, N),
            nn.ReLU(inplace=True),
            conv(N, N),
            nn.ReLU(inplace=True),
            conv(N, M),
        )

        self.g_s = nn.Sequential(
            deconv(M, N),
            nn.ReLU(inplace=True),
            deconv(N, N),
            nn.ReLU(inplace=True),
            deconv(N, N),
            nn.ReLU(inplace=True),
            deconv(N, 3),
        )

        self.h_a = nn.Sequential(
            conv(M, N, stride=1, kernel_size=3),
            nn.ReLU(inplace=True),
            conv(N, N),
            nn.ReLU(inplace=True),
            conv(N, N),
        )

        self.h_s = nn.Sequential(
            deconv(N, M),
            nn.ReLU(inplace=True),
            deconv(M, M * 3 // 2),
            nn.ReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3),
        )
        
        self.name = "mean_scale_hyperprior"
    
    @property
    def downsampling_factor(self) -> int:
        return int(2 ** (4 + 2))
    
    def forward(self, x):
        nB, _, imH, imW = x.shape
        device = x.device
        bpp_lmb = self.bpp_lmb
        y = self.g_a(x)
        z = self.h_a(y)
        z_hat, z_likelihoods = self.entropy_bottleneck(z)
        gaussian_params = self.h_s(z_hat)
        scales_hat, means_hat = gaussian_params.chunk(2, 1)
        y_hat, y_likelihoods = self.gaussian_conditional(y, scales_hat, means=means_hat)
        x_hat = self.g_s(y_hat)

        return {
            "x_hat": x_hat,
            "likelihoods": {"y": y_likelihoods, "z": z_likelihoods},
            "lmb": bpp_lmb
        }
    
    @torch.no_grad()
    def compress(self, x):
        y = self.g_a(x)
        z = self.h_a(y)

        compressed_z = self.entropy_bottleneck.compress(z)
        z_hat = self.entropy_bottleneck.decompress(compressed_z, z.size()[-2:])

        gaussian_params = self.h_s(z_hat)
        scales_hat, means_hat = gaussian_params.chunk(2, 1)
        indexes = self.gaussian_conditional.build_indexes(scales_hat)
        compressed_y = self.gaussian_conditional.compress(y, indexes, means=means_hat)
        compressed_obj = {"strings": [compressed_y, compressed_z], "shape": tuple(z.shape[2:])}
        return compressed_obj

    @torch.no_grad()
    def decompress(self, compressed_obj):
        bitstreams, latent_shape = compressed_obj["strings"], compressed_obj["shape"]
        z_quantized = self.entropy_bottleneck.decompress(bitstreams[1], latent_shape)
        gaussian_params = self.h_s(z_quantized)
        scales_hat, means_hat = gaussian_params.chunk(2, 1)
        indexes = self.gaussian_conditional.build_indexes(scales_hat)
        y_quantized = self.gaussian_conditional.decompress(bitstreams[0], indexes, means=means_hat)
        x_hat = self.g_s(y_quantized).clamp_(0, 1)
        return {"x_hat": x_hat}
  

class Cheng2020Anchor(CompressionModel):
    """Anchor model variant from `"Learned Image Compression with
    Discretized Gaussian Mixture Likelihoods and Attention Modules"
    <https://arxiv.org/abs/2001.01568>`_, by Zhengxue Cheng, Heming Sun, Masaru
    Takeuchi, Jiro Katto.

    Uses residual blocks with small convolutions (3x3 and 1x1), and sub-pixel
    convolutions for up-sampling.

    Args:
        N (int): Number of channels
    """

    def __init__(self, N, bpp_lmb, **kwargs):
        super().__init__(**kwargs)

        self.g_a = nn.Sequential(
            ResidualBlockWithStride(3, N, stride=2),
            ResidualBlock(N, N),
            ResidualBlockWithStride(N, N, stride=2),
            ResidualBlock(N, N),
            ResidualBlockWithStride(N, N, stride=2),
            ResidualBlock(N, N),
            conv3x3(N, N, stride=2),
        )

        self.h_a = nn.Sequential(
            conv3x3(N, N),
            nn.LeakyReLU(inplace=True),
            conv3x3(N, N),
            nn.LeakyReLU(inplace=True),
            conv3x3(N, N, stride=2),
            nn.LeakyReLU(inplace=True),
            conv3x3(N, N),
            nn.LeakyReLU(inplace=True),
            conv3x3(N, N, stride=2),
        )

        self.h_s = nn.Sequential(
            conv3x3(N, N),
            nn.LeakyReLU(inplace=True),
            subpel_conv3x3(N, N, 2),
            nn.LeakyReLU(inplace=True),
            conv3x3(N, N * 3 // 2),
            nn.LeakyReLU(inplace=True),
            subpel_conv3x3(N * 3 // 2, N * 3 // 2, 2),
            nn.LeakyReLU(inplace=True),
            conv3x3(N * 3 // 2, N * 2),
        )

        self.g_s = nn.Sequential(
            ResidualBlock(N, N),
            ResidualBlockUpsample(N, N, 2),
            ResidualBlock(N, N),
            ResidualBlockUpsample(N, N, 2),
            ResidualBlock(N, N),
            ResidualBlockUpsample(N, N, 2),
            ResidualBlock(N, N),
            subpel_conv3x3(N, 3, 2),
        )
    
        self.entropy_parameters = nn.Sequential(
            nn.Conv2d(N * 12 // 3, N * 10 // 3, 1),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(N * 10 // 3, N * 8 // 3, 1),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(N * 8 // 3, N * 6 // 3, 1),
        )

        self.context_prediction = MaskedConv2d(
            N, 2 * N, kernel_size=5, padding=2, stride=1
        )
        
        self.entropy_bottleneck = EntropyBottleneck(N)

        self.name = "cheng_anchor"
        self.gaussian_conditional = GaussianConditional(None)
        self.N = int(N)
        self.bpp_lmb = bpp_lmb
    
    @property
    def downsampling_factor(self) -> int:
        return 2 ** (4 + 2)

    def forward(self, x):
        nB, _, imH, imW = x.shape
        device = x.device
        # bpp_lmb = torch.tensor([self.bpp_lmb]).expand(nB).view(-1, 1).to(device)
        bpp_lmb = self.bpp_lmb
        y = self.g_a(x)
        z = self.h_a(y)
        z_hat, z_likelihoods = self.entropy_bottleneck(z)
        params = self.h_s(z_hat)

        y_hat = self.gaussian_conditional.quantize(
            y, "noise" if self.training else "dequantize"
        )
        ctx_params = self.context_prediction(y_hat)
        gaussian_params = self.entropy_parameters(
            torch.cat((params, ctx_params), dim=1)
        )
        scales_hat, means_hat = gaussian_params.chunk(2, 1)
        _, y_likelihoods = self.gaussian_conditional(y, scales_hat, means=means_hat)
        x_hat = self.g_s(y_hat)

        return {
            "x_hat": x_hat,
            "likelihoods": {"y": y_likelihoods, "z": z_likelihoods},
            "lmb": bpp_lmb
        }

    @classmethod
    def from_state_dict(cls, state_dict):
        """Return a new model instance from `state_dict`."""
        N = state_dict["g_a.0.conv1.weight"].size(0)
        net = cls(N)
        net.load_state_dict(state_dict)
    
    def compress(self, x):
        if next(self.parameters()).device != torch.device("cpu"):
            warnings.warn(
                "Inference on GPU is not recommended for the autoregressive "
                "models (the entropy coder is run sequentially on CPU)."
            )

        y = self.g_a(x)
        z = self.h_a(y)

        z_strings = self.entropy_bottleneck.compress(z)
        z_hat = self.entropy_bottleneck.decompress(z_strings, z.size()[-2:])

        params = self.h_s(z_hat)

        s = 4  # scaling factor between z and y
        kernel_size = 5  # context prediction kernel size
        padding = (kernel_size - 1) // 2

        y_height = z_hat.size(2) * s
        y_width = z_hat.size(3) * s

        y_hat = F.pad(y, (padding, padding, padding, padding))

        y_strings = []
        for i in range(y.size(0)):
            string = self._compress_ar(
                y_hat[i : i + 1],
                params[i : i + 1],
                y_height,
                y_width,
                kernel_size,
                padding,
            )
            y_strings.append(string)

        return {"strings": [y_strings, z_strings], "shape": z.size()[-2:]}

    def _compress_ar(self, y_hat, params, height, width, kernel_size, padding):
        cdf = self.gaussian_conditional.quantized_cdf.tolist()
        cdf_lengths = self.gaussian_conditional.cdf_length.tolist()
        offsets = self.gaussian_conditional.offset.tolist()

        encoder = BufferedRansEncoder()
        symbols_list = []
        indexes_list = []

        # Warning, this is slow...
        # TODO: profile the calls to the bindings...
        masked_weight = self.context_prediction.weight * self.context_prediction.mask
        for h in range(height):
            for w in range(width):
                y_crop = y_hat[:, :, h : h + kernel_size, w : w + kernel_size]
                ctx_p = F.conv2d(
                    y_crop,
                    masked_weight,
                    bias=self.context_prediction.bias,
                )

                # 1x1 conv for the entropy parameters prediction network, so
                # we only keep the elements in the "center"
                p = params[:, :, h : h + 1, w : w + 1]
                gaussian_params = self.entropy_parameters(torch.cat((p, ctx_p), dim=1))
                gaussian_params = gaussian_params.squeeze(3).squeeze(2)
                scales_hat, means_hat = gaussian_params.chunk(2, 1)

                indexes = self.gaussian_conditional.build_indexes(scales_hat)

                y_crop = y_crop[:, :, padding, padding]
                y_q = self.gaussian_conditional.quantize(y_crop, "symbols", means_hat)
                y_hat[:, :, h + padding, w + padding] = y_q + means_hat

                symbols_list.extend(y_q.squeeze().tolist())
                indexes_list.extend(indexes.squeeze().tolist())

        encoder.encode_with_indexes(
            symbols_list, indexes_list, cdf, cdf_lengths, offsets
        )

        string = encoder.flush()
        return string

    def decompress(self, compressed_obj):
        strings, shape = compressed_obj["strings"], compressed_obj["shape"]
        assert isinstance(strings, list) and len(strings) == 2

        if next(self.parameters()).device != torch.device("cpu"):
            warnings.warn(
                "Inference on GPU is not recommended for the autoregressive "
                "models (the entropy coder is run sequentially on CPU)."
            )

        # FIXME: we don't respect the default entropy coder and directly call the
        # range ANS decoder

        z_hat = self.entropy_bottleneck.decompress(strings[1], shape)
        params = self.h_s(z_hat)

        s = 4  # scaling factor between z and y
        kernel_size = 5  # context prediction kernel size
        padding = (kernel_size - 1) // 2

        y_height = z_hat.size(2) * s
        y_width = z_hat.size(3) * s

        # initialize y_hat to zeros, and pad it so we can directly work with
        # sub-tensors of size (N, C, kernel size, kernel_size)
        y_hat = torch.zeros(
            (z_hat.size(0), self.N, y_height + 2 * padding, y_width + 2 * padding),
            device=z_hat.device,
        )

        for i, y_string in enumerate(strings[0]):
            self._decompress_ar(
                y_string,
                y_hat[i : i + 1],
                params[i : i + 1],
                y_height,
                y_width,
                kernel_size,
                padding,
            )

        y_hat = F.pad(y_hat, (-padding, -padding, -padding, -padding))
        x_hat = self.g_s(y_hat).clamp_(0, 1)
        return {"x_hat": x_hat}

    def _decompress_ar(
        self, y_string, y_hat, params, height, width, kernel_size, padding
    ):
        cdf = self.gaussian_conditional.quantized_cdf.tolist()
        cdf_lengths = self.gaussian_conditional.cdf_length.tolist()
        offsets = self.gaussian_conditional.offset.tolist()

        decoder = RansDecoder()
        decoder.set_stream(y_string)

        # Warning: this is slow due to the auto-regressive nature of the
        # decoding... See more recent publication where they use an
        # auto-regressive module on chunks of channels for faster decoding...
        for h in range(height):
            for w in range(width):
                # only perform the 5x5 convolution on a cropped tensor
                # centered in (h, w)
                y_crop = y_hat[:, :, h : h + kernel_size, w : w + kernel_size]
                ctx_p = F.conv2d(
                    y_crop,
                    self.context_prediction.weight,
                    bias=self.context_prediction.bias,
                )
                # 1x1 conv for the entropy parameters prediction network, so
                # we only keep the elements in the "center"
                p = params[:, :, h : h + 1, w : w + 1]
                gaussian_params = self.entropy_parameters(torch.cat((p, ctx_p), dim=1))
                scales_hat, means_hat = gaussian_params.chunk(2, 1)

                indexes = self.gaussian_conditional.build_indexes(scales_hat)
                rv = decoder.decode_stream(
                    indexes.squeeze().tolist(), cdf, cdf_lengths, offsets
                )
                rv = torch.Tensor(rv).reshape(1, -1, 1, 1)
                rv = self.gaussian_conditional.dequantize(rv, means_hat)

                hp = h + padding
                wp = w + padding
                y_hat[:, :, hp : hp + 1, wp : wp + 1] = rv


@register_model
def factorized_prior(N, M, bpp_lmb):
    # N = 64          # 48
    # M = 96          # 64
    # bpp_lmb = 10**-2
    model = FactorizedPrior(N=N, M=M, bpp_lmb=bpp_lmb)
    return model

@register_model
def scale_hyperprior(N, M, bpp_lmb):
    # N = 64
    # M = 96
    # bpp_lmb = 0.08                                             # [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
    model = ScaleHyperprior(N=N, M=M, bpp_lmb=bpp_lmb)
    return model

@register_model
def cheng_anchor(N, M, bpp_lmb):
    # N = 64
    # M = 96
    # bpp_lmb = 0.08                                             # [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
    model = Cheng2020Anchor(N=N, bpp_lmb=bpp_lmb)
    return model

@register_model
def mean_scale_hyperprior(N, M, bpp_lmb):
    # N = 64
    # M = 96
    # bpp_lmb = 0.08                                             # [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
    model = MeanScaleHyperprior(N=N, M=M, bpp_lmb=bpp_lmb)
    return model

@register_model
def nic(N, M):
    config = get_config("config.yaml")
    # config['embed_dim'] = cfgs["nic"][quality][0]
    # config['latent_dim'] = cfgs["nic"][quality][1]
    # model = NIC(config)
    model = TinyLIC(N=N, M=M)
    return model


@register_model
def cheng_anchor_quantized(N, M, bpp_lmb, wq_params, aq_params, quality, device='cpu', pretrained=False):
    # N = 64
    # M = 96
    # bpp_lmb = 0.08                                             # [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24] 
    model = Cheng2020Anchor(N=N, bpp_lmb=bpp_lmb)
    if pretrained:
        model_url = model_urls[model.name]["mse"][(quality, (N, N), bpp_lmb)]
        state_dict = load_state_dict_from_url(model_url, progress=True)
        state_dict = load_pretrained(state_dict)
        model.load_state_dict(state_dict, strict=True)
    model.to(device)
    quant_model = QuantModel(model=model, weight_quant_params=wq_params, act_quant_params=aq_params)
    
    return quant_model

@register_model
def scale_hyperprior_quantized(N, M, bpp_lmb, wq_params, aq_params, quality, device='cpu', pretrained=False):
    # N = 64
    # M = 96
    # bpp_lmb = 0.08                                             # [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
    model = ScaleHyperprior(N=N, M=M, bpp_lmb=bpp_lmb)
    if pretrained:
        model_url = model_urls[model.name]["mse"][(quality, (N, M), bpp_lmb)]
        state_dict = load_state_dict_from_url(model_url, progress=True)
        state_dict = load_pretrained(state_dict)
        model.load_state_dict(state_dict, strict=True)
    model.to(device)
    quant_model = QuantModel(model=model, weight_quant_params=wq_params, act_quant_params=aq_params)
    
    return quant_model

@register_model
def mean_scale_hyperprior_quantized(N, M, bpp_lmb, wq_params, aq_params, quality, device='cpu', pretrained=False):
    # N = 64
    # M = 96
    # bpp_lmb = 0.08                                             # [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
    model = MeanScaleHyperprior(N=N, M=M, bpp_lmb=bpp_lmb)
    if pretrained:
        # need to fix
        checkpoint_root = Path(f'checkpoints/mean_scale_hyperprior')
        checkpoint_paths = list(checkpoint_root.rglob('*.pt'))
        checkpoint_paths.sort()
        ckptpath = checkpoint_paths[quality-1]
        print(ckptpath)
        print(f"Quality: {quality}", ckptpath)
        checkpoint = torch.load(ckptpath)
        model.load_state_dict(checkpoint['model'], strict=True)
        
        # model_url = model_urls[model.name]["mse"][(quality, (N, M), bpp_lmb)]
        # state_dict = load_state_dict_from_url(model_url, progress=True)
        # state_dict = load_pretrained(state_dict)
        # model.load_state_dict(state_dict, strict=True)
    model.to(device)
    quant_model = QuantModel(model=model, weight_quant_params=wq_params, act_quant_params=aq_params)
    
    return quant_model

@register_model
def nic_quantized(wq_params, aq_params, quality, device='cpu', pretrained=False):
    config = get_config("config.yaml")
    config['embed_dim'] = cfgs["nic"][quality][0]
    config['latent_dim'] = cfgs["nic"][quality][1]
    model = NIC(config)
    if pretrained:
        # model_url = model_urls["nic"]["mse"][quality]
        # state_dict = load_state_dict_from_url(model_url, progress=True)
        # state_dict = load_pretrained(state_dict)
        # model.load_state_dict(state_dict, strict=True)
        checkpoint_root = Path(f'checkpoints/nic')
        checkpoint_paths = list(checkpoint_root.rglob('*.pt'))
        checkpoint_paths.sort()
        ckptpath = checkpoint_paths[quality-1]
        print(ckptpath)
        print(f"Quality: {quality}", ckptpath)
        checkpoint = torch.load(ckptpath)
        model.load_state_dict(checkpoint['model'], strict=True)
    model.to(device)
    quant_model = QuantModel(model=model, weight_quant_params=wq_params, act_quant_params=aq_params)
    
    return quant_model


root_url_nic = "./ckpts/Lu2022"
root_url = "https://compressai.s3.amazonaws.com/models/v1"
model_urls = {
    "factorized_prior": {
        "mse": {
            (1, (128, 192), 0.0018): f"{root_url}/bmshj2018-factorized-prior-1-446d5c7f.pth.tar",
            (2, (128, 192), 0.0035): f"{root_url}/bmshj2018-factorized-prior-2-87279a02.pth.tar",
            (3, (128, 192), 0.0067): f"{root_url}/bmshj2018-factorized-prior-3-5c6f152b.pth.tar",
            (4, (128, 192), 0.0130): f"{root_url}/bmshj2018-factorized-prior-4-1ed4405a.pth.tar",
            (5, (128, 192), 0.0250): f"{root_url}/bmshj2018-factorized-prior-5-866ba797.pth.tar",
            (6, (192, 320), 0.0483): f"{root_url}/bmshj2018-factorized-prior-6-9b02ea3a.pth.tar",
            (7, (192, 320), 0.0932): f"{root_url}/bmshj2018-factorized-prior-7-6dfd6734.pth.tar",
            (8, (192, 320), 0.1800): f"{root_url}/bmshj2018-factorized-prior-8-5232faa3.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/bmshj2018-factorized-ms-ssim-1-9781d705.pth.tar",
            2: f"{root_url}/bmshj2018-factorized-ms-ssim-2-4a584386.pth.tar",
            3: f"{root_url}/bmshj2018-factorized-ms-ssim-3-5352f123.pth.tar",
            4: f"{root_url}/bmshj2018-factorized-ms-ssim-4-4f91b847.pth.tar",
            5: f"{root_url}/bmshj2018-factorized-ms-ssim-5-b3a88897.pth.tar",
            6: f"{root_url}/bmshj2018-factorized-ms-ssim-6-ee028763.pth.tar",
            7: f"{root_url}/bmshj2018-factorized-ms-ssim-7-8c265a29.pth.tar",
            8: f"{root_url}/bmshj2018-factorized-ms-ssim-8-8811bd14.pth.tar",
        },
    },
    "scale_hyperprior": {
        "mse": {
            (1, (128, 192), 0.0018): f"{root_url}/bmshj2018-hyperprior-1-7eb97409.pth.tar",
            (2, (128, 192), 0.0035): f"{root_url}/bmshj2018-hyperprior-2-93677231.pth.tar",
            (3, (128, 192), 0.0067): f"{root_url}/bmshj2018-hyperprior-3-6d87be32.pth.tar",
            (4, (128, 192), 0.0130): f"{root_url}/bmshj2018-hyperprior-4-de1b779c.pth.tar",
            (5, (128, 192), 0.0250): f"{root_url}/bmshj2018-hyperprior-5-f8b614e1.pth.tar",
            (6, (192, 320), 0.0483): f"{root_url}/bmshj2018-hyperprior-6-1ab9c41e.pth.tar",
            (7, (192, 320), 0.0932): f"{root_url}/bmshj2018-hyperprior-7-3804dcbd.pth.tar",
            (8, (192, 320), 0.1800): f"{root_url}/bmshj2018-hyperprior-8-a583f0cf.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/bmshj2018-hyperprior-ms-ssim-1-5cf249be.pth.tar",
            2: f"{root_url}/bmshj2018-hyperprior-ms-ssim-2-1ff60d1f.pth.tar",
            3: f"{root_url}/bmshj2018-hyperprior-ms-ssim-3-92dd7878.pth.tar",
            4: f"{root_url}/bmshj2018-hyperprior-ms-ssim-4-4377354e.pth.tar",
            5: f"{root_url}/bmshj2018-hyperprior-ms-ssim-5-c34afc8d.pth.tar",
            6: f"{root_url}/bmshj2018-hyperprior-ms-ssim-6-3a6d8229.pth.tar",
            7: f"{root_url}/bmshj2018-hyperprior-ms-ssim-7-8747d3bc.pth.tar",
            8: f"{root_url}/bmshj2018-hyperprior-ms-ssim-8-cc15b5f3.pth.tar",
        },
    },
    "mean_scale_hyperprior": {
        "mse": {
            (1, (128, 192), 0.0018): f"{root_url}/bmshj2018-hyperprior-1-7eb97409.pth.tar",
            (2, (128, 192), 0.0035): f"{root_url}/bmshj2018-hyperprior-2-93677231.pth.tar",
            (3, (128, 192), 0.0067): f"{root_url}/bmshj2018-hyperprior-3-6d87be32.pth.tar",
            (4, (128, 192), 0.0130): f"{root_url}/bmshj2018-hyperprior-4-de1b779c.pth.tar",
            (5, (128, 192), 0.0250): f"{root_url}/bmshj2018-hyperprior-5-f8b614e1.pth.tar",
            # (6, (192, 320), 0.0483): f"{root_url}/bmshj2018-hyperprior-6-1ab9c41e.pth.tar",
            # (7, (192, 320), 0.0932): f"{root_url}/bmshj2018-hyperprior-7-3804dcbd.pth.tar",
            # (8, (192, 320), 0.1800): f"{root_url}/bmshj2018-hyperprior-8-a583f0cf.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/bmshj2018-hyperprior-ms-ssim-1-5cf249be.pth.tar",
            2: f"{root_url}/bmshj2018-hyperprior-ms-ssim-2-1ff60d1f.pth.tar",
            3: f"{root_url}/bmshj2018-hyperprior-ms-ssim-3-92dd7878.pth.tar",
            4: f"{root_url}/bmshj2018-hyperprior-ms-ssim-4-4377354e.pth.tar",
            5: f"{root_url}/bmshj2018-hyperprior-ms-ssim-5-c34afc8d.pth.tar",
            6: f"{root_url}/bmshj2018-hyperprior-ms-ssim-6-3a6d8229.pth.tar",
            7: f"{root_url}/bmshj2018-hyperprior-ms-ssim-7-8747d3bc.pth.tar",
            8: f"{root_url}/bmshj2018-hyperprior-ms-ssim-8-cc15b5f3.pth.tar",
        },
    },
    "mbt2018-mean": {
        "mse": {
            1: f"{root_url}/mbt2018-mean-1-e522738d.pth.tar",
            2: f"{root_url}/mbt2018-mean-2-e54a039d.pth.tar",
            3: f"{root_url}/mbt2018-mean-3-723404a8.pth.tar",
            4: f"{root_url}/mbt2018-mean-4-6dba02a3.pth.tar",
            5: f"{root_url}/mbt2018-mean-5-d504e8eb.pth.tar",
            6: f"{root_url}/mbt2018-mean-6-a19628ab.pth.tar",
            7: f"{root_url}/mbt2018-mean-7-d5d441d1.pth.tar",
            8: f"{root_url}/mbt2018-mean-8-8089ae3e.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/mbt2018-mean-ms-ssim-1-5bf9c0b6.pth.tar",
            2: f"{root_url}/mbt2018-mean-ms-ssim-2-e2a1bf3f.pth.tar",
            3: f"{root_url}/mbt2018-mean-ms-ssim-3-640ce819.pth.tar",
            4: f"{root_url}/mbt2018-mean-ms-ssim-4-12626c13.pth.tar",
            5: f"{root_url}/mbt2018-mean-ms-ssim-5-1be7f059.pth.tar",
            6: f"{root_url}/mbt2018-mean-ms-ssim-6-b83bf379.pth.tar",
            7: f"{root_url}/mbt2018-mean-ms-ssim-7-ddf9644c.pth.tar",
            8: f"{root_url}/mbt2018-mean-ms-ssim-8-0cc7b94f.pth.tar",
        },
    },
    "mbt2018": {
        "mse": {
            1: f"{root_url}/mbt2018-1-3f36cd77.pth.tar",
            2: f"{root_url}/mbt2018-2-43b70cdd.pth.tar",
            3: f"{root_url}/mbt2018-3-22901978.pth.tar",
            4: f"{root_url}/mbt2018-4-456e2af9.pth.tar",
            5: f"{root_url}/mbt2018-5-b4a046dd.pth.tar",
            6: f"{root_url}/mbt2018-6-7052e5ea.pth.tar",
            7: f"{root_url}/mbt2018-7-8ba2bf82.pth.tar",
            8: f"{root_url}/mbt2018-8-dd0097aa.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/mbt2018-ms-ssim-1-2878436b.pth.tar",
            2: f"{root_url}/mbt2018-ms-ssim-2-c41cb208.pth.tar",
            3: f"{root_url}/mbt2018-ms-ssim-3-d0dd64e8.pth.tar",
            4: f"{root_url}/mbt2018-ms-ssim-4-a120e037.pth.tar",
            5: f"{root_url}/mbt2018-ms-ssim-5-9b30e3b7.pth.tar",
            6: f"{root_url}/mbt2018-ms-ssim-6-f8b3626f.pth.tar",
            7: f"{root_url}/mbt2018-ms-ssim-7-16e6ff50.pth.tar",
            8: f"{root_url}/mbt2018-ms-ssim-8-0cb49d43.pth.tar",
        },
    },
    "cheng_anchor": {      
        "mse": {
            (1, (128, 128), 0.0018): f"{root_url}/cheng2020-anchor-1-dad2ebff.pth.tar",
            (2, (128, 128), 0.0035): f"{root_url}/cheng2020-anchor-2-a29008eb.pth.tar",
            (3, (128, 128), 0.0067): f"{root_url}/cheng2020-anchor-3-e49be189.pth.tar",
            (4, (192, 192), 0.0130): f"{root_url}/cheng2020-anchor-4-98b0b468.pth.tar",
            (5, (192, 192), 0.0250): f"{root_url}/cheng2020-anchor-5-23852949.pth.tar",
            (6, (192, 192), 0.0483): f"{root_url}/cheng2020-anchor-6-4c052b1a.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/cheng2020_anchor-ms-ssim-1-20f521db.pth.tar",
            2: f"{root_url}/cheng2020_anchor-ms-ssim-2-c7ff5812.pth.tar",
            3: f"{root_url}/cheng2020_anchor-ms-ssim-3-c23e22d5.pth.tar",
            4: f"{root_url}/cheng2020_anchor-ms-ssim-4-0e658304.pth.tar",
            5: f"{root_url}/cheng2020_anchor-ms-ssim-5-c0a95e77.pth.tar",
            6: f"{root_url}/cheng2020_anchor-ms-ssim-6-f2dc1913.pth.tar",
        },
    },
    "cheng2020-attn": {
        "mse": {
            1: f"{root_url}/cheng2020_attn-mse-1-465f2b64.pth.tar",
            2: f"{root_url}/cheng2020_attn-mse-2-e0805385.pth.tar",
            3: f"{root_url}/cheng2020_attn-mse-3-2d07bbdf.pth.tar",
            4: f"{root_url}/cheng2020_attn-mse-4-f7b0ccf2.pth.tar",
            5: f"{root_url}/cheng2020_attn-mse-5-26c8920e.pth.tar",
            6: f"{root_url}/cheng2020_attn-mse-6-730501f2.pth.tar",
        },
        "ms-ssim": {
            1: f"{root_url}/cheng2020_attn-ms-ssim-1-c5381d91.pth.tar",
            2: f"{root_url}/cheng2020_attn-ms-ssim-2-5dad201d.pth.tar",
            3: f"{root_url}/cheng2020_attn-ms-ssim-3-5c9be841.pth.tar",
            4: f"{root_url}/cheng2020_attn-ms-ssim-4-8b2f647e.pth.tar",
            5: f"{root_url}/cheng2020_attn-ms-ssim-5-5ca1f34c.pth.tar",
            6: f"{root_url}/cheng2020_attn-ms-ssim-6-216423ec.pth.tar",
        },
    },
    "nic": {
        "mse": {
            1: f"{root_url_nic}/mse/nic_mse_1.pt",
            2: f"{root_url_nic}/mse/nic_mse_2.pt",
            3: f"{root_url_nic}/mse/nic_mse_3.pt",
            4: f"{root_url_nic}/mse/nic_mse_4.pt",
            5: f"{root_url_nic}/mse/nic_mse_5.pt",
            6: f"{root_url_nic}/mse/nic_mse_6.pt",
            7: f"{root_url_nic}/mse/nic_mse_7.pt",
            8: f"{root_url_nic}/mse/nic_mse_8.pt",
        },
        "ms-ssim": {
            1: f"{root_url_nic}/ms-ssim/nic_ms-ssim_1.pt",
            2: f"{root_url_nic}/ms-ssim/nic_ms-ssim_2.pt",
            3: f"{root_url_nic}/ms-ssim/nic_ms-ssim_3.pt",
            4: f"{root_url_nic}/ms-ssim/nic_ms-ssim_4.pt",
            5: f"{root_url_nic}/ms-ssim/nic_ms-ssim_5.pt",
            6: f"{root_url_nic}/ms-ssim/nic_ms-ssim_6.pt",
            7: f"{root_url_nic}/ms-ssim/nic_ms-ssim_7.pt",
            8: f"{root_url_nic}/ms-ssim/nic_ms-ssim_8.pt",
        },
    },
}

cfgs = {
    "factorized-prior": {
        1: (128, 192),
        2: (128, 192),
        3: (128, 192),
        4: (128, 192),
        5: (128, 192),
        6: (192, 320),
        7: (192, 320),
        8: (192, 320),
    },
    "bmshj2018-factorized-relu": {
        1: (128, 192),
        2: (128, 192),
        3: (128, 192),
        4: (128, 192),
        5: (128, 192),
        6: (192, 320),
        7: (192, 320),
        8: (192, 320),
    },
    "scale_hyperprior": {
        1: [(128, 192), 0.0018],
        2: [(128, 192), 0.0035],
        3: [(128, 192), 0.0067],
        4: [(128, 192), 0.0130],
        5: [(128, 192), 0.0250],
        6: [(192, 320), 0.0483],
        7: [(192, 320), 0.0932],
        8: [(192, 320), 0.1800]
    },
    "mean_scale_hyperprior": {
        1: [(128, 192), 0.0018],
        2: [(128, 192), 0.0035],
        3: [(128, 192), 0.0067],
        4: [(128, 192), 0.0130],
        5: [(128, 192), 0.0250],
        # 6: [(192, 320), 0.0483],
        # 7: [(192, 320), 0.0932],
        # 8: [(192, 320), 0.1800]
    },
    "mbt2018-mean": {
        1: (128, 192),
        2: (128, 192),
        3: (128, 192),
        4: (128, 192),
        5: (192, 320),
        6: (192, 320),
        7: (192, 320),
        8: (192, 320),
    },
    "mbt2018": {
        1: (192, 192),
        2: (192, 192),
        3: (192, 192),
        4: (192, 192),
        5: (192, 320),
        6: (192, 320),
        7: (192, 320),
        8: (192, 320),
    },
    "cheng_anchor": {
        1: [(128, 128), 0.0018],
        2: [(128, 128), 0.0035],
        3: [(128, 128), 0.0067],
        4: [(192, 192), 0.0130],
        5: [(192, 192), 0.0250],
        6: [(192, 192), 0.0483],
    },
    "cheng2020-attn": {
        1: (128,),
        2: (128,),
        3: (128,),
        4: (192,),
        5: (192,),
        6: (192,),
    },
    "nic": {
        1: (128, 320),
        2: (128, 320),
        3: (128, 320),
        4: (128, 320),
        5: (128, 320),
        6: (128, 320),
        7: (128, 320),
        8: (128, 320),
    },
}

