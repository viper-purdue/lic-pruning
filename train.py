from pathlib import Path
from tqdm import tqdm
from collections import defaultdict, OrderedDict
from PIL import Image
import os
import sys
import json
import time
import math
import argparse
import numpy as np
import random
import torch
import torch.cuda.amp as amp
import torch.nn.functional as tnf
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.optim as optim
from torch.nn.utils import prune
from torch.hub import load_state_dict_from_url
import compressai

import torchvision as tv
import timm
import timm.utils
from pytorch_msssim import ms_ssim

from compressai.datasets import Vimeo90kDataset
from compressai.zoo.pretrained import load_pretrained

from utils import get_trainloader, get_valloader, ImageDataset, increment_dir, SimpleTable, get_cosine_lrf, pad, crop, get_model_size
from models.vae import FactorizedPrior, ScaleHyperprior, Cheng2020Anchor, model_urls, cfgs
from models.registry import get_model
from models.quantization import BIT_TYPE_DICT
from channel_independence import CHIP, RANK
from model_pruner import prune_model, prune_remove, prune_module
from models.quantization.quant_layer import QuantModule
from models.quantization.quant_model import QuantModel


DATA_DIR = Path('/home/jackfruit/a/hossai34/datasets/coco/images')                     # Path('/pub/hossai34/datasets/vimeo_septuplet'), 'datasets/CIFAR100'


def compute_psnr(a, b):
    mse = tnf.mse_loss(a, b)
    return -10 * math.log10(mse)

def compute_msssim(a, b):
    return ms_ssim(a, b, data_range=1., win_size=3,  win_sigma=1).item()

def compute_loss(x, x_hat, likelihoods, bpp_lmb):
    # bitrate of the quantized latent
    nB, _, H, W = x.size()
    bpp_loss = sum(
            (-1.0 * torch.log2(likelihood).sum(dim=(1, 2, 3)) / float(H * W))
            for likelihood in likelihoods.values()
        )
    # mean square error
    mse_loss = tnf.mse_loss(x, x_hat)
    # final loss term
    loss = bpp_lmb*(255**2)*mse_loss + bpp_loss.mean()

    stats = OrderedDict()
    stats['loss'] = loss
    stats['bppix'] = bpp_loss.mean().item()
    stats['MSE'] = mse_loss.item()
    stats['PSNR'] = compute_psnr(x, x_hat)
    stats['MS_SSIM'] = compute_msssim(x, x_hat)

    return stats

@torch.no_grad()
def evaluate_model(model: torch.nn.Module, testloader):
    """ Image classification evaluation with a testloader.

    Args:
        model (torch.nn.Module): pytorch model
        testloader (torch.utils.data.Dataloader): test dataloader
    """
    device = next(model.parameters()).device
    dtype = next(model.parameters()).dtype

    stats_avg_meter = defaultdict(timm.utils.AverageMeter)
    print(f'Evaluating {type(model)}, device={device}, dtype={dtype}')
    print(f'batch_size={testloader.batch_size}, num_workers={testloader.num_workers}')
    pbar = tqdm(testloader)
    
    for imgs in pbar:
        # sanity check
        imgs: torch.FloatTensor
        assert (imgs.dim() == 4)
        
        imgs = imgs.to(device=device, dtype=dtype)
        nB, _, imH, imW = imgs.shape

        # forward pass, get prediction
        out_dict = model(imgs)
        rec_imgs = out_dict["x_hat"]
        bpp_lmb = out_dict["lmb"]
        
        stats = compute_loss(imgs, rec_imgs, out_dict["likelihoods"], bpp_lmb)
        for k, v in stats.items():
            stats_avg_meter[k].update(float(v), n=nB)

        # logging
        msg = ''.join([f'{k}={v.avg:.4g}, ' for k,v in stats_avg_meter.items()])            # stats_avg_meter is a dict with keys ('top1', 'top5') and values (timm.utils.AverageMeter, timm.utils.AverageMeter)
        pbar.set_description(msg)                                                           # each timm.utils.AverageMeter contains four attributes val, avg, sum, count
    pbar.close()

    # compute total statistics and return
    _random_key = list(stats_avg_meter.keys())[0]
    assert stats_avg_meter[_random_key].count == len(testloader.dataset)
    results = {k: v.avg for k,v in stats_avg_meter.items()}                         # dict {'top1':top1_acc_value, 'top5':top5_acc_value}
    return results


def get_config():
    # ====== set the run settings ======
    parser = argparse.ArgumentParser()
    # wandb setting
    parser.add_argument('--wbproject',  type=str,  default='model-compression-ICIP')
    parser.add_argument('--wbgroup',    type=str,  default='pruning-quantization-image')
    parser.add_argument('--wbmode',     type=str,  default='online')
    # model setting
    parser.add_argument('--model',      type=str,  default='cheng_anchor_quantized')
    parser.add_argument('--model_args', type=str,  default='')
    parser.add_argument('--model_quality', type=int,  default='1')
    # resume setting
    parser.add_argument('--resume',     type=str,  default='')
    parser.add_argument('--pretrained_compressai',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--pretrained',   action=argparse.BooleanOptionalAction, default=False)
    #pruning settings
    parser.add_argument('--prune_model',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--prune_NAS',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--prune_channels',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--prune_quantize',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('-r', '--prune_ratio', type=float, default=0.2)
    parser.add_argument('--hyper_ratio', type=float, default=0.2)
    parser.add_argument('--alpha_init', type=float, default=5)
    parser.add_argument('--prune_block', type=float, default=20)
    parser.add_argument('--prune_type', type=str, default="unstructured", choices=['unstructured', 'structured', 'random_structured', 'CHIP', 'RANK'])
    parser.add_argument('--repeat', type=int, default=2)
    parser.add_argument('--prune_batch', type=int, default=5)
    #quanitzation settings
    parser.add_argument('--quantize_pretrained',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--quantize_model',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--wquant_calibration_mode', type=str, default="channel_wise", choices=['layer_wise', 'channel_wise'])
    parser.add_argument('--wquantizer_type', type=str, default="scaled-lsq", choices=['lsq', 'uniform', 'quant-noise-lsq', 'scaled-lsq'])
    parser.add_argument('--wquant_scale_method', type=str, default="minmax", choices=['minmax', 'ema', 'mse'])
    parser.add_argument('--aquant_calibration_mode', type=str, default="channel_wise", choices=['layer_wise', 'channel_wise'])
    parser.add_argument('--aquantizer_type', type=str, default="scaled-lsq", choices=['lsq', 'uniform', 'quant-noise-lsq', 'scaled-lsq'])
    parser.add_argument('--aquant_scale_method', type=str, default="minmax", choices=['minmax', 'ema', 'mse'])
    parser.add_argument('--wquant_bits', type=str, default="uint8")
    parser.add_argument('--aquant_bits', type=str, default="uint8")
    parser.add_argument('--dynamic_aquant',   action=argparse.BooleanOptionalAction, default=False)                                                         
    parser.add_argument('--MPQ',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--limit-8-bit-quant',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--MPQ_threshold', type=float, default=2)
    parser.add_argument('--CR', type=float, default=0.6)
    parser.add_argument('--optim_params', type=str, default=None, choices=['qparams', 'netparams', 'qparams_netparams'])
    parser.add_argument('--qlr',         type=float,default=1e-5) 
    parser.add_argument('--prune_quant_switch', type=int, default=-1)
    # training setting
    parser.add_argument('-c', '--ckpt_name', type=str, default='scale_hyperprior_7_uncompressed')
    parser.add_argument('--train_size', type=int,  default=256)
    parser.add_argument('--gpu_id', type=int, default=0)
    # evaluation setting
    parser.add_argument('--val_size',   type=int,  default=256)
    parser.add_argument('--val_crop_r', type=float,default=1)
    # optimization setting
    parser.add_argument('--batch_size', type=int,  default=16)
    parser.add_argument('--accum_num',  type=int,  default=1)
    parser.add_argument('--optimizer',  type=str,  default='adam', choices=['adam', 'sgd'])
    parser.add_argument('--lr',         type=float,default=1e-5)            # 0.01   # 1e-4     # 1e-5 for finetuning
    # parser.add_argument('--aux_lr',     type=float,default=1e-3)
    parser.add_argument('--lr_sched',   type=str,  default='')                # 'cosine', 'plateau'
    parser.add_argument('--wdecay',     type=float,default=0.0001)
    # training policy setting
    parser.add_argument('--epochs',     type=int,  default=60)
    parser.add_argument('--amp',        action=argparse.BooleanOptionalAction, default=False)                # what is argparse.BooleanOptionalAction
    # miscellaneous training setting
    parser.add_argument('--eval_first', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--eval_per',   type=int,  default=1)
    # device setting
    parser.add_argument('--fixseed',    action='store_true')
    parser.add_argument('--workers',    type=int,  default=4)
    parser.add_argument('--ddp_find',   action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--test_script',   action=argparse.BooleanOptionalAction, default=False)
    cfg = parser.parse_args()

    # optimizer
    cfg.momentum = 0.9

    return cfg


class TrainWrapper():
    def __init__(self) -> None:
        pass

    def set_device_(self):
        cfg = self.cfg

        local_rank = int(os.environ.get('LOCAL_RANK', -1))
        world_size = int(os.environ.get('WORLD_SIZE', 1))
        _count = torch.cuda.device_count()

        if world_size == 1: # standard mode
            assert local_rank == -1
            print(f'Visible devices={_count}, using idx 0:', torch.cuda.get_device_properties(0), '\n')
            device = torch.device('cuda', cfg.gpu_id)
            # device = 'cpu'
            is_main = True
            distributed = False
        else: # DDP mode
            assert local_rank >= 0
            assert torch.distributed.is_nccl_available()
            torch.distributed.init_process_group(backend="nccl")
            assert local_rank == torch.distributed.get_rank()
            assert world_size == torch.distributed.get_world_size()

            torch.distributed.barrier()
            device = torch.device('cuda', local_rank)
            is_main = (local_rank == 0)
            distributed = True

        if cfg.fixseed: # fix random seeds for reproducibility
            timm.utils.random_seed(2 + local_rank)
        torch.backends.cudnn.benchmark = True

        if is_main:
            print(f'Batch size on each dataloader (ie, GPU) = {cfg.batch_size}')
            print(f'Gradient accmulation: {cfg.accum_num} backwards() -> one step()')
            bs_effective = cfg.batch_size * world_size * cfg.accum_num
            msg = f'Effective batch size = {bs_effective}, learning rate = {cfg.lr}, ' + \
                  f'weight decay = {cfg.wdecay}'
            print(msg)

        cfg.world_size   = world_size
        self.device      = device
        self.local_rank  = local_rank
        self.is_main     = is_main
        self.distributed = distributed

    def set_dataset_(self):
        cfg = self.cfg

        if self.is_main:
            print('Initializing Datasets and Dataloaders...')
        train_split = 'train2017'
        val_split = 'val2017'
        calib_split = 'calib2017'

        train_dir = os.path.join(DATA_DIR, train_split)
        val_dir = os.path.join(DATA_DIR, val_split)
        calib_dir = os.path.join(DATA_DIR, calib_split)

        trainloader = get_trainloader(root_dir=train_dir, img_size=cfg.train_size, batch_size=cfg.batch_size, workers=cfg.workers, distributed=self.distributed)
        pruneloader = get_trainloader(root_dir=train_dir, img_size=cfg.train_size, batch_size=cfg.prune_batch, workers=cfg.workers, distributed=self.distributed)
        quantloader = get_trainloader(root_dir=calib_dir, img_size=cfg.train_size, batch_size=1, workers=cfg.workers, distributed=self.distributed)
        # num_classes = len(trainloader.dataset.classes)

        if self.is_main: # test set
            # print(f'Number of classes = {num_classes}')
            print(f'Training root: {train_dir}')
            print('Training transform:', trainloader.dataset.transform)
            valloader = get_valloader(root_dir=val_dir, img_size=cfg.val_size, crop_ratio=cfg.val_crop_r, batch_size=cfg.batch_size//4, workers=cfg.workers//2)
            print(f'Val root: {val_dir}')         
        else:
            valloader = None

        self.trainloader = trainloader
        self.valloader  = valloader
        self.pruneloader = pruneloader
        self.quantloader = quantloader
        # self.cfg.num_classes = num_classes

    def set_model_(self):
        cfg = self.cfg
        
        if 'quantized' in cfg.model:
            name = '_'.join(cfg.model.split('_')[:-1])
        else:
            name = cfg.model
        (N, M), bpp_lmb = cfgs[name][cfg.model_quality]
        model_func = get_model(cfg.model)
        if not cfg.quantize_model:
            model = model_func(N=N, M=M, bpp_lmb=bpp_lmb)  
            prune_model =  model_func(N=N, M=M, bpp_lmb=bpp_lmb)             
            if cfg.pretrained_compressai:
                model_url = model_urls[model.name]["mse"][(cfg.model_quality, (N, M), bpp_lmb)]
                state_dict = load_state_dict_from_url(model_url, progress=True)
                state_dict = load_pretrained(state_dict)
                model.load_state_dict(state_dict, strict=True)   
                prune_model.load_state_dict(state_dict, strict=True)                                                                             
            elif cfg.pretrained:
                checkpoint_root = Path(f'checkpoints/{name}')
                checkpoint_paths = list(checkpoint_root.rglob('*.pt'))
                checkpoint_paths.sort()
                ckptpath = checkpoint_paths[cfg.model_quality-1]
                print(ckptpath)
                print(f"Quality: {cfg.model_quality}", ckptpath)
                checkpoint = torch.load(ckptpath)
                self.prune_checkpoint = checkpoint
                model.load_state_dict(checkpoint['model'], strict=True)
                prune_model.load_state_dict(checkpoint['model'], strict=True) 
            self.model = model.to(self.device)                  
            self.prune_model = prune_model.to(self.device) 
            if cfg.prune_quantize: 
                wq_params = {'scale_method': cfg.wquant_scale_method, 
                            'bit_type': BIT_TYPE_DICT[cfg.wquant_bits], 
                            'calibration_mode': cfg.wquant_calibration_mode, 
                            'quantizer_type': cfg.wquantizer_type}
                aq_params = {'scale_method': cfg.aquant_scale_method, 
                            'bit_type': BIT_TYPE_DICT[cfg.aquant_bits], 
                            'calibration_mode': cfg.aquant_calibration_mode, 
                            'quantizer_type': cfg.aquantizer_type, 
                            'dynamic': cfg.dynamic_aquant}
                quant_model = QuantModel(model=self.model, weight_quant_params=wq_params, act_quant_params=aq_params)
                self.model = quant_model
                # model = self.model
                # print(type(model))
                # self.quantize_model_()
                # model.set_quant_state(False, False)
            
        else:
            wq_params = {'scale_method': cfg.wquant_scale_method, 
                         'bit_type': BIT_TYPE_DICT[cfg.wquant_bits], 
                         'calibration_mode': cfg.wquant_calibration_mode, 
                         'quantizer_type': cfg.wquantizer_type}
            aq_params = {'scale_method': cfg.aquant_scale_method, 
                         'bit_type': BIT_TYPE_DICT[cfg.aquant_bits], 
                         'calibration_mode': cfg.aquant_calibration_mode, 
                         'quantizer_type': cfg.aquantizer_type, 
                         'dynamic': cfg.dynamic_aquant}
            model = model_func(N, M, bpp_lmb, wq_params, aq_params, cfg.model_quality, device=self.device, pretrained=cfg.quantize_pretrained)
            self.model = model.to(self.device)

        if self.distributed: # DDP mode
            self.model = DDP(model, device_ids=[self.local_rank], output_device=self.local_rank,
                             find_unused_parameters=cfg.ddp_find)


    def set_optimizer_(self):
        cfg, model = self.cfg, self.model
        
        for name, param in model.named_parameters():
            param.requires_grad = True
        pg_info = defaultdict(list)
        for k, v in model.named_parameters():
            assert isinstance(k, str) and isinstance(v, torch.Tensor)
            pg_info['weights'].append(f'{k:<80s} {v.shape}')

        for name, param in model.named_parameters():
            if param.requires_grad:
                print(name)
                
        qparams = [p for n, p in model.named_parameters() if "quantizer" in n]
        # names = [n for n, p in model.named_parameters() if "quantizer" in n]
        # print(names)
        print(f'Number of quantization parameters:  {len(qparams)}')
        netparams = [p for n, p in model.named_parameters() if "quantizer" not in n]
        print(f'Number of network parameters:  {len(netparams)}')

        # optimizer
        if cfg.optimizer == 'sgd':
            if cfg.optim_params == "qparams_netparams":
                optimizer = torch.optim.SGD([{'params': netparams},{'params': qparams, 'lr': cfg.qlr}], lr=cfg.lr, momentum=cfg.momentum)
            elif cfg.optim_params == "qparams":
                cfg.lr = cfg.qlr
                optimizer = torch.optim.SGD(qparams, lr=cfg.lr, momentum=cfg.momentum)
            else:
                optimizer = torch.optim.SGD(netparams, lr=cfg.lr, momentum=cfg.momentum)
        elif cfg.optimizer == 'adam':
            if cfg.optim_params == "qparams_netparams":
                optimizer = torch.optim.Adam([{'params': netparams},{'params': qparams, 'lr': cfg.qlr}], lr=cfg.lr)
            elif cfg.optim_params == "qparams":
                cfg.lr = cfg.qlr
                optimizer = torch.optim.Adam(qparams, lr=cfg.lr)
            else:
                optimizer = torch.optim.Adam(netparams, lr=cfg.lr)
        else:
            raise ValueError(f'Unknown optimizer: {cfg.optimizer}')


        if self.is_main:
            print('optimizer parameter groups:', *[f'[{k}: {len(pg)}]' for k, pg in pg_info.items()])
            self.pg_info_to_log = pg_info

        self.optimizer = optimizer
        self.scaler = amp.GradScaler(enabled=cfg.amp) # Automatic mixed precision
        if cfg.lr_sched == 'plateau':
            self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, mode='min', factor=0.1, patience=5)

    def set_logging_dir_(self):
        cfg = self.cfg

        prev_loss = 1e8
        log_parent = Path(f'runs/{cfg.wbproject}')
        if cfg.resume: # resume
            run_name = cfg.resume
            log_dir = log_parent / run_name
            assert log_dir.is_dir(), f'Try to resume from {log_dir} but it does not exist'
            ckpt_path = log_dir / 'last.pt'
            checkpoint = torch.load(ckpt_path)
            # print(type(self.model))
            if self.distributed:
                self.model.module.load_state_dict(checkpoint['model'], strict=True)
            else:
                self.model.load_state_dict(checkpoint['model'], strict=True)
            self.optimizer.load_state_dict(checkpoint['optimizer'])
            if cfg.amp:
                self.scaler.load_state_dict(checkpoint['scaler'])
            if cfg.lr_sched == 'plateau':
                self.scheduler.load_state_dict(checkpoint["lr_scheduler"])
            start_epoch = checkpoint['epoch']
            prev_result = checkpoint.get('results', None)
            prev_loss = prev_result['loss'] if prev_result is not None else prev_loss
            if self.is_main:
                print(f'Resuming run {log_dir}. Loaded checkpoint from {ckpt_path}.',
                      f'Epoch={start_epoch}, results={prev_result}')
        elif cfg.prune_model:
            if cfg.prune_NAS:
                if cfg.prune_channels:
                    _base = f'{cfg.model}-quality_{cfg.model_quality}-{cfg.prune_type}-{cfg.prune_ratio}_filters+channels-NAS-pruned'
                else:
                    _base = f'{cfg.model}-quality_{cfg.model_quality}-{cfg.prune_type}-{cfg.prune_ratio}-NAS-pruned'
            else:
                if cfg.prune_channels:
                    _base = f'{cfg.model}-quality_{cfg.model_quality}-{cfg.prune_type}-{cfg.prune_ratio}_filters+channels-pruned'
                else:
                    _base = f'{cfg.model}-quality_{cfg.model_quality}-{cfg.prune_type}-{cfg.prune_ratio}-pruned'
            if cfg.prune_quantize:
                _base = _base + "-quantized"
            if cfg.test_script:
                run_name = "test_run"
            else:
                run_name = increment_dir(dir_root=log_parent, name=_base)
            self.run_name = run_name
            print(f"MODEL WILL BE SAVED AS: {run_name}")
            log_dir = log_parent / run_name
            if self.is_main:
                if cfg.test_script:
                    os.makedirs(log_dir, exist_ok=True)
                else:
                    os.makedirs(log_dir, exist_ok=False)
                print(str(self.model), file=open(log_dir / 'model.txt', 'w'))
                json.dump(cfg.__dict__, fp=open(log_dir / 'config.json', 'w'), indent=2)
                json.dump(self.pg_info_to_log, fp=open(log_dir / 'optimizer.json', 'w'), indent=2)
                print('Training config:\n', cfg, '\n')
            start_epoch = 0
        else: # new experiment
            _base = f'{cfg.model}-quality_{cfg.model_quality}'
            if cfg.test_script:
                run_name = "test_run"
            else:
                run_name = increment_dir(dir_root=log_parent, name=_base)
            log_dir = log_parent / run_name # logging dir
            print(f"MODEL WILL BE SAVED AS: {run_name}")
            self.run_name = run_name
            if self.is_main:
                if cfg.test_script:
                    os.makedirs(log_dir, exist_ok=True)
                else:
                    os.makedirs(log_dir, exist_ok=False)
                print(str(self.model), file=open(log_dir / 'model.txt', 'w'))
                json.dump(cfg.__dict__, fp=open(log_dir / 'config.json', 'w'), indent=2)
                json.dump(self.pg_info_to_log, fp=open(log_dir / 'optimizer.json', 'w'), indent=2)
                print('Training config:\n', cfg, '\n')
            start_epoch = 0

        if hasattr(self.model, "bpp_lmb"):
            print(f"Model is training with lambda value: {self.model.bpp_lmb}")
        cfg.log_dir = str(log_dir)
        self._log_dir     = log_dir
        self._start_epoch = start_epoch
        self._best_loss   = prev_loss
    
    def prune_model_(self):
        cfg = self.cfg

        if cfg.prune_channels:
            print(f'Pruning model {cfg.model}-quality_{cfg.model_quality}-type_{cfg.prune_type}-ratio_{cfg.prune_ratio}_filters+channels')
        else:
            print(f'Pruning model {cfg.model}-quality_{cfg.model_quality}-type_{cfg.prune_type}-ratio_{cfg.prune_ratio}')
        model = self.model
        model.eval()
        results = self.evaluate(-1, -1) 
        print('Results before pruning:', results)
        start_time = time.time()
        first_module_name, last_module_name = prune_model(model, cfg.prune_type, cfg.prune_ratio, cfg.repeat, self.pruneloader, self.device, prune_channels=cfg.prune_channels)
        self.first_module_name = first_module_name
        self.last_module_name = last_module_name
        end_time = time.time()
        print(f"Total time to prune model: {int((end_time-start_time)//3600)} hour(s) {round((end_time-start_time)/60 - float((end_time-start_time)//3600)*60)} minute(s)")
        print("Sparsity in conv1.weight: {:.2f}%".format(100. * float(torch.sum(self.model.get_submodule(first_module_name).weight == 0))/ float(self.model.get_submodule(first_module_name).weight.nelement())))
        results = self.evaluate(-1, -1) 
        print('Results after pruning:', results)
        if self.is_main:
            print(f'Pruning complete. Finetuning model {cfg.model}-quality_{cfg.model_quality}-type_{cfg.prune_type}-ratio_{cfg.prune_ratio}')
        if cfg.quantize_model:
            model.set_quant_state(False, False)
    
    def prune_NAS_(self):
        cfg = self.cfg
        MODELS_TO_PRUNE = ["Conv2d", "ConvTranspose2d", "Linear"]
        prune_ratio_list = np.arange(0.1, 0.8, 0.05)
        
        if cfg.prune_channels:
            # prune_ratio_list = np.arange(0.1, 0.85, 0.05)
            # prune_ratio_list = prune_ratio_list / 2
            prune_ratio_list = np.array([0.065, 0.095, 0.125, 0.165, 0.195, 0.225, 0.265, 0.295, 0.325, 0.365])    # [0.215, 0.235, 0.255, 0.275, 0.295, 0.315, 0.335, 0.355, 0.375, 0.395]                     #  [0.165, .195, 0.225, 0.265, 0.295, 0.325, 0.365, 0.395, 0.425], [0.265, 0.295, 0.325, 0.365, 0.395, 0.425, 0.465, 0.495, 0.525, 0.565]
        
        val_split = 'calib2017'
        calib_dir = os.path.join(DATA_DIR, val_split)
        transform = [tv.transforms.CenterCrop(cfg.val_size),  tv.transforms.ToTensor()]
        transform = tv.transforms.Compose(transform)
        dataset = ImageDataset(calib_dir, transform=transform)
        dataloader = torch.utils.data.DataLoader(dataset, batch_size=32, shuffle=False, num_workers=cfg.workers//2, pin_memory=True, drop_last=False)
    
        print(f'Pruning model {cfg.model}-quality_{cfg.model_quality}-type_{cfg.prune_type}-ratio_NAS')
        model = self.model
        prune_model = self.prune_model
        model.eval()
        results = evaluate_model(model, dataloader) 
        loss = results['loss']
        print('Results before pruning:', results)
        first_module_name = None
        for name, module in prune_model.named_modules():
            if isinstance(module, QuantModule):
                if module.org_module_type in MODELS_TO_PRUNE:
                    if 'g_s' in name:
                        last_module_name = name
                if module.org_module_type == "Conv2d" and first_module_name == None:
                    first_module_name = name
            elif isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
                if 'g_s' in name:
                    last_module_name = name
                if isinstance(module, torch.nn.Conv2d) and first_module_name == None:
                    first_module_name = name
        print("Last module is:  ", last_module_name)
        
        start_time = time.time()
        print("Loss before pruning: ", loss)
        loss = loss - (2.5/100) * loss
        
        k = 0
        prune_ratio_per_module = []
        for name, module in prune_model.named_modules():
            if name != last_module_name and name != first_module_name:
                if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
                    Cout, Cin, h, w = module.weight.shape
                    print(f"Step: {k}   \t  Module being pruned: {name} \t  SHAPE: {Cout}, {Cin}, {h}, {w}")
                    k += 1
                    prune_ratio_per_module.append(0)
        print(prune_ratio_per_module)
        print(len(prune_ratio_list))
        
        for name, param in prune_model.named_parameters():
            param.requires_grad = True
        
        sparsity_dict = {}
        alpha = cfg.alpha_init     # delta_loss around 5   
        alpha_adjust = 0.5 
        while True:
            i = -1
            for name, module in prune_model.named_modules():  
                if name != last_module_name and name != first_module_name:
                    if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
                        i += 1
                        print(f"Step: {i}")
                        print("Module pruned:  ", name, "   Type of module: ", type(module))
                        # if 'h' in name:
                        if 'g' in name:
                            epochs = 30
                        else:
                            epochs = 15
                        prune_ratio_prev = prune_ratio_list[0]
                        prune_ratio_per_module[i] = prune_ratio_list[0]
                        patience = 0
                        for prune_ratio in prune_ratio_list:
                            prune_model.load_state_dict(self.prune_checkpoint['model'], strict=True) 
                            params = [p for n, p in prune_model.named_parameters() if p.requires_grad]
                            optimizer = torch.optim.Adam(params, lr=1e-4)
                            prune_module(prune_model, name, cfg.prune_type, prune_ratio, 1, self.pruneloader, self.device, prune_channels=cfg.prune_channels)
                            prune_model.train()
                            for _ in tqdm(range(epochs)):
                                for imgs in dataloader:
                                    imgs = imgs.to(device=self.device)
                                    nB, _, imH, imW = imgs.shape
                                    out_dict = prune_model(imgs)
                                    rec_imgs = out_dict["x_hat"]
                                    bpp_lmb = out_dict["lmb"]
                                    stats = compute_loss(imgs, rec_imgs, out_dict["likelihoods"], bpp_lmb)
                                    train_loss = stats["loss"]
                                    train_loss.backward()
                                    optimizer.step()
                                    optimizer.zero_grad()
                            
                            prune_model.eval()
                            results = evaluate_model(prune_model, dataloader)
                            prune_loss = results['loss']
                            delta_loss = (np.abs(loss-prune_loss)/loss) * 100
                            diff_loss = prune_loss - loss
                            print(f"Prune ratio:    {prune_ratio},  prune_loss: {prune_loss},   delta_loss: {delta_loss},   diff_loss: {diff_loss},     zeros: {float(torch.sum(prune_model.get_submodule(name).weight == 0))}")
                            print("Sparsity in weight: {:.2f}%".format(100. * float(torch.sum(prune_model.get_submodule(name).weight == 0))/ float(prune_model.get_submodule(name).weight.nelement())))
                            prune_remove(module, last_module_name, first_module_name)
                            
                            if delta_loss < alpha:
                                print(f"Pruning ratio for layer {name}: {prune_ratio_prev}")
                                prune_ratio_per_module[i] = prune_ratio
                            else:
                                patience += 1
                                if patience == 3:
                                    break
                        
            prune_model.load_state_dict(self.prune_checkpoint['model'], strict=True)
            i = 0
            for name, module in prune_model.named_modules():
                if name != last_module_name and name != first_module_name:
                    if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
                        prune_module(prune_model, name, cfg.prune_type, prune_ratio_per_module[i], 1, self.pruneloader, self.device, prune_channels=cfg.prune_channels)
                        i += 1
            prune_remove(prune_model, last_module_name, first_module_name)                        
            
            self.first_module_name = first_module_name
            self.last_module_name = last_module_name
            end_time = time.time()
            print(f"Total time to prune model: {int((end_time-start_time)//3600)} hour(s) {round((end_time-start_time)/60 - float((end_time-start_time)//3600)*60)} minute(s)")
            print(f'Pruning ratios: {prune_ratio_per_module}')  
            
            ####################################### Adaptive Search ########################################
            uncompressed_model_size = get_model_size(prune_model, model_type="full_precision")
            model_size = get_model_size(prune_model, model_type="full_precision", pruned_model=True)
            sparsity_current = 1 - model_size/uncompressed_model_size
            
            if np.abs(sparsity_current - cfg.prune_ratio) <= 0.05:
                print(f"Sparsity: {sparsity_current}")
                print(f"Target sparsity has been achieved with a threshold value of {alpha}.")
                # json.dump(cfg.__dict__, fp=open(self._log_dir / 'config_new.json', 'w'), indent=2)
                sparsity_dict[alpha] = f"sparsity={float(round(sparsity_current, 3))},   apha_adjust={alpha_adjust}"                               # (float(round(CR_current, 3)), beta_adjust)
                json.dump(sparsity_dict, fp=open(self._log_dir / 'sparsity_alpha.json', 'w'), indent=2)
                break
            elif sparsity_current >= cfg.prune_ratio:
                print(f"Sparsity: {sparsity_current}")
                print(f"Target sparsity has not been achieved with a threshold value of {alpha}.")
                # json.dump(cfg.__dict__, fp=open(self._log_dir / 'config_new.json', 'w'), indent=2)
                sparsity_dict[alpha] = f"sparsity={float(round(sparsity_current, 3))},   alpha_adjust={alpha_adjust}"                          # (float(round(CR_current, 3)), beta_adjust)
                json.dump(sparsity_dict, fp=open(self._log_dir / 'sparsity_alpha.json', 'w'), indent=2)
                alpha -= alpha_adjust
                alpha = np.max((alpha, 0.001))
                alpha_adjust *= 0.1
                alpha += alpha_adjust
                print(f"Threshold value is increased to {alpha}.")
            else:
                if np.abs(sparsity_current - cfg.prune_ratio) >= .15:
                    alpha_adjust *= 5
                elif np.abs(sparsity_current - cfg.prune_ratio) >= .05:
                    alpha_adjust *= 2
                print(f"Sparsity: {sparsity_current}")
                sparsity_dict[alpha] = f"sparsity={float(round(sparsity_current, 3))},   alpha_adjust={alpha_adjust}"                             # (float(round(CR_current, 3)), beta_adjust)
                json.dump(sparsity_dict, fp=open(self._log_dir / 'sparsity_alpha.json', 'w'), indent=2)
                print(f"Target sparsity has not been achieved with a threshold value of {alpha}.")
                alpha += alpha_adjust
                print(f"Threshold value is increased to {alpha}.")  
            ####################################### Adaptive Search ########################################
        
        prune_ratio_dict = {}
        for i, r in enumerate(prune_ratio_per_module):
            prune_ratio_dict[i] = r
        json.dump(prune_ratio_dict, fp=open(self._log_dir / 'prune_ratios.json', 'w'), indent=2)
        
        i = 0
        print(type(model))
        if cfg.prune_quantize:    
            last_module_name = "model."+last_module_name
            first_module_name = "model."+first_module_name
            self.last_module_name = last_module_name
            self.first_module_name = first_module_name
        for name, module in model.named_modules():
            if name != last_module_name and name != first_module_name:
                if getattr(module, 'org_module_type', 0)=="Conv2d" or getattr(module, 'org_module_type', 0)=="ConvTranspose2d" or getattr(module, 'org_module_type', 0)=="Linear" or isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
                    Cout, Cin, h, w = module.weight.shape
                    print(f"Module index: {i}   \t  Module being pruned: {name} \t  SHAPE: {Cout}, {Cin}, {h}, {w} \t Pruning Ratio: {prune_ratio_per_module[i]}")
                    prune_module(model, name, cfg.prune_type, prune_ratio_per_module[i], cfg.repeat, self.pruneloader, self.device, prune_channels=cfg.prune_channels)
                    i += 1
        
        results = evaluate_model(model, dataloader) 
        print('Results after pruning:', results)
        if self.is_main:
            print(f'Pruning complete. Finetuning model {cfg.model}-quality_{cfg.model_quality}-type_{cfg.prune_type}-ratio_{cfg.prune_ratio}-NAS')
    
    def quantize_model_(self):
        cfg = self.cfg
        
        model = self.model 
        model.eval()
        model.set_quant_state(False, False)
        results = self.evaluate(-1, -1)                                 # Here
        print('Results before quantization:', results)
        # Quantization
        start_time = time.time()
        model.set_calibrate(True, True)
        with torch.no_grad():
            for i, img, in enumerate(tqdm(self.quantloader, desc="Collecting callibration images")):
                img = img.to(self.device)
                if i == 100:
                    break
                model(img)
        model(img)
        model.set_calibrate(False, False)
        model.set_quant_state(True, False)
        results = self.evaluate(-1, -1)                                 # Here
        print('Results after weight quantization without optimization:', results)
        for name, param in model.named_parameters():
            if "quantizer" in name:
                param.requires_grad = True
        end_time = time.time()
        print(f"Total time to quantize model: {int((end_time-start_time)//3600)} hour(s) {round((end_time-start_time)/60 - float((end_time-start_time)//3600)*60)} minute(s)")  

    def set_wandb_(self):
        cfg = self.cfg

        # check if there is a previous run to resume
        wbid_path = self._log_dir / 'wandb_id.txt'
        if os.path.exists(wbid_path):
            run_ids = open(wbid_path, mode='r').read().strip().split('\n')
            rid = run_ids[-1]
        else:
            rid = None
        # initialize wandb
        import wandb
        # run_name = f'{self._log_dir.stem} {cfg.model_args}'
        run_name = self.run_name
        wbrun = wandb.init(project=cfg.wbproject, group=cfg.wbgroup, name=run_name,
                           config=cfg, dir='runs/', id=rid, resume='allow',
                           save_code=True, mode=cfg.wbmode)
        cfg = wbrun.config
        cfg.wandb_id = wbrun.id
        with open(wbid_path, mode='a') as f:
            print(wbrun.id, file=f)

        self.wbrun = wbrun
        self.cfg = cfg
    
    def seed_all(self, seed=1029):
        random.seed(seed)
        os.environ['PYTHONHASHSEED'] = str(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
        torch.backends.cudnn.benchmark = False  # may slow
        torch.backends.cudnn.deterministic = True

    def main(self):
        # config
        self.seed_all()
        self.cfg = get_config()
        cfg = self.cfg
        # core
        self.set_device_()
        self.set_dataset_()
        self.set_model_()
        model = self.model
        self.set_optimizer_()
        # logging
        self.set_logging_dir_()
        if cfg.prune_model:
            if cfg.prune_NAS:
                self.prune_NAS_()
            else:
                self.prune_model_()
            if cfg.prune_quantize: 
                self.quantize_model_()
                model.set_quant_state(False, False)
                self.set_optimizer_()  
        if self.is_main:
            self.set_wandb_()
            # self.stats_table = SimpleTable(['Epoch', 'GPU_mem', 'lr'])
            self.stats_table = SimpleTable(['Epoch', 'GPU_mem', 'lr'])
            
        print(f'Number of trainable parameters: {len([p for n, p in model.named_parameters() if p.requires_grad])}')

        # ======================== start training ========================
        for epoch in range(self._start_epoch, cfg.epochs):
            if epoch == cfg.prune_quant_switch:
                print(f"Quantizing pruned model finetuned for {epoch} epochs")
                if cfg.dynamic_aquant:
                    model.set_quant_state(True, False)
                else:
                    model.set_quant_state(True, True)
                
                MODELS_TO_PRUNE = ["Conv2d", "ConvTranspose2d", "Linear"]
                for name, module in model.named_modules():
                    if isinstance(module, QuantModule):
                        if module.org_module_type in MODELS_TO_PRUNE:
                            sparsity = 1.0 - float(sum(torch.count_nonzero(param) for _, param in module.named_parameters())) / sum(param.numel() for _, param in module.named_parameters())
                            print(f"Layer: {name}       Sparsity: {sparsity}")
                
                for name, module in model.named_modules():
                    if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                        print(name, module.org_module_type)
                        weight = module.weight_quantizer(module.weight, module.lr)
                        if module.org_module_type == "ConvTranspose2d":
                            num_weight = torch.unique(weight[:,0,:,:]).reshape(-1).shape
                            num_weight_before = torch.unique(module.weight[:,0,:,:]).reshape(-1).shape
                        else:
                            num_weight = torch.unique(weight[0]).reshape(-1).shape
                            num_weight_before = torch.unique(module.weight[0]).reshape(-1).shape
                        print(f'Module: {name},     type: {module.org_module_type},   bit-type: {module.weight_quantizer.bit_type.name},   unique weight elements before: {num_weight_before}, unique weight elements after: {num_weight}')
                
            time.sleep(0.1)

            if self.distributed:
                self.trainloader.sampler.set_epoch(epoch)
            if hasattr(timm.utils.unwrap_model(self.model), 'set_epoch'):                   # can't find this set_epoch method
                timm.utils.unwrap_model(self.model).set_epoch(epoch, cfg.epochs, verbose=self.is_main)

            pbar = enumerate(self.trainloader)
            if self.is_main:
                if ((epoch != self._start_epoch) or cfg.eval_first) and (epoch % cfg.eval_per == 0):
                    self.evaluate(epoch, niter=epoch*len(self.trainloader))

                self.init_logging_()
                pbar = tqdm(pbar, total=len(self.trainloader))

            if cfg.lr_sched == 'cosine' or cfg.lr_sched == 'plateau':
                self.adjust_lr_(epoch)                                                              # compute_loss(x, x_hat, y_likelihoods, bpp_lmb)
                for name, module in model.named_modules():
                    if isinstance(module, QuantModule):
                        module.lr = self.optimizer.param_groups[0]['lr']
            # timm.utils.unwrap_model(model).train()
            model.train()
            for bi, imgs in pbar:
                niter = epoch * len(self.trainloader) + bi
                imgs = imgs.to(device=self.device)
                nB, _, imH, imW = imgs.shape
                # forward
                with amp.autocast(enabled=cfg.amp):
                    out_dict = model(imgs)
                    rec_imgs = out_dict["x_hat"]
                    bpp_lmb = out_dict["lmb"]
                    stats = compute_loss(imgs, rec_imgs, out_dict["likelihoods"], bpp_lmb)
                    loss = stats["loss"]
                self.scaler.scale(loss).backward()
                if cfg.prune_quantize:    
                    torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
                self.scaler.step(self.optimizer)
                self.scaler.update()
                self.optimizer.zero_grad()

                if self.is_main:
                    self.logging(pbar, epoch, bi, niter, imgs, stats)
            if self.is_main:
                pbar.close()

        if self.is_main:
            if cfg.prune_model:
                prune_remove(model, self.last_module_name, self.first_module_name)
                print("Sparsity in entire model: {:.3f}%".format(100. - 100. * float(sum(torch.count_nonzero(p) for p in model.parameters()))/ float(sum(p.numel() for p in model.parameters()))))
                MODELS_TO_PRUNE = ["Conv2d", "ConvTranspose2d", "Linear"]
                for name, module in model.named_modules():
                    if isinstance(module, QuantModule):
                        if module.org_module_type in MODELS_TO_PRUNE:
                            sparsity = 1.0 - float(sum(torch.count_nonzero(param) for _, param in module.named_parameters())) / sum(param.numel() for _, param in module.named_parameters())
                            print(f"Layer: {name}       Sparsity: {sparsity}")
                
                for name, module in model.named_modules():
                    if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                        print(name, module.org_module_type)
                        weight = module.weight_quantizer(module.weight, module.lr)
                        if module.org_module_type == "ConvTranspose2d":
                            num_weight = torch.unique(weight[:,0,:,:]).reshape(-1).shape
                            num_weight_before = torch.unique(module.weight[:,0,:,:]).reshape(-1).shape
                        else:
                            num_weight = torch.unique(weight[0]).reshape(-1).shape
                            num_weight_before = torch.unique(module.weight[0]).reshape(-1).shape
                        print(f'Module: {name},     type: {module.org_module_type},   bit-type: {module.weight_quantizer.bit_type.name},   unique weight elements before: {num_weight_before}, unique weight elements after: {num_weight}')
            results = self.evaluate(epoch+1, niter)                                 # Here
            print('Training finished. results:', results)
        if self.distributed:
            torch.distributed.destroy_process_group()
        
    def adjust_lr_(self, epoch, loss=0):
        cfg = self.cfg

        if cfg.lr_sched == 'cosine':
            lrf = get_cosine_lrf(epoch, cfg.lr, cfg.epochs-1)
            qlrf = get_cosine_lrf(epoch, cfg.qlr, cfg.epochs-1)
            if cfg.optim_params == "qparams_netparams":
                self.optimizer.param_groups[0]['lr'] = cfg.lr * lrf
                self.optimizer.param_groups[1]['lr'] = cfg.qlr * qlrf
            else:
                self.optimizer.param_groups[0]['lr'] = cfg.lr * lrf
        elif cfg.lr_sched == 'plateau':
            self.scheduler.step(loss)

    def init_logging_(self):
        # initialize stats table and progress bar
        for k in self.stats_table.keys():
            self.stats_table[k] = 0.0
        self._pbar_header = self.stats_table.get_header()
        print('\n', self._pbar_header)
        time.sleep(0.1)

    @torch.no_grad()
    def logging(self, pbar, epoch, bi, niter, imgs, stats):
        cfg = self.cfg

        self.stats_table['Epoch'] = f'{epoch}/{cfg.epochs-1}'

        mem = torch.cuda.max_memory_allocated(self.device) / 1e9
        torch.cuda.reset_peak_memory_stats()
        self.stats_table['GPU_mem'] = f'{mem:.3g}G'

        cur_lr = self.optimizer.param_groups[0]['lr']
        self.stats_table['lr'] = cur_lr

        keys_to_log = []
        for k, v in stats.items():
            if isinstance(v, torch.Tensor) and v.numel() == 1:
                v = v.detach().cpu().item()
            assert isinstance(v, (float, int))
            prev = self.stats_table.get(k, 0.0)
            self.stats_table[k] = (prev * bi + v) / (bi + 1)
            keys_to_log.append(k)
        pbar_header, pbar_body = self.stats_table.update()
        if pbar_header != self._pbar_header:
            print(pbar_header, f"GPU: {self.local_rank}")
            self._pbar_header = pbar_header
        pbar.set_description(pbar_body)

        # # Weights & Biases logging
        if niter % 100 == 0:
            _num = min(16, imgs.shape[0])
            _log_dic = {
                'general/lr': cur_lr
            }
            _log_dic.update(
                {'train/'+k: self.stats_table[k] for k in keys_to_log}
            )
            self.wbrun.log(_log_dic, step=niter)

    def evaluate(self, epoch, niter):
        cfg = self.cfg
        assert self.is_main
        # Evaluation
        _log_dic = {'general/epoch': epoch}
        _eval_model = timm.utils.unwrap_model(self.model).eval()
        results = evaluate_model(_eval_model, testloader=self.valloader)
        _log_dic.update({'metric/plain_val_'+k: v for k,v in results.items()})
        # save last checkpoint
        checkpoint = {
            'model'     : _eval_model.state_dict(),
            'optimizer' : self.optimizer.state_dict(),
            'scaler'    : self.scaler.state_dict(),
            'epoch'     : epoch,
            'results'   : results,
        }
        if cfg.lr_sched == 'plateau':
            checkpoint["lr_scheduler"] = self.scheduler.state_dict()
        torch.save(checkpoint, self._log_dir / 'last.pt')

        # wandb log
        if epoch != -1:
            self.wbrun.log(_log_dic, step=niter)
        # Log evaluation results to file
        msg = self.stats_table.get_body() + '||' + '%10.4g' % results['loss']
        with open(self._log_dir / 'results.txt', 'a') as f:
            f.write(msg + '\n')

        return results


def main():
    TrainWrapper().main()

if __name__ == '__main__':
    main()

