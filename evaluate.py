import os
import json
import pickle
from tqdm import tqdm
from pathlib import Path
from collections import defaultdict, OrderedDict
import tempfile
import argparse
import time
import math
import numpy as np
import torch
from torch.utils.data import DataLoader
import torch.nn.functional as tnf
import torchvision as tv
from torch.hub import load_state_dict_from_url

import timm
import timm.utils
from pytorch_msssim import ms_ssim

from models.vae import FactorizedPrior, ScaleHyperprior, Cheng2020Anchor, model_urls, cfgs
from models.registry import get_model
from models.quantization import BIT_TYPE_DICT
from utils import MyCIFAR100Dataset, ImageDataset, get_valloader, get_trainloader, pad, crop, compute_padding, get_model_size
from compressai.zoo.pretrained import load_pretrained
from models.quantization.quant_layer import QuantModule
from models.quantization.quant_model import QuantModel


def compute_psnr(a, b):
    mse = tnf.mse_loss(a, b)
    return -10 * math.log10(mse)

def compute_msssim(a, b):
    return ms_ssim(a, b, data_range=1., win_size=3,  win_sigma=1).item()

def get_object_size(obj, unit='bits'):
    assert unit == 'bits'
    pickle.dump(obj, open("my_file", "wb"))
    num_bits = os.path.getsize("my_file") * 8
    
    return num_bits


def evaluate_model(model, model_name, args, CIFAR=False, KODAK=False, CLIC=False, TECNICK=False):
    if KODAK or CLIC or TECNICK:
        test_transform = tv.transforms.Compose([tv.transforms.ToTensor()])
    else:
        test_transform = tv.transforms.Compose([
            # tv.transforms.Resize(args.img_size),
            tv.transforms.RandomResizedCrop(args.img_size),
            # tv.transforms.RandomCrop(args.img_size, pad_if_needed=True, padding_mode='reflect'),
            tv.transforms.ToTensor(),
        ])
    if CIFAR:
        testset = MyCIFAR100Dataset(args.data_root, train=False, transform=test_transform, download=True)
    elif KODAK:
        testset = ImageDataset(os.path.join(args.data_root, 'kodak'), transform=test_transform) 
    elif CLIC:
        testset = ImageDataset(os.path.join(args.data_root, 'clic41'), transform=test_transform) 
    elif TECNICK:
        testset = ImageDataset(os.path.join(args.data_root, 'tecnick'), transform=test_transform) 
    else:
        # testset = Vimeo90kDataset(args.data_root, transform=test_transform, split="valid", tuplet=7)
        testset = ImageDataset(os.path.join(args.data_root, 'coco/images/val2017'), transform=test_transform)  
        
    testloader = DataLoader(
        testset, batch_size=args.batch_size, shuffle=False, num_workers=args.workers,
        pin_memory=True, drop_last=True
    )

    device = next(model.parameters()).device

    pbar = tqdm(testloader)
    stats_accumulate = defaultdict(float)
    for im in pbar:
        im = im.to(device=device)
        nB, imC, imH, imW = im.shape
                
        if model_name == "cheng_anchor" and not (CLIC or TECNICK):
            p = 256  # maximum 6 strides of 2, and window size 4 for the smallest latent fmap: 4*2^6=256
            im = pad(im, p)
        
        if CLIC or TECNICK:
            padx, unpad = compute_padding(imH, imW, min_div=2**6)
            im_padded = tnf.pad(im, padx, mode="constant", value=0)  
        # sender side: compress and save
        
        if args.real_bpp:
            if args.quant_model or args.prune_quantize:
                if CLIC or TECNICK:
                    compressed_obj = model.model.compress(im_padded)
                else:
                    compressed_obj = model.model.compress(im)   
            else:
                if CLIC or TECNICK:
                    compressed_obj = model.compress(im_padded)
                else:
                    compressed_obj = model.compress(im) 
            # sender side: compute bpp
            num_bits = get_object_size((compressed_obj["strings"][0], compressed_obj["strings"][1], compressed_obj["shape"]))
            bpp = num_bits / float(imH * imW)
            # receiver side: class prediction
            if args.quant_model or args.prune_quantize:
                rec_im = model.model.decompress(compressed_obj)["x_hat"]
            else:
                rec_im = model.decompress(compressed_obj)["x_hat"]
        else:
            if CLIC or TECNICK:
                out = model(im_padded)
            else:
                out = model(im)
            rec_im = out["x_hat"]
            bpp = sum(
                    (-1.0 * torch.log2(likelihood).sum(dim=(1, 2, 3)) / float(imH * imW))
                    for likelihood in out['likelihoods'].values()).item()
        
        if CLIC or TECNICK:
            rec_im = tnf.pad(rec_im, unpad)
        
        if model_name == "cheng_anchor" and not (CLIC or TECNICK):
            rec_im = crop(rec_im, (imH, imW))
            rec_im.clamp_(0, 1)

        stats_accumulate['count'] += float(nB)
        stats = {
            'PSNR': compute_psnr(im, rec_im),
            'MS-SSIM': compute_msssim(im, rec_im),
            'bpp': bpp
        }
        for k, v in stats.items():
            stats_accumulate[k] += float(v)

        # logging
        _cnt = stats_accumulate['count']
        msg = ', '.join([f'{k}={v/_cnt:.4g}' for k,v in stats_accumulate.items() if (k != 'count')])
        pbar.set_description(msg)
    pbar.close()

    # compute total statistics and return
    total_count = stats_accumulate.pop('count')
    results = {k: v/total_count for k,v in stats_accumulate.items()}
    return results


def evaluate_all_bit_rate(model_name, args, CIFAR=False, KODAK=False, CLIC=False, TECNICK=False):
    # device = torch.device('cuda', 0)
    device = torch.device('cpu')
    # if not args.quant_model:
    #     checkpt_name = model_name
    # else:
    checkpt_name = args.checkpoint
    # checkpoint_root = Path(f'checkpoints/{model_name}')
    checkpoint_root = Path(f'checkpoints/{checkpt_name}')
    os.makedirs('results', exist_ok=True)
    os.makedirs('results_kodak', exist_ok=True)
    os.makedirs('results_kodak_ICIP', exist_ok=True)
    os.makedirs('results_clic', exist_ok=True)
    os.makedirs('results_tecnick', exist_ok=True)
    if KODAK:
        if args.real_bpp:
            save_json_path = Path(f'results_kodak_ICIP/{checkpt_name}_kodak.json')
        else:
            if args.variable_bit_aquant:
                save_json_path = Path(f'results_kodak/{checkpt_name}_variable_bit_aquant_entropy_bpp_kodak.json')
            else:
                save_json_path = Path(f'results_kodak/{checkpt_name}_entropy_bpp_kodak_TRIAL.json')
    elif CLIC:
        if args.real_bpp:
            save_json_path = Path(f'results_clic/{checkpt_name}_clic.json')
        else:
            if args.variable_bit_aquant:
                save_json_path = Path(f'results_clic/{checkpt_name}_variable_bit_aquant_entropy_bpp_clic.json')
            else:
                save_json_path = Path(f'results_clic/{checkpt_name}_entropy_bpp_clic.json')
    elif TECNICK:
        if args.real_bpp:
            save_json_path = Path(f'results_tecnick/{checkpt_name}_tecnick.json')
        else:
            if args.variable_bit_aquant:
                save_json_path = Path(f'results_tecnick/{checkpt_name}_variable_bit_aquant_entropy_bpp_tecnick.json')
            else:
                save_json_path = Path(f'results_tecnick/{checkpt_name}_entropy_bpp_tecnick.json')
    else:
        save_json_path = Path(f'results/{checkpt_name}.json')
    # save_json_path = Path(f'results_kodak/{model_name}.json')
    if save_json_path.is_file():
        print(f'==== Warning: {save_json_path} already exists. Will overwrite it! ====')
    else:
        print(f'Will save results to {save_json_path} ...')

    if args.quant_model or model_name=="mean_scale_hyperprior" or model_name=="nic":
        checkpoint_paths = list(checkpoint_root.rglob('*.pt'))
        checkpoint_paths.sort()
        print(f'Find {len(checkpoint_paths)} checkpoints in {checkpoint_root}. Evaluating them ...')

    results_of_all_models = defaultdict(list)
    
    model_size_path = f'model_sizes/{checkpt_name}.json'
    FP_model_sizes = 0
    FPQ_model_sizes = 0
    MPQ_model_sizes = 0
    if args.prune_model:
        FP_pruned_model_sizes = 0
        FPQ_pruned_model_sizes = 0
        MPQ_pruned_model_sizes = 0
    
    for path_idx, (hyper_params, url) in enumerate(model_urls[model_name]["mse"].items()):
        print(path_idx)
        if KODAK or CLIC or TECNICK:
            args.batch_size = 1
        
        SCALE_METHOD = "minmax"             
        SIGNED = BIT_TYPE_DICT['int8']
        UNSIGNED = BIT_TYPE_DICT['uint8']
        DYNAMIC = False
        if args.dynamic_aquant:
            DYNAMIC = True
        SCALE_WEIGHTS = False
        if args.scaled_weights:
            SCALE_WEIGHTS = True
        wq_params = {'scale_method': SCALE_METHOD, 'bit_type': UNSIGNED, 'calibration_mode': "channel_wise", 'quantizer_type': "scaled-lsq", 'scale_weights': SCALE_WEIGHTS, 'use_qe_loss': False}
        aq_params = {'scale_method': SCALE_METHOD, 'bit_type': BIT_TYPE_DICT[args.aquant_bits], 'calibration_mode': args.aquant_calibration_mode, 'quantizer_type': "scaled-lsq", 'dynamic': DYNAMIC}
        
        quality, (N, M), bpp_lmb = hyper_params  
        if args.quant_model:
            model = get_model(model_name+"_quantized")(N, M, bpp_lmb, wq_params, aq_params, quality, pretrained=True)
        else:
            # model = get_model(model_name+"_quantized")(N, M, bpp_lmb, wq_params, aq_params, quality, pretrained=True)
            model = get_model(model_name)(N, M, bpp_lmb)
            if args.prune_quantize:
                quant_model = QuantModel(model=model, weight_quant_params=wq_params, act_quant_params=aq_params)
                model = quant_model
        model = model.to(device=device)
        
        #         print(type(module))
        #         # print(type(module.weight))
        #         # print(module.weight.shape)
        #         C_out, C_in, k, _ = module.weight.shape
        #         print(C_out, C_in, k, k)
        # exit()
        
        # for name, module in model.named_modules():
        #     if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
        #         print(name, module.org_module_type)
        # exit()
                    
        if args.MPQ:
            with open(os.path.join(checkpoint_root, f'bit_types_{quality}.json'), 'r') as f:
                bit_type_names = json.load(f) 
            modules_to_quant = []
            module_names = []
            for name, module in model.named_modules():
                if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                    modules_to_quant.append(module)
                    module_names.append(name)
            last_module_name = module_names[-1]
            latent_module_name = module_names[3]
            assert len(modules_to_quant)==len(bit_type_names), '--Mismatch in the number of layers to be quantized'
            for i, (module, bit_type_str) in enumerate(zip(modules_to_quant, bit_type_names.values())): 
                if args.variable_bit_aquant:
                    if BIT_TYPE_DICT[bit_type_str].bits < 0:
                        act_bit = BIT_TYPE_DICT['uint8']
                    else:
                        act_bit = BIT_TYPE_DICT[bit_type_str]
                    module.init_MPQ(BIT_TYPE_DICT[bit_type_str], act_bit_type=act_bit)
                else:
                    # module.init_MPQ(BIT_TYPE_DICT[bit_type_str])
                    if module_names[i] == last_module_name:
                        module.init_MPQ(BIT_TYPE_DICT[bit_type_str], act_bit_type=BIT_TYPE_DICT['uint8'])
                    else:
                        module.init_MPQ(BIT_TYPE_DICT[bit_type_str])
            
        if args.quant_model or args.prune_quantize:
            DATA_DIR = Path('/home/jackfruit/a/hossai34/datasets/coco/images') 
            train_split = 'train2017'
            train_dir = os.path.join(DATA_DIR, train_split)
            trainloader = get_trainloader(root_dir=train_dir, 
                                            img_size=args.img_size, 
                                            batch_size=args.calib_batchsize, 
                                            workers=args.workers, 
                                            distributed=False, 
                                            CIFAR=False)
            model.set_quant_state(False, False)
            model.set_calibrate(True, True)
            with torch.no_grad():
                for i, img, in enumerate(tqdm(trainloader, desc="Collecting callibration images")):
                    img = img.to(device)
                    if i == 10:
                        break
                    output = model(img)
            output = model(img)
            model.set_calibrate(False, False)
            model.set_quant_state(True, True)
            # model.set_quant_state(False, False)
        
        # FP_model_sizes += get_model_size(model, model_type="full_precision", bit_per_module=None)  
        # print(f'Full precision floating point model size: {get_model_size(model, model_type="full_precision", bit_per_module=None)} MB')     
        # if args.prune_model:
        #     FP_pruned_model_sizes += get_model_size(model, model_type="full_precision", bit_per_module=None, pruned_model=True)
        #     print(f'Full precision pruned floating point model size: {get_model_size(model, model_type="full_precision", bit_per_module=None, pruned_model=True)} MB')
        # if args.quant_model:
        #     FPQ_model_sizes += get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)
        #     print(f'Fixed precision quantized model size: {get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)} MB')
        #     if args.prune_model:
        #         FPQ_pruned_model_sizes += get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8, pruned_model=True)
        #         print(f'Fixed precision quantized + pruned model size: {get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8, pruned_model=True)} MB')
        #     if args.MPQ:
        #         MPQ_model_sizes += get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()))
        #         print(f'Mixed precision quantized model size: {get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()))} MB')
        #         if args.prune_model:
        #             MPQ_pruned_model_sizes += get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()), pruned_model=True)
        #             print(f'Mixed precision quantized + pruned model size: {get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()), pruned_model=True)} MB')
        
        # for name, module in model.named_modules():
        #     print(name, type(module))
        # for name, param in model.model.named_parameters():
        #     print(name)
        # count = 0
        # for name, param in model.model.named_parameters():
        #     if 'entropy_bottleneck' in name or 'quantizer' in name:
        #         count += 1
        #     else:
        #         count += 1
        #     # # elif 'quantizer' in name:
        #     # #     count
        #     # else:
        # print(count)
        # count = 0
        # for name, param in model.model.named_parameters():
        #     if 'entropy_bottleneck' in name:
        #         count += 1
        #     elif 'quantizer' in name:
        #         count += 1
        # print(count)
        # print(sum(1 for name, param in model.model.named_parameters()))
        # # print(sum(1 for name, param in model.model.named_parameters() if 'weight' in name))
        # # print(sum(1 for name, param in model.model.named_parameters() if 'bias' in name))
        # # print(sum(1 for name, param in model.model.named_parameters() if ('weight' in name or 'bias' in name)))
        # # print(sum(1 for name, param in model.model.named_parameters() if not ('weight' in name or 'bias' in name)))
        # for name, param in model.named_parameters():
        #     print(name)
            
        # exit()        
        # param_list1 = []
        # for name, param in model.named_parameters():
        #     if name == "model.g_a.0.weight": # or name == "model.g_a.0.weight": # or name == "model.h_a.4.act_quantizer.scale":
        #         print(name)
        #         print(param.squeeze().shape)
        #         print(param.reshape(-1,1).squeeze()[:20])
        #         # print(param)
        #         # print(torch.unique(param))
        #         # param_list1.append(torch.unique(param).squeeze())
        #         param_list1.append(param.reshape(-1,1).squeeze()[:20].clone())
        #         print("######################")
        
        # for name, param in model.named_parameters():
        #     # print(name)
        #     if "model.g_s.4.weight_scaler.alpha" in name:
        #         print(f"Alpha for {name}:   ")
        #         print(param.shape)
        #         param = param.data.clone().reshape(-1).squeeze()
        #         print(param)
        #         break
        
        if args.quant_model or model_name=="mean_scale_hyperprior" or args.prune_quantize:
            ckptpath = checkpoint_paths[path_idx]
            print(ckptpath)
            print(f"Quality: {quality}", ckptpath)
            checkpoint = torch.load(ckptpath)
            model.load_state_dict(checkpoint['model'], strict=True)
            if args.quant_model or args.prune_quantize:
                model.set_quant_state(True, True)
        else:
            state_dict = load_state_dict_from_url(url, progress=True)
            state_dict = load_pretrained(state_dict)
            model.load_state_dict(state_dict, strict=True)
        
        # print(f"Sparsity: {100. - 100. * float(sum(torch.count_nonzero(p) for p in model.parameters()))/ float(sum(p.numel() for p in model.parameters()))}")
        # for name, module in model.named_modules():
        #     if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
        #         sparsity = 1.0 - float(sum(torch.count_nonzero(param) for _, param in module.named_parameters())) / sum(param.numel() for _, param in module.named_parameters())
        #         print(f"Layer: {name}       Sparsity: {sparsity}")
  
        
        
        print(f"Sparsity: {100. - 100. * float(sum(torch.count_nonzero(p) for p in model.parameters()))/ float(sum(p.numel() for p in model.parameters()))}")
        FP_model_sizes += get_model_size(model, model_type="full_precision", bit_per_module=None)  
        print(f'Full precision floating point model size: {get_model_size(model, model_type="full_precision", bit_per_module=None)} MB')     
        if args.prune_model:
            FP_pruned_model_sizes += get_model_size(model, model_type="full_precision", bit_per_module=None, pruned_model=True)
            print(f'Full precision pruned floating point model size: {get_model_size(model, model_type="full_precision", bit_per_module=None, pruned_model=True)} MB')
        if args.quant_model or args.prune_quantize:
            FPQ_model_sizes += get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)
            print(f'Fixed precision quantized model size: {get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8)} MB')
            if args.prune_model:
                FPQ_pruned_model_sizes += get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8, pruned_model=True)
                print(f'Fixed precision quantized + pruned model size: {get_model_size(model, model_type="fixed_precision_quantization", bit_per_module=8, pruned_model=True)} MB')
            if args.MPQ:
                MPQ_model_sizes += get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()))
                print(f'Mixed precision quantized model size: {get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()))} MB')
                if args.prune_model:
                    MPQ_pruned_model_sizes += get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()), pruned_model=True)
                    print(f'Mixed precision quantized + pruned model size: {get_model_size(model, model_type="mixed_precision_quantization", bit_per_module=list(bit_type_names.values()), pruned_model=True)} MB')
        
        MODELS_TO_PRUNE = ["Conv2d", "ConvTranspose2d", "Linear"]
        for name, module in model.named_modules():
            if isinstance(module, QuantModule):
                if module.org_module_type in MODELS_TO_PRUNE:
                    sparsity = 1.0 - float(sum(torch.count_nonzero(param) for _, param in module.named_parameters())) / sum(param.numel() for _, param in module.named_parameters())
                    print(f"Layer: {name}       Sparsity: {sparsity}")
        
        for name, module in model.named_modules():
            if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                print(name, module.org_module_type)
                weight = module.weight_quantizer(module.weight, module.lr)
                if module.org_module_type == "ConvTranspose2d":
                    num_weight = torch.unique(weight[:,0,:,:]).reshape(-1).shape
                    num_weight_before = torch.unique(module.weight[:,0,:,:]).reshape(-1).shape
                else:
                    num_weight = torch.unique(weight[0]).reshape(-1).shape
                    num_weight_before = torch.unique(module.weight[0]).reshape(-1).shape
                print(f'Module: {name},     type: {module.org_module_type},   bit-type: {module.weight_quantizer.bit_type.name},   unique weight elements before: {num_weight_before}, unique weight elements after: {num_weight}')
            
        # for name, param in model.named_parameters():
        #     if name == "model.g_a.0.weight": # or name == "model.g_a.0.weight": # or name == "model.h_a.4.act_quantizer.scale":
        
        # param_list2 = []
        # for name, param in model.named_parameters():
        #     if name == "model.g_a.0.weight": # or name == "model.g_a.0.weight": # or name == "model.h_a.4.act_quantizer.scale":
        #         print(name)
        #         print(param.squeeze().shape)
        #         print(param.reshape(-1,1).squeeze()[:20])
        #         # print(param)
        #         # print(torch.unique(param))
        #         # param_list2.append(torch.unique(param).squeeze())
        #         param_list2.append(param.reshape(-1,1).squeeze()[:20])
        #         print("######################")
        
        # for i in range(len(param_list1[0])):
        #     print(param_list1[0][i], param_list2[0][i])
        # print(param_list1[1][0])
        # print(param_list2[1][0])
            
        # for i in range(len(param_list1)):
        #     count = 0
        #     for j in range(len(param_list1[i])):
        #         print(param_list1[i][j])
        #         print(param_list2[i][j])
        #         if param_list1[i][j] != param_list2[i][j]:
        #             count += 1
        #     print(f"Mismatches for model: {count} out of {len(param_list1[i])} elements")
        
        # exit()
        # for name, param1 in model.named_parameters():
        #     # print(name)
        #     if "model.g_s.4.weight_scaler.alpha" in name:
        #         print(f"Alpha for {name}:   ")
        #         print(param1.shape)
        #         param1 = param1.data.clone().reshape(-1).squeeze()
        #         print(param1)
        #         break
        
        # print(param==param1)
        # print(param.shape)
        # print(param1.shape)
        # k = 0
        # for i in range(len(param)):
        #     if param[i]==param1[i]:
        #         k+=1
        # print(k)

        # model = model.to(device=device)
        model.eval()
        model.update()

        results = evaluate_model(model, model_name, args, CIFAR=CIFAR, KODAK=KODAK, CLIC=CLIC, TECNICK=TECNICK)
        print(results)
        results['quality-lmb'] = (quality, bpp_lmb)
        # results['checkpoint'] = str(ckptpath.relative_to(checkpoint_root))
        results
        for k,v in results.items():
            results_of_all_models[k].append(v)

        with open(save_json_path, 'w') as f:
            json.dump(results_of_all_models, fp=f, indent=4)
    
    # if args.quant_model:
    model_sizes = {}
    model_sizes['FP'] = FP_model_sizes / len(checkpoint_paths)
    if args.quant_model or args.prune_quantize:
        model_sizes['FPQ'] = FPQ_model_sizes / len(checkpoint_paths)
        if args.MPQ:
            model_sizes['MPQ'] = MPQ_model_sizes / len(checkpoint_paths)
    if args.prune_model:
        model_sizes['FP_pruned'] = FP_pruned_model_sizes / len(checkpoint_paths)
        if args.quant_model or args.prune_quantize:
            model_sizes['FPQ_pruned'] = FPQ_pruned_model_sizes / len(checkpoint_paths)
            if args.MPQ:
                model_sizes['MPQ_pruned'] = MPQ_pruned_model_sizes / len(checkpoint_paths)
        
    with open(model_size_path, 'w') as f:
        json.dump(model_sizes, fp=f, indent=4)
    


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--models', type=str, nargs='+',
        default=['scale_hyperprior'])                                                                     # 'ours_n0', 'ours_n4', 'ours_n8'
    parser.add_argument('-c', '--checkpoint', type=str, default='scale_hyperprior_8_uncompressed')
    parser.add_argument('-d', '--data_root',  type=str, default='/home/jackfruit/a/hossai34/datasets')
    parser.add_argument('-i', '--img_size', type=int, default=256)
    parser.add_argument('-b', '--batch_size', type=int, default=1)
    parser.add_argument('-w', '--workers',    type=int, default=1)
    parser.add_argument('--cifar', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--kodak', action=argparse.BooleanOptionalAction, default=False)    
    parser.add_argument('--clic', action=argparse.BooleanOptionalAction, default=False)                                
    parser.add_argument('--tecnick', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--quant_model', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--prune_model', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--prune_quantize', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--prune_NAS', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--dynamic_aquant', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--variable_bit_aquant', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--MPQ', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--aquant_calibration_mode', type=str, default="channel_wise", choices=['layer_wise', 'channel_wise'])
    parser.add_argument('--aquant_bits', type=str, default="uint8")
    parser.add_argument('--scaled_weights', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--real_bpp', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--calib-batchsize', default=1, type=int, help='batchsize of calibration set')
    parser.add_argument('--calib-iter', default=50, type=int)
    args = parser.parse_args()

    # torch.set_num_threads(1)
    for model_name in args.models:
        evaluate_all_bit_rate(model_name, args, CIFAR=args.cifar, KODAK=args.kodak, CLIC=args.clic, TECNICK=args.tecnick)
        print()


if __name__ == '__main__':
    main()
