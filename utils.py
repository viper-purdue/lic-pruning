from pathlib import Path
from tqdm import tqdm
from collections import defaultdict, OrderedDict
from PIL import Image
import os
import json
import time
import math
import argparse
import numpy as np
import torch
import torch.cuda.amp as amp
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.nn.functional as F

from torch.utils.data import Dataset, DataLoader
import torchvision as tv
import timm
import timm.utils

from compressai.datasets import Vimeo90kDataset
from models.quantization import BIT_TYPE_DICT
from models.quantization.quant_layer import QuantModule
from models.quantization.quant_model import QuantModel

class MyCIFAR100Dataset(tv.datasets.CIFAR100):

    def __getitem__(self, index: int):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        img, target = self.data[index], self.targets[index]

        # doing this so that it is consistent with all other datasets
        # to return a PIL Image
        img = Image.fromarray(img)

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img
        

class ImageDataset(Dataset):
    def __init__(self, root, transform):
        self.root = root # will be accessed by the training script
        self.transform = transform
        # scan and add images
        self.image_paths = sorted(Path(root).rglob('*.*'))
        assert len(self.image_paths) > 0, f'Found {len(self.image_paths)} images in {root}.'

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, index):
        impath = self.image_paths[index]
        img = Image.open(impath).convert('RGB') 
        im = self.transform(img)
        return im


def get_trainloader(root_dir, img_size: int, batch_size: int, workers: int, distributed=False):
    """ get training data loader

    Args:
        root_dir ([type]): [description]
        aug (str): transform congif string
        img_size (int): input image size
        input_norm ([type]): input normalization type
        batch_size (int): [description]
        workers (int): [description]
    """
    # IMAGENET_MEAN = (0.485, 0.456, 0.406)
    # IMAGENET_STD  = (0.229, 0.224, 0.225)
    transform = [
        # tv.transforms.RandomResizedCrop(img_size),
        tv.transforms.RandomCrop(img_size, pad_if_needed=True, padding_mode='reflect'),
        tv.transforms.RandomHorizontalFlip(p=0.5),
        tv.transforms.ToTensor(),
        # tv.transforms.Normalize(mean=IMAGENET_MEAN, std=IMAGENET_STD)
    ]
    transform = tv.transforms.Compose(transform)
    trainset = ImageDataset(root_dir, transform=transform)
    sampler = torch.utils.data.distributed.DistributedSampler(trainset) if distributed else None
    trainloader = torch.utils.data.DataLoader(
        trainset, batch_size=batch_size, shuffle=(sampler is None), num_workers=workers,
        pin_memory=False, drop_last=False, sampler=sampler
    )
    return trainloader


def get_valloader(root_dir, img_size=256, crop_ratio=0.875, batch_size=1, workers=0, KODAK=False):

    if KODAK:
        transform = [tv.transforms.ToTensor()]
    else:
        transform = [
            # tv.transforms.RandomResizedCrop(img_size),
            tv.transforms.CenterCrop(img_size),
            # tv.transforms.RandomHorizontalFlip(p=0.5),
            tv.transforms.ToTensor(),
            # tv.transforms.Normalize(mean=IMAGENET_MEAN, std=IMAGENET_STD)
        ]
    transform = tv.transforms.Compose(transform)

    
    dataset = ImageDataset(root_dir, transform=transform)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=False, num_workers=workers,
        pin_memory=True, drop_last=False
    )
    return dataloader


def increment_dir(dir_root='runs/', name='exp'):
    """ Increament directory name. E.g., exp_1, exp_2, exp_3, ...

    Args:
        dir_root (str, optional): root directory. Defaults to 'runs/'.
        name (str, optional): dir prefix. Defaults to 'exp'.
    """
    assert isinstance(dir_root, (str, Path))
    dir_root = Path(dir_root)
    n = 0
    while (dir_root / f'{name}_{n}').is_dir():
        n += 1
    name = f'{name}_{n}'
    return name


class SimpleTable(OrderedDict):
    def __init__(self, init_keys=[]):
        super().__init__()
        # initialization: assign None to initial keys
        for key in init_keys:
            if not isinstance(key, str):
                print(f'Progress bar logger key: {key} is not a string')
            self[key] = None
        self._str_lengths = {k: 8 for k,v in self.items()}

    def _update_length(self, key, length):
        old = self._str_lengths.get(key, 0)
        if length <= old:
            return old
        else:
            self._str_lengths[key] = length
            return length

    def update(self, border=False):
        """ Update the string lengths, and return header and body

        Returns:
            str: table header
            str: table body
        """
        header = []
        body = []
        for k,v in self.items():
            # convert any object to string
            key = self.obj_to_str(k)
            val = self.obj_to_str(v)
            # get str length
            str_len = max(len(key), len(val)) + 2
            str_len = self._update_length(k, str_len)
            # make header and body string
            keystr = f'{key:^{str_len}}|'
            valstr = f'{val:^{str_len}}|'
            header.append(keystr)
            body.append(valstr)
        header = ''.join(header)
        if border:
            header = print(header)
        body = ''.join(body)
        return header, body

    def get_header(self, border=False):
        header = []
        body = []
        for k in self.keys():
            key = self.obj_to_str(k)
            str_len = self._str_lengths[k]
            keystr = f'{key:^{str_len}}|'
            header.append(keystr)
        header = ''.join(header)
        if border:
            header = print(header)
        return header

    def get_body(self):
        body = []
        for k,v in self.items():
            val = self.obj_to_str(v)
            str_len = self._str_lengths[k]
            valstr = f'{val:^{str_len}}|'
            body.append(valstr)
        body = ''.join(body)
        return body

    @staticmethod
    def obj_to_str(obj, digits=4):
        if isinstance(obj, str):
            return obj
        elif isinstance(obj, float) or hasattr(obj, 'float'):
            obj = float(obj)
            return f'{obj:.{digits}g}'
        elif isinstance(obj, list):
            strings = [SimpleTable.obj_to_str(item, 3) for item in obj]
            return '[' + ', '.join(strings) + ']'
        elif isinstance(obj, tuple):
            strings = [SimpleTable.obj_to_str(item, 3) for item in obj]
            return '(' + ', '.join(strings) + ')'
        else:
            return str(obj)


def get_cosine_lrf(n, lrf_min, T):
    """ Cosine learning rate factor

    Args:
        n (int): current epoch. 0, 1, 2, ..., T
        lrf_min (float): final (should also be minimum) learning rate factor
        T (int): total number of epochs
    """
    assert 0 <= n <= T, f'n={n}, T={T}'
    lrf = lrf_min + 0.5 * (1 - lrf_min) * (1 + math.cos(n * math.pi / T))
    return lrf


def bd_rate(r1, psnr1, r2, psnr2):
    """ Compute average bit rate saving of RD-2 over RD-1.

    Equivalent to the implementations in:
    https://github.com/Anserw/Bjontegaard_metric/blob/master/bjontegaard_metric.py
    https://github.com/google/compare-codecs/blob/master/lib/visual_metrics.py

    args:
        r1    (list, np.ndarray): baseline rate
        psnr1 (list, np.ndarray): baseline psnr
        r2    (list, np.ndarray): rate 2
        psnr2 (list, np.ndarray): psnr 2
    """
    lr1 = np.log(r1)
    lr2 = np.log(r2)

    # fit each curve by a polynomial
    degree = 2
    p1 = np.polyfit(psnr1, lr1, deg=degree)
    p2 = np.polyfit(psnr2, lr2, deg=degree)
    # compute integral of the polynomial
    p_int1 = np.polyint(p1)
    p_int2 = np.polyint(p2)
    # area under the curve = integral(max) - integral(min)
    min_psnr = max(min(psnr1), min(psnr2))
    max_psnr = min(max(psnr1), max(psnr2))
    auc1 = np.polyval(p_int1, max_psnr) - np.polyval(p_int1, min_psnr)
    auc2 = np.polyval(p_int2, max_psnr) - np.polyval(p_int2, min_psnr)

    # find avgerage difference
    avg_exp_diff = (auc2 - auc1) / (max_psnr - min_psnr)
    avg_diff = (np.exp(avg_exp_diff) - 1) * 100

    if False: # debug
        import matplotlib.pyplot as plt
        plt.figure(figsize=(8,6))
        l1 = plt.plot(psnr1, lr1, label='PSNR-logBPP 1',
                      marker='.', markersize=12, linestyle='None')
        l2 = plt.plot(psnr2, lr2, label='PSNR-logBPP 2',
                      marker='.', markersize=12, linestyle='None')
        x = np.linspace(min_psnr, max_psnr, num=100)
        # x = np.linspace(min_psnr-10, max_psnr+10, num=100)
        plt.plot(x, np.polyval(p1, x), label='polyfit 1',
                 linestyle='-', color=l1[0].get_color())
        plt.savefig("images/Exp10.jpg")
        plt.plot(x, np.polyval(p2, x), label='polyfit 2',
                 linestyle='-', color=l2[0].get_color())
        plt.savefig("images/Ex11.jpg")
        plt.legend(loc='lower right')
        plt.xlim(np.concatenate([psnr1,psnr2]).min()-1, np.concatenate([psnr1,psnr2]).max()+1)
        plt.ylim(np.concatenate([lr1, lr2]).min()-0.1, np.concatenate([lr1, lr2]).max()+0.1)
        # plt.ylim(np.concatenate(lr1, lr2).min(), max(np.concatenate(lr1, lr2)))
        plt.show()
    return avg_diff


def pad(x, p=2 ** 6):
    h, w = x.size(2), x.size(3)
    H = (h + p - 1) // p * p
    W = (w + p - 1) // p * p
    padding_left = (W - w) // 2
    padding_right = W - w - padding_left
    padding_top = (H - h) // 2
    padding_bottom = H - h - padding_top
    return F.pad(
        x,
        (padding_left, padding_right, padding_top, padding_bottom),
        mode="constant",
        value=0,
    )


def crop(x, size):
    H, W = x.size(2), x.size(3)
    h, w = size
    padding_left = (W - w) // 2
    padding_right = W - w - padding_left
    padding_top = (H - h) // 2
    padding_bottom = H - h - padding_top
    return F.pad(
        x,
        (-padding_left, -padding_right, -padding_top, -padding_bottom),
        mode="constant",
        value=0,
    )


def compute_padding(in_h: int, in_w: int, *, out_h=None, out_w=None, min_div=1):
    """Returns tuples for padding and unpadding.

    Args:
        in_h: Input height.
        in_w: Input width.
        out_h: Output height.
        out_w: Output width.
        min_div: Length that output dimensions should be divisible by.
    """
    if out_h is None:
        out_h = (in_h + min_div - 1) // min_div * min_div
    if out_w is None:
        out_w = (in_w + min_div - 1) // min_div * min_div

    if out_h % min_div != 0 or out_w % min_div != 0:
        raise ValueError(
            f"Padded output height and width are not divisible by min_div={min_div}."
        )

    left = (out_w - in_w) // 2
    right = out_w - in_w - left
    top = (out_h - in_h) // 2
    bottom = out_h - in_h - top

    pad = (left, right, top, bottom)
    unpad = (-left, -right, -top, -bottom)

    return pad, unpad
    

def get_model_size(model, model_type="full_precision", bit_per_module=None, pruned_model=False):
    assert model_type in ["full_precision", "fixed_precision_quantization", "mixed_precision_quantization"], '--model type not supported'
   
    if model_type == "full_precision":
        if isinstance(model, QuantModel):
            if pruned_model:
                model_size = 32.0 * float(sum(torch.count_nonzero(param) for _, param in model.model.named_parameters()))
            else:
                model_size = 32.0 * sum(param.numel() for _, param in model.model.named_parameters())
        else:
            if pruned_model:
                model_size = 32.0 * float(sum(torch.count_nonzero(param) for _, param in model.named_parameters()))
            else:
                model_size = 32.0 * sum(param.numel() for _, param in model.named_parameters())
    elif model_type == "fixed_precision_quantization":
        assert isinstance(bit_per_module, int)
        model_size = 0
        for name, param in model.model.named_parameters():
            if 'entropy_bottleneck' in name or 'quantizer' in name:
                model_size += 32 * param.numel()
            else:
                if pruned_model:
                    model_size += bit_per_module * float(torch.count_nonzero(param))
                else:
                    model_size += bit_per_module * param.numel()
    elif model_type == "mixed_precision_quantization":
        assert isinstance(bit_per_module, list)
        model_size = 0
        for name, param in model.model.named_parameters():
            if 'entropy_bottleneck' in name or 'quantizer' in name:
                model_size += 32 * param.numel()
        modules_to_quant = []
        for name, module in model.named_modules():
            if isinstance(module, QuantModule) and module.org_module_type != "PixelShuffle":
                modules_to_quant.append(module)
        assert len(modules_to_quant)==len(bit_per_module), '--Mismatch in the number of layers to be quantized'
        for module, bit_type_str in zip(modules_to_quant, bit_per_module): 
            if pruned_model:
                model_size += BIT_TYPE_DICT[bit_type_str].bits * float(torch.count_nonzero(module.weight)) 
                model_size += BIT_TYPE_DICT[bit_type_str].bits * float(torch.count_nonzero(module.bias)) 
            else:
                model_size += BIT_TYPE_DICT[bit_type_str].bits * module.weight.numel() 
                model_size += BIT_TYPE_DICT[bit_type_str].bits * module.bias.numel() 
    
    return model_size * 10**-6 / 8.0 
                
                

