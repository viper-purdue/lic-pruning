import numpy as np
import torch.nn as nn
import torch
from torch.nn.utils import prune
from tqdm import tqdm
from channel_independence import CHIP, RANK
from models.quantization.quant_layer import QuantModule

MODELS_TO_PRUNE = ["Conv2d", "ConvTranspose2d", "Linear"]             # "MaskedConv2d"

def prune_model(model: nn.Module, prune_type: str, prune_ratio: int, repeat: int, trainloader: torch.utils.data.DataLoader, device: torch.device, prune_channels=False):
    first_module_name = None
    for name, module in model.named_modules():
        if isinstance(module, QuantModule):
            if module.org_module_type in MODELS_TO_PRUNE:
                if 'g_s' in name:
                    last_module_name = name
            if module.org_module_type == "Conv2d" and first_module_name == None:
                first_module_name = name
        elif isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
            if 'g_s' in name:
                last_module_name = name
            if isinstance(module, nn.Conv2d) and first_module_name == None:
                first_module_name = name
    print("Last module is:  ", last_module_name)
        # if isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
        #     if 'g_s' in name:
        #         last_module_name = name
        # if isinstance(module, nn.Conv2d) and first_module_name == None:
        #     first_module_name = name
    if prune_type == "CHIP" or prune_type == "RANK":       
        repeat = repeat
        def get_activation(name, transpose=False):
            def hook(model, input, output):
                if not transpose:
                    activation[name] = output.detach()
                else:
                    activation[name] = input[0].detach()
            return hook          
        for name, module in model.named_modules():
            if isinstance(module, QuantModule):
                if module.org_module_type in MODELS_TO_PRUNE:
                    if name != last_module_name and name != first_module_name:
                        print("NAME OF LAYER: ", name, "TYPE OF LAYER: ", type(module))
                        activation = {}
                        hook = module.register_forward_hook(get_activation('name', transpose=False))
                        feat_map = []                                                # activations of one layer for all the batches
                        with torch.no_grad():
                            trainloader_iterator = iter(trainloader)
                            for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                                try:
                                    imgs = next(trainloader_iterator)
                                except StopIteration:
                                    trainloader_iterator = iter(trainloader)
                                    imgs = next(trainloader_iterator) 
                                imgs = imgs.to(device)
                                model(imgs)
                                feat_map.append(activation['name'])
                        hook.remove()    
                        if prune_type == "CHIP":   
                            if module.org_module_type=="ConvTranspose2d":
                                CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                                # CHIP.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)
                            else:
                                CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                                # CHIP.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0) 
                        elif prune_type == "RANK":   
                            if module.org_module_type=="ConvTranspose2d":
                                RANK.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                                # RANK.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)
                            else:
                                RANK.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                                # RANK.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)    
                                    
                        ####################### input activations ######################### 
                        if prune_channels:       
                            activation = {}
                            hook = module.register_forward_hook(get_activation('name', transpose=True))
                            feat_map = []                                                # activations of one layer for all the batches
                            with torch.no_grad():
                                trainloader_iterator = iter(trainloader)
                                for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                                    try:
                                        imgs = next(trainloader_iterator)
                                    except StopIteration:
                                        trainloader_iterator = iter(trainloader)
                                        imgs = next(trainloader_iterator) 
                                    imgs = imgs.to(device)
                                    model(imgs)
                                    feat_map.append(activation['name'])
                            hook.remove()    
                            if prune_type == "CHIP":   
                                if module.org_module_type=="ConvTranspose2d":
                                    CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                                else:
                                    CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                                # prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                            elif prune_type == "RANK":   
                                if module.org_module_type=="ConvTranspose2d":
                                    CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                                else:
                                    CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                                # prune.l1_unstructured(module, name='bias', amount=prune_ratio)      
            
            elif isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
                if name != last_module_name and name != first_module_name:
                    print("NAME OF LAYER: ", name, "TYPE OF LAYER: ", type(module))
                    activation = {}
                    hook = module.register_forward_hook(get_activation('name', transpose=False))
                    feat_map = []                                                # activations of one layer for all the batches
                    with torch.no_grad():
                        trainloader_iterator = iter(trainloader)
                        for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                            try:
                                imgs = next(trainloader_iterator)
                            except StopIteration:
                                trainloader_iterator = iter(trainloader)
                                imgs = next(trainloader_iterator) 
                            imgs = imgs.to(device)
                            model(imgs)
                            feat_map.append(activation['name'])
                    hook.remove()
                    if prune_type == "CHIP":   
                        if isinstance(module, nn.ConvTranspose2d):
                            CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                            # CHIP.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)
                        else:
                            CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                            # CHIP.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0) 
                    elif prune_type == "RANK":   
                        if isinstance(module, nn.ConvTranspose2d):
                            RANK.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                            # RANK.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)
                        else:
                            RANK.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                            # RANK.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)   
                    
                    ####################### input activations #########################  
                    if prune_channels:      
                        activation = {}
                        hook = module.register_forward_hook(get_activation('name', transpose=True))
                        feat_map = []                                                # activations of one layer for all the batches
                        with torch.no_grad():
                            trainloader_iterator = iter(trainloader)
                            for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                                try:
                                    imgs = next(trainloader_iterator)
                                except StopIteration:
                                    trainloader_iterator = iter(trainloader)
                                    imgs = next(trainloader_iterator) 
                                imgs = imgs.to(device)
                                model(imgs)
                                feat_map.append(activation['name'])
                        hook.remove()    
                        if prune_type == "CHIP":   
                            if isinstance(module, nn.ConvTranspose2d):
                                CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                            else:
                                CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                            # prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                        elif prune_type == "RANK":   
                            if isinstance(module, nn.ConvTranspose2d):
                                CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                            else:
                                CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                            # prune.l1_unstructured(module, name='bias', amount=prune_ratio)                  
                                    
    else:
        for name, module in model.named_modules():
            if isinstance(module, QuantModule):
                if module.org_module_type in MODELS_TO_PRUNE: 
                    if name != last_module_name and name != first_module_name:
                        if prune_type == "structured":
                            prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=0)
                            if prune_channels: 
                                prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=1)
                            #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                        elif prune_type == "random_structured":
                            prune.random_structured(module, name='weight', amount=prune_ratio, dim=0)
                            #prune.random_unstructured(module, name='bias', amount=prune_ratio)
                            if prune_channels: 
                                prune.random_structured(module, name='weight', amount=prune_ratio, dim=1)
                        else:
                            prune.l1_unstructured(module, name='weight', amount=prune_ratio)
                            #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
            elif isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
                    if name != last_module_name and name != first_module_name:
                        if prune_type == "structured":
                            prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=0)
                            if prune_channels: 
                                prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=1)
                            #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                        elif prune_type == "random_structured":
                            prune.random_structured(module, name='weight', amount=prune_ratio, dim=0)
                            #prune.random_unstructured(module, name='bias', amount=prune_ratio)
                            if prune_channels: 
                                prune.random_structured(module, name='weight', amount=prune_ratio, dim=1)
                        else:
                            prune.l1_unstructured(module, name='weight', amount=prune_ratio)
                            #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
    
    return first_module_name, last_module_name


def prune_remove(model: nn.Module, last_module_name: str, first_module_name: str):
    for name, module in model.named_modules():
        if isinstance(module, QuantModule):
            if module.org_module_type in MODELS_TO_PRUNE:
                if name != last_module_name and name != first_module_name:
                    prune.remove(module, 'weight')
                    #prune.remove(module, 'bias')
        elif isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
            if name != last_module_name and name != first_module_name:
                prune.remove(module, 'weight')
                #prune.remove(module, 'bias')


def prune_module(model: nn.Module, module_to_prune: str, prune_type: str, prune_ratio: int, repeat: int, trainloader: torch.utils.data.DataLoader, device: torch.device, prune_channels=False):
    for name, module in model.named_modules():
        if name == module_to_prune:
            if prune_type == "CHIP" or prune_type == "RANK":
                repeat = repeat
                def get_activation(name, transpose=False):
                    def hook(model, input, output):
                        if not transpose:
                            activation[name] = output.detach()
                        else:
                            activation[name] = input[0].detach()
                    return hook          
                # if isinstance(module, QuantModule):
                #     if module.org_module_type in MODELS_TO_PRUNE:
                # print("NAME OF LAYER: ", name, "TYPE OF LAYER: ", type(module))
                activation = {}
                hook = module.register_forward_hook(get_activation('name', transpose=False))
                feat_map = []                                                # activations of one layer for all the batches
                with torch.no_grad():
                    trainloader_iterator = iter(trainloader)
                    # for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                    for i in range(repeat):
                        try:
                            imgs = next(trainloader_iterator)
                        except StopIteration:
                            trainloader_iterator = iter(trainloader)
                            imgs = next(trainloader_iterator) 
                        imgs = imgs.to(device)
                        model(imgs)
                        feat_map.append(activation['name'])
                hook.remove()    
                if prune_type == "CHIP":   
                    # if module.org_module_type=="ConvTranspose2d":
                    if getattr(module, 'org_module_type', 0)=="ConvTranspose2d" or isinstance(module, nn.ConvTranspose2d):
                    # if isinstance(module, nn.ConvTranspose2d):
                        CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                        # CHIP.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)
                    else:
                        CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                        # CHIP.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0) 
                elif prune_type == "RANK":   
                    # if module.org_module_type=="ConvTranspose2d":
                    if getattr(module, 'org_module_type', 0)=="ConvTranspose2d" or isinstance(module, nn.ConvTranspose2d):
                    # if isinstance(module, nn.ConvTranspose2d):
                        RANK.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                        # RANK.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)
                    else:
                        RANK.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                        # RANK.apply(module, name='bias', amount=prune_ratio, feat_map=feat_map, dim=0)   
                                            
                ####################### input activations #########################   \
                if prune_channels:     
                    activation = {}
                    hook = module.register_forward_hook(get_activation('name', transpose=True))
                    feat_map = []                                                # activations of one layer for all the batches
                    with torch.no_grad():
                        trainloader_iterator = iter(trainloader)
                        # for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                        for i in range(repeat):
                            try:
                                imgs = next(trainloader_iterator)
                            except StopIteration:
                                trainloader_iterator = iter(trainloader)
                                imgs = next(trainloader_iterator) 
                            imgs = imgs.to(device)
                            model(imgs)
                            feat_map.append(activation['name'])
                    hook.remove()    
                    if prune_type == "CHIP":   
                        # if module.org_module_type=="ConvTranspose2d":
                        if getattr(module, 'org_module_type', 0)=="ConvTranspose2d" or isinstance(module, nn.ConvTranspose2d):
                        # if isinstance(module, nn.ConvTranspose2d):
                            CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                        else:
                            CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                        # prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                    elif prune_type == "RANK":   
                        # if module.org_module_type=="ConvTranspose2d":
                        if getattr(module, 'org_module_type', 0)=="ConvTranspose2d" or isinstance(module, nn.ConvTranspose2d):
                        # if isinstance(module, nn.ConvTranspose2d):
                            CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=0)
                        else:
                            CHIP.apply(module, name='weight', amount=prune_ratio, feat_map=feat_map, dim=1)
                        # prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                    
            else:
                # if isinstance(module, QuantModule):
                #     if module.org_module_type in MODELS_TO_PRUNE: 
                        # if name != last_module_name and name != first_module_name:
                if prune_type == "structured":
                    prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=0)
                    if prune_channels:
                        prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=1)
                    #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                elif prune_type == "random_structured":
                    prune.random_structured(module, name='weight', amount=prune_ratio, dim=0)
                    if prune_channels:
                        prune.random_structured(module, name='weight', amount=prune_ratio, dim=1)
                    #prune.random_unstructured(module, name='bias', amount=prune_ratio)
                else:
                    prune.l1_unstructured(module, name='weight', amount=prune_ratio)
                    #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                # elif isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
                #     # if name != last_module_name and name != first_module_name:
                #     if prune_type == "structured":
                #         prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=0)
                #         if prune_channels:
                #             prune.ln_structured(module, name='weight', amount=prune_ratio, n=2, dim=1)
                #         #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
                #     elif prune_type == "random_structured":
                #         prune.random_structured(module, name='weight', amount=prune_ratio, dim=0)
                #         if prune_channels:
                #             prune.random_structured(module, name='weight', amount=prune_ratio, dim=1)
                #         #prune.random_unstructured(module, name='bias', amount=prune_ratio)
                #     else:
                #         prune.l1_unstructured(module, name='weight', amount=prune_ratio)
                #         #prune.l1_unstructured(module, name='bias', amount=prune_ratio)
    
    
    # first_module_name = None
    # for name, module in model.named_modules():
    #     if isinstance(module, QuantModule):
    #         if module.org_module_type in MODELS_TO_PRUNE:
    #             if 'g_s' in name:
    #                 last_module_name = name
    #         if module.org_module_type == "Conv2d" and first_module_name == None:
    #             first_module_name = name
    #     elif isinstance(module, nn.Conv2d) or isinstance(module, nn.ConvTranspose2d) or isinstance(module, nn.Linear):
    #         if 'g_s' in name:
    #             last_module_name = name
    #         if isinstance(module, nn.Conv2d) and first_module_name == None:
    #             first_module_name = name
    # print("Last module is:  ", last_module_name)
    
    # return first_module_name, last_module_name
                   