import numpy as np
import torch
from torch.nn.utils import prune
import os
from tqdm import tqdm
import time


def reduced_1_row_norm(input, row_index, data_index):
    input[data_index, row_index, :] = torch.zeros(input.shape[-1])
    m = torch.norm(input[data_index, :, :], p = 'nuc').item()
    return m

def ci_score(conv_output):
    # conv_output = torch.tensor(np.round(np.load(path_conv), 4))
    conv_reshape = conv_output.reshape(conv_output.shape[0], conv_output.shape[1], -1)
    # print(conv_reshape.shape)

    r1_norm = torch.zeros([conv_reshape.shape[0], conv_reshape.shape[1]])
    # for i in tqdm(range(conv_reshape.shape[0]), desc="Calculating CI Score"):
    for i in range(conv_reshape.shape[0]):
        for j in range(conv_reshape.shape[1]):
            r1_norm[i, j] = reduced_1_row_norm(conv_reshape.clone(), j, data_index = i)

    ci = np.zeros_like(r1_norm)
    for i in range(r1_norm.shape[0]):
        # original_norm = torch.norm(torch.tensor(conv_reshape[i, :, :]), p='nuc').item()
        original_norm = torch.norm(conv_reshape[i, :, :].clone().detach(), p='nuc').item()
        ci[i] = original_norm - r1_norm[i]

    # return shape: [batch_size, filter_number]
    return ci

def mean_repeat_ci(feat_maps):
    layer_ci = []
    for feat_map in tqdm(feat_maps, desc="Calculating CI Score"):
        batch_ci = ci_score(feat_map)                   # [batch_size, num_filters]
        # print(batch_ci)
        # print(type(batch_ci))
        batch_ci_mean = np.mean(batch_ci, axis=0)       # [1, num_filters]
        layer_ci.append(batch_ci_mean)
    layer_ci_mean = np.mean(layer_ci, axis=0)           # [1, num_filters]
    
    return layer_ci_mean

def rank_score(conv_output):
    if conv_output.dim() <= 2:
        ranks = torch.abs(conv_output)
    else:      
        batch_size = conv_output.shape[0]
        num_filters = conv_output.shape[1]
        ranks = torch.zeros([batch_size, num_filters])                      # [batch_size, num_filters]
        for i in range(batch_size):
            for j in range(num_filters):
                ranks[i, j] = torch.linalg.matrix_rank(conv_output[i,j,:,:]).item()
    
    return ranks
            
def mean_repeat_rank(feat_maps):
    layer_rank = []
    for feat_map in tqdm(feat_maps, desc="Calculating RANK Score"):
        batch_rank = rank_score(feat_map)                   # [batch_size, num_filters]
        # print(batch_rank)
        # print(batch_rank.shape, type(batch_rank))
        batch_rank_mean = torch.mean(batch_rank, axis=0).reshape(1, -1)       # [1, num_filters]
        # print(batch_rank_mean.shape)
        layer_rank.append(batch_rank_mean)
    # print(torch.cat(layer_rank, dim=0).shape)
    layer_rank_mean = torch.mean(torch.cat(layer_rank, dim=0), axis=0)           # [1, num_filters]
    
    return layer_rank_mean
        

class CHIP(prune.BasePruningMethod):
    """Prune every other entry in a tensor
    """
    PRUNING_TYPE = 'structured'
    
    def __init__(self, amount, feat_map, dim=0):
        """
        Args:
            amount (_type_): _description_
            module (_type_): _description_
            input (torch.tensor): input tensor to the layer module of shape [batch_size, in_channels, w1, h1]
            n (int, optional): _description_. Defaults to 2.
            dim (int, optional): _description_. Defaults to 0.
        """
        self.amount = amount
        self.dim = dim
        self.feat_map = feat_map           # [batch_size, num_filters, w2, h2] / list of elements of [batch_size, num_filters, w2, h2]
        # print("Starting CI calculation.......")
        # batch_ci = ci_score(self.feat_map)                      # [batch_size, num_filters]
        # self.ci_mean = mean_repeat_ci(self.feat_map)
        # print("End of CI calculation.......")
        # self.ci_mean = np.mean(batch_ci, axis=0).reshape(-1)                # [1, num_filters]     

    def compute_mask(self, t, default_mask):
        mask = default_mask.clone(memory_format=torch.contiguous_format)                             # [num_filters, in_channels, k, k]
        tensor_size = t.shape[self.dim]
        nparams_toprune = int(tensor_size * self.amount)
        # print("SIZE", tensor_size)
        # print("PRUNE", nparams_toprune)
        nparams_tokeep = tensor_size - nparams_toprune
        # print("KEEP", nparams_tokeep)
        # print(torch.from_numpy(self.ci_mean).shape)
        ci_mean = mean_repeat_ci(self.feat_map)
        topk = torch.topk(torch.from_numpy(ci_mean), k=nparams_tokeep, largest=True)         # [1, num_filters]
        self.feat_map = 0
        del self.feat_map
        
        def make_mask(t, dim, indices):
            # init mask to 0
            mask = torch.zeros_like(t)
            # e.g.: slc = [None, None, None], if len(t.shape) = 3
            slc = [slice(None)] * len(t.shape)
            # replace a None at position=dim with indices
            # e.g.: slc = [None, None, [0, 2, 3]] if dim=2 & indices=[0,2,3]
            slc[dim] = indices
            # use slc to slice mask and replace all its entries with 1s
            # e.g.: mask[:, :, [0, 2, 3]] = 1
            mask[slc] = 1
            return mask

        if t.dim() <= 1:
            mask.view(-1)[topk.indices] = 0
        else:
            mask = make_mask(t, self.dim, topk.indices)
            # print("INDICES: ", topk.indices)
            # print("MASK:    ", mask[0])
            # print("MASK:    ", mask[13])
            # print("MASK:    ", mask[27])
            mask *= default_mask.to(dtype=mask.dtype)

        return mask
    

class RANK(prune.BasePruningMethod):
    """Prune every other entry in a tensor
    """
    PRUNING_TYPE = 'structured'
    
    def __init__(self, amount, feat_map, dim=0):
        """
        Args:
            amount (_type_): _description_
            module (_type_): _description_
            input (torch.tensor): input tensor to the layer module of shape [batch_size, in_channels, w1, h1]
            n (int, optional): _description_. Defaults to 2.
            dim (int, optional): _description_. Defaults to 0.
        """
        self.amount = amount
        self.dim = dim
        self.feat_map = feat_map           # [batch_size, num_filters, w2, h2] / list of elements of [batch_size, num_filters, w2, h2]
        # print("Starting CI calculation.......")
        # batch_ci = ci_score(self.feat_map)                      # [batch_size, num_filters]
        # self.ci_mean = mean_repeat_ci(self.feat_map)
        # print("End of CI calculation.......")
        # self.ci_mean = np.mean(batch_ci, axis=0).reshape(-1)                # [1, num_filters]     

    def compute_mask(self, t, default_mask):
        mask = default_mask.clone(memory_format=torch.contiguous_format)                             # [num_filters, in_channels, k, k]
        tensor_size = t.shape[self.dim]
        nparams_toprune = int(tensor_size * self.amount)
        # print("SIZE", tensor_size)
        # print("PRUNE", nparams_toprune)
        nparams_tokeep = tensor_size - nparams_toprune
        # print("KEEP", nparams_tokeep)
        # print(torch.from_numpy(self.ci_mean).shape)
        rank_mean = mean_repeat_rank(self.feat_map)
        topk = torch.topk(rank_mean, k=nparams_tokeep, largest=True)         # [1, num_filters]
        self.feat_map = 0
        del self.feat_map
        
        def make_mask(t, dim, indices):
            # init mask to 0
            mask = torch.zeros_like(t)
            # e.g.: slc = [None, None, None], if len(t.shape) = 3
            slc = [slice(None)] * len(t.shape)
            # replace a None at position=dim with indices
            # e.g.: slc = [None, None, [0, 2, 3]] if dim=2 & indices=[0,2,3]
            slc[dim] = indices
            # use slc to slice mask and replace all its entries with 1s
            # e.g.: mask[:, :, [0, 2, 3]] = 1
            mask[slc] = 1
            return mask

        if t.dim() <= 1:
            mask.view(-1)[topk.indices] = 0
        else:
            mask = make_mask(t, self.dim, topk.indices)
            # print("INDICES: ", topk.indices)
            # print("MASK:    ", mask[0])
            # print("MASK:    ", mask[13])
            # print("MASK:    ", mask[27])
            mask *= default_mask.to(dtype=mask.dtype)

        return mask
    
        
        

