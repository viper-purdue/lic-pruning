from pathlib import Path
from tqdm import tqdm
from collections import defaultdict, OrderedDict
import os
import json
import time
import math
import numpy as np
import argparse
import torch
import torch.cuda.amp as amp
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.nn.utils import prune
import compressai

import torchvision as tv
import timm
import timm.utils

# from train import evaluate_model
from models.vae import FactorizedPrior, ScaleHyperprior
from models.registry import get_model
from nni.compression.pytorch.utils import count_flops_params
import copy

import platform
import torch.version
import torch.profiler as tp
torch.set_grad_enabled(False)
from channel_independence import CHIP
from torch.quantization import QuantStub, DeQuantStub, prepare, convert, fuse_modules


class QuantizedModel(torch.nn.Module):
    def __init__(self, model_fp32):
        super(QuantizedModel, self).__init__()
        # QuantStub converts tensors from floating point to quantized.
        # This will only be used for inputs.
        self.quant = QuantStub()
        # DeQuantStub converts tensors from quantized to floating point.
        # This will only be used for outputs.
        self.dequant = DeQuantStub()
        # FP32 model
        self.model_fp32 = model_fp32

    def forward(self, x):
        # manually specify where tensors will be converted from floating
        # point to quantized in the quantized model
        x = self.quant(x)
        x = self.model_fp32(x)
        # manually specify where tensors will be converted from quantized
        # to floating point in the quantized model
        x = self.dequant(x)
        return x
    
    @torch.no_grad()
    def compress(self, x, bpp_lmb):
        x = self.quant(x)
        bpp_lmb = self.quant(bpp_lmb)
        x = self.model_fp32.compress(x, bpp_lmb)
        x = self.dequant(x)
        
        return x

    @torch.no_grad()
    def decompress(self, compressed_obj, bpp_lmb):
        x = self.quant(compressed_obj)
        bpp_lmb = self.quant(bpp_lmb)
        x = self.model_fp32.decompress(x, bpp_lmb)
        x = self.dequant(x)

        return x
    
    
def model_equivalence(model_1, model_2, device, rtol=1e-05, atol=1e-08, num_tests=100, input_size=(1,3,32,32)):

    model_1.to(device)
    model_2.to(device)

    for _ in range(num_tests):
        x = torch.rand(size=input_size).to(device)
        # y1 = model_1(x)['x_hat'].detach().cpu().numpy()
        # y2 = model_2(x)['x_hat'].detach().cpu().numpy()
        lmb = torch.tensor([1e-1]).expand(1).view(-1, 1).to(device)
        y1 = model_1.evaluate(x, lmb)['x_hat'].detach().cpu().numpy()
        y2 = model_2.evaluate(x, lmb)['x_hat'].detach().cpu().numpy()
        if np.allclose(a=y1, b=y2, rtol=rtol, atol=atol, equal_nan=False) == False:
            print("Model equivalence test sample failed: ")
            print(y1)
            print(y2)
            return False

    return True
    

def estimate_flops(function, inputs):
    with tp.profile(activities=[tp.ProfilerActivity.CPU], with_flops=True) as prof:
        _ = function(*inputs)
    tp_flops = sum([event.flops for event in prof.events()]) / 2
    return tp_flops

defualt_inputs = (torch.randn(1, 3, 256, 256),)

def compress_model(model_name, args):
    device = torch.device('cpu')
    checkpt_name = args.checkpoint
    checkpoint_root = Path(f'checkpoints/{checkpt_name}')
    os.makedirs("results", exist_ok=True)

    checkpoint_path = os.path.join(checkpoint_root, 'last.pt')
    print(checkpoint_path)
    
    model = get_model(model_name)()
    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint['model'])
    model = model.to(device=device)
    model.eval()
    
    
    ##############FLOPs######################## 
    # flops, params, results = count_flops_params(model, (1, 3, 256, 256))
    # print(f'FLOPs: {flops/1e9:.3f}G,  Params: {params/1e6:.3f}M')
    
    # from thop import profile, clever_format
    # input = torch.randn(1, 3, 256, 256)
    # macs, params = profile(model, inputs=(input, ))
    # macs, params = clever_format([macs, params], "%.3f")
    # # print(f'FLOPs: {macs/1e6:.3f}M,  Params: {params/1e6:.3f}M')
    # print(f'FLOPs: {macs},  Params: {params}')
    
    from ptflops import get_model_complexity_info
    with torch.cuda.device(0):
        macs, params = get_model_complexity_info(model, (3, 256, 256), as_strings=True,
                                                print_per_layer_stat=False, verbose=False)
        print('{:<30}  {:<8}'.format('Computational complexity: ', macs))
        print('{:<30}  {:<8}'.format('Number of parameters: ', params))

    # nflops = estimate_flops(model.forward, defualt_inputs)
    # nparams = sum([p.numel() for p in model.parameters() if p.requires_grad])
    # print(f'==== ResNet-50: {nflops/1e6:.3f}M FLOPS, {nparams/1e6:.2f}M params ====')
    
    print(sum(p.numel() for p in model.parameters())/1e6)
    ##############FLOPs######################## 
    
    if args.prune_model:
    # ======================== model pruning ========================
        start_time = time.time()
        if args.prune_type == "CHIP":
            device = torch.device('cuda', 1)
            model = model.to(device=device)        
            repeat = args.repeat
            from utils import get_trainloader
            train_dir = os.path.join(Path('/data/home/hossai34/datasets/coco/images'), 'train2017')
            trainloader = get_trainloader(root_dir=train_dir, img_size=args.img_size, batch_size=args.batch_size, workers=4, distributed=False, CIFAR=False)
            def get_activation(name, transpose=False):
                    def hook(model, input, output):
                        if not transpose:
                            activation[name] = output.detach()
                        else:
                            # print(input[0].shape)
                            activation[name] = input[0].detach()
                    return hook          
            for name, module in model.named_modules():
                # if name != "g_s.conv3":
                    # if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.ConvTranspose2d) or isinstance(module, torch.nn.Linear):
                    #     # print(name)
                    #     activation = {}
                    #     hook = module.register_forward_hook(get_activation('conv'))
                    #     feat_map = []                                                # activations of one layer for all the batches
                    #     with torch.no_grad():
                    #         trainloader_iterator = iter(trainloader)
                    #         for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                    #             try:
                    #                 imgs = next(trainloader_iterator)
                    #             except StopIteration:
                    #                 trainloader_iterator = iter(trainloader)
                    #                 imgs = next(trainloader_iterator)
                    #             imgs = imgs.to(device)
                    #             model(imgs)
                    #             feat_map.append(activation['conv'])                   
                    #     # print(len(feat_map))
                    #     # print(type(feat_map[0]))
                    #     # print(feat_map[0].shape)
                    #     feat_map = torch.cat(feat_map, dim=0)
                    #     hook.remove()
                    #     # print(type(feat_map))
                    #     # print(feat_map.shape)                
                    #     if isinstance(module, torch.nn.ConvTranspose2d):
                    #         CHIP.apply(module, name='weight', amount=args.prune_ratio, feat_map=feat_map, dim=1)
                    #         CHIP.apply(module, name='bias', amount=args.prune_ratio, feat_map=feat_map, dim=0)
                    #     else:
                    #         CHIP.apply(module, name='weight', amount=args.prune_ratio, feat_map=feat_map, dim=0)
                    #         CHIP.apply(module, name='bias', amount=args.prune_ratio, feat_map=feat_map, dim=0)     
                if name != "g_s.conv3":
                    print("NAME OF LAYER: ", name, "TYPE OF LAYER: ", type(module))
                    activation = {}
                    if isinstance(module, torch.nn.ConvTranspose2d):
                        TRANSPOSE = False
                    else:
                        TRANSPOSE = False
                    hook = module.register_forward_hook(get_activation('name', transpose=TRANSPOSE))
                    feat_map = []                                                # activations of one layer for all the batches
                    with torch.no_grad():
                        trainloader_iterator = iter(trainloader)
                        for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                            try:
                                imgs = next(trainloader_iterator)
                            except StopIteration:
                                trainloader_iterator = iter(trainloader)
                                imgs = next(trainloader_iterator) 
                            imgs = imgs.to(device)
                            
                            ############## average feature maps ##################
                            lmb_list = [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
                            feat_list = []
                            for lmb in lmb_list:
                                lmb = torch.tensor([lmb]).expand(args.batch_size).view(-1, 1).to(device)
                                model.evaluate(imgs, lmb)
                                feat_list.append(activation['name'])
                            feat = torch.mean(torch.stack(feat_list), dim=0)
                            
                            feat_map.append(feat)
                                    
                                # for batch_idx, imgs in enumerate(tqdm(trainloader, desc=f"Computing the feature maps of Layer: {name}")):
                                #     if batch_idx >= repeat:
                                #         break  
                                #     imgs = imgs.to(self.device)
                                #     lmb = torch.tensor([1e-1]).expand(cfg.prune_batch).view(-1, 1).to(self.device)
                                #     model.evaluate(imgs, lmb)
                                #     feat_map.append(activation['name'])               
                            # feat_map = torch.cat(feat_map, dim=0)
                            hook.remove()    
                            if cfg.prune_type == "CHIP":   
                                # CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                # if not isinstance(module, torch.nn.ConvTranspose2d):
                                #     CHIP.apply(module, name='bias', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                # else:
                                #     prune.l1_unstructured(module, name='bias', amount=cfg.prune_ratio)
            
                                if isinstance(module, torch.nn.ConvTranspose2d):
                                    CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=1)
                                    CHIP.apply(module, name='bias', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                else:
                                    CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                    CHIP.apply(module, name='bias', amount=cfg.prune_ratio, feat_map=feat_map, dim=0) 
                            elif cfg.prune_type == "RANK":   
                                # CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                # if not isinstance(module, torch.nn.ConvTranspose2d):
                                #     CHIP.apply(module, name='bias', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                # else:
                                #     prune.l1_unstructured(module, name='bias', amount=cfg.prune_ratio)
            
                                if isinstance(module, torch.nn.ConvTranspose2d):
                                    RANK.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=1)
                                    RANK.apply(module, name='bias', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                else:
                                    RANK.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                                    RANK.apply(module, name='bias', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)   
                                      
                    ######################## input activations #########################        
                    activation = {}
                    if isinstance(module, torch.nn.ConvTranspose2d):
                        TRANSPOSE = True
                    else:
                        TRANSPOSE = True
                    hook = module.register_forward_hook(get_activation('name', transpose=TRANSPOSE))
                    feat_map = []                                                # activations of one layer for all the batches
                    with torch.no_grad():
                        trainloader_iterator = iter(trainloader)
                        for i in tqdm(range(repeat), desc=f"Computing the feature maps of Layer: {name}"):
                            try:
                                imgs = next(trainloader_iterator)
                            except StopIteration:
                                trainloader_iterator = iter(trainloader)
                                imgs = next(trainloader_iterator) 
                            imgs = imgs.to(self.device)
                            # lmb = torch.tensor([1e-1]).expand(cfg.prune_batch).view(-1, 1).to(self.device)
                            # lmb_min = torch.tensor([1e-6]).expand(cfg.prune_batch).view(-1, 1).to(self.device)
                            # lmb_max = torch.tensor([10.24]).expand(cfg.prune_batch).view(-1, 1).to(self.device)
                            # # model.evaluate(imgs, lmb)
                            # model.evaluate(imgs, lmb_min)
                            # feat_min = activation['name']
                            # model.evaluate(imgs, lmb_max)
                            # feat_max = activation['name']
                            # # feat = torch.div((feat_min + feat_max), 2.0)
                            # feat = torch.mean(torch.stack([feat_min, feat_max]), dim=0)
                            # # feat_map.append(activation['name'])  
                            
                            lmb_list = [1e-6, 1e-3, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56, 5.12, 10.24]
                            feat_list = []
                            for lmb in lmb_list:
                                lmb = torch.tensor([lmb]).expand(args.batch_size).view(-1, 1).to(device)
                                model.evaluate(imgs, lmb)
                                feat_list.append(activation['name'])
                            feat = torch.mean(torch.stack(feat_list), dim=0)
                            
                            feat_map.append(feat)
                        # for batch_idx, imgs in enumerate(tqdm(trainloader, desc=f"Computing the feature maps of Layer: {name}")):
                        #     if batch_idx >= repeat:
                        #         break  
                        #     imgs = imgs.to(self.device)
                        #     lmb = torch.tensor([1e-1]).expand(cfg.prune_batch).view(-1, 1).to(self.device)
                        #     model.evaluate(imgs, lmb)
                        #     feat_map.append(activation['name'])               
                    # feat_map = torch.cat(feat_map, dim=0)
                    hook.remove()    
                    if cfg.prune_type == "CHIP":   
                        if isinstance(module, torch.nn.ConvTranspose2d):
                            CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                        else:
                            CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=1)
                        prune.l1_unstructured(module, name='bias', amount=cfg.prune_ratio)
                    elif cfg.prune_type == "RANK":   
                        if isinstance(module, torch.nn.ConvTranspose2d):
                            CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=0)
                        else:
                            CHIP.apply(module, name='weight', amount=cfg.prune_ratio, feat_map=feat_map, dim=1)
                        prune.l1_unstructured(module, name='bias', amount=cfg.prune_ratio)
            
        else:
            for name, module in model.named_modules():
                ###########debug########################
                print("1:   ", name, "  ", type(module))
                for n, m in module.named_parameters():
                    print("##########   ", n, "     parameters: ", m.numel())
                ###########debug########################

                # if args.prune_type == "CHIP":
                #     chip_pruner = CHIP(amount=args.prune_ratio, module, input, n=2, dim=0)
                        
                if isinstance(module, torch.nn.Conv2d): 
                    if args.prune_type == "structured":
                        prune.ln_structured(module, name='weight', amount=args.prune_ratio, n=2, dim=0)
                        # prune.ln_structured(module, name='weight', amount=args.prune_ratio, n=2, dim=1)
                        # chip_pruner = CHIP(amount=args.prune_ratio, module, input, n=2, dim=0)
                        # CHIP.apply()
                    else:
                        prune.l1_unstructured(module, name='weight', amount=args.prune_ratio)
                    prune.l1_unstructured(module, name='bias', amount=args.prune_ratio)
                elif isinstance(module, torch.nn.ConvTranspose2d):
                    if args.prune_type == "structured":
                        prune.ln_structured(module, name='weight', amount=args.prune_ratio, n=2, dim=0)
                        # prune.ln_structured(module, name='weight', amount=args.prune_ratio, n=2, dim=1)
                    else:
                        prune.l1_unstructured(module, name='weight', amount=args.prune_ratio)
                    prune.l1_unstructured(module, name='bias', amount=args.prune_ratio)
                elif isinstance(module, torch.nn.Linear):
                    if args.prune_type == "structured":
                        prune.ln_structured(module, name='weight', amount=args.prune_ratio, n=2, dim=0)
                        # prune.ln_structured(module, name='weight', amount=args.prune_ratio, n=2, dim=1)
                    else:
                        prune.l1_unstructured(module, name='weight', amount=args.prune_ratio)
                    prune.l1_unstructured(module, name='bias', amount=args.prune_ratio)
            end_time = time.time()
            print(f"Total time to prune model: {(end_time-start_time)//3600} hours {round((end_time-start_time)/60 - float((end_time-start_time)//3600)*60)} minutes")
                # elif isinstance(module, compressai.layers.gdn.GDN):
                #     if args.prune_type == "structured":
                #         prune.ln_structured(module, name='gamma', amount=args.prune_ratio, n=2, dim=0)
                #     else:
                #         prune.l1_unstructured(module, name='gamma', amount=args.prune_ratio)
                #     prune.l1_unstructured(module, name='beta', amount=args.prune_ratio)
                    # if not args.finetune:
                    #     prune.remove(module, 'weight')
                    # print("three", module.weight.shape)
                # prune 40% of connections in all linear layers
                # elif isinstance(module, torch.nn.Linear):
                #     # prune.l1_unstructured(module, name='weight', amount=0.4)
                #     prune.ln_structured(module, name='weight', amount=0.4, n=2, dim=0)
                #     prune.remove(module, 'weight')
                
        ##############FLOPs########################  
        # flops, params, results = count_flops_params(model, (1, 3, 256, 256))
        # print(f'FLOPs: {flops/1e9:.3f}G,  Params: {params/1e6:.3f}M')
        
        # input = torch.randn(1, 3, 256, 256)
        # macs, params = profile(model, inputs=(input, ))
        # macs, params = clever_format([macs, params], "%.3f")
        # # print(f'FLOPs: {macs/1e6:.3f}M,  Params: {params/1e6:.3f}M')
        # print(f'FLOPs: {macs},  Params: {params}')
            
        for name, module in model.named_modules():
            if name != "g_s.conv3":
                if isinstance(module, torch.nn.Conv2d):
                    prune.remove(module, 'weight')
                    prune.remove(module, 'bias')
                if isinstance(module, torch.nn.ConvTranspose2d):
                    prune.remove(module, 'weight')
                    prune.remove(module, 'bias')
                if isinstance(module, torch.nn.Linear):
                    prune.remove(module, 'weight')
                    prune.remove(module, 'bias')
            # if isinstance(module, compressai.layers.gdn.GDN):
            #     prune.remove(module, 'gamma')
            #     prune.remove(module, 'beta')
        device = torch.device('cpu')
        model = model.to(device=device)
        model.eval()
        with torch.cuda.device(0):
            macs, params = get_model_complexity_info(model, (3, 256, 256), as_strings=True,
                                                    print_per_layer_stat=False, verbose=False)
            print('{:<30}  {:<8}'.format('Computational complexity: ', macs))
            print('{:<30}  {:<8}'.format('Number of parameters: ', params))
        

            
        print("Sparsity in entire model: {:.3f}%".format(100. - 100. * float(sum(torch.count_nonzero(p) for p in model.parameters()))/ float(sum(p.numel() for p in model.parameters()))))
            
        ###########debug########################
        print(float(sum(p.numel() for p in model.entropy_bottleneck.parameters())))
        print(100.* float(sum(p.numel() for p in model.entropy_bottleneck.parameters()) + (128.0+16384)*4.0 + 8.0*128.0)/float(sum(p.numel() for p in model.parameters())))
        print(100.* float(sum(p.numel() for p in model.gaussian_conditional.parameters()))/float(sum(p.numel() for p in model.parameters())))
        ###########debug########################
            
            # print(float(sum(p.numel() for p in model.parameters())))
            # print(float(sum(torch.count_nonzero(p) for p in model.parameters())))
            # for name, p in model.named_parameters():
            #     print(name, p.shape)
            # print("########################")
            # for name, p in model.named_modules():
            #     print(name)
            
            # nflops = estimate_flops(model.forward, defualt_inputs)
            # nparams = sum([p.numel() for p in model.parameters() if p.requires_grad])
            # print(f'==== ResNet-50: {nflops/1e6:.3f}M FLOPS, {nparams/1e6:.2f}M params ====')
            ##############FLOPs######################## 
    if args.quantize_model:
        # ======================== model quantization ========================
        import random
        def set_random_seeds(random_seed=0):
            torch.manual_seed(random_seed)
            torch.backends.cudnn.deterministic = True
            torch.backends.cudnn.benchmark = False
            np.random.seed(random_seed)
            random.seed(random_seed)

        random_seed = 0
        num_classes = 10
        cuda_device = torch.device("cuda:0")
        cpu_device = torch.device("cpu")

        model_dir = "checkpoints"
        model_filename = "Quantized.pt"
        quantized_model_filename = "Quantized.pt"
        model_filepath = os.path.join(model_dir, model_filename)
        quantized_model_filepath = os.path.join(model_dir, quantized_model_filename)
        set_random_seeds(random_seed=random_seed)

        # train_loader, test_loader = prepare_dataloader(num_workers=8, train_batch_size=128, eval_batch_size=256)
        from utils import get_trainloader
        train_dir = os.path.join(Path('/data/home/hossai34/datasets/coco/images'), 'train2017')
        trainloader = get_trainloader(root_dir=train_dir, img_size=args.img_size, batch_size=args.batch_size, workers=4, distributed=False, CIFAR=False)
        from utils import MyCIFAR100Dataset, ImageDataset, get_valloader
        from torch.utils.data import DataLoader
        test_transform = tv.transforms.Compose([tv.transforms.ToTensor()])
        testset = ImageDataset(os.path.join('/data/home/hossai34/datasets', 'kodak'), transform=test_transform) 
        testloader = DataLoader(
        testset, batch_size=1, shuffle=False, num_workers=4,
        pin_memory=True, drop_last=True)

        
        model = model.to(device=cpu_device)
        # Make a copy of the model for layer fusion
        fused_model = copy.deepcopy(model).to(device=cpu_device)

        model.eval()
        # The model has to be switched to evaluation mode before any layer fusion.
        # Otherwise the quantization will not work correctly.
        fused_model.eval()
        
        # for module_name, module in fused_model.named_children():
        #     if ("g_a" in module_name) or ("g_s" in module_name):
        #         for sub_module_name, sub_module in module.named_children():
        #             if "cconv" in sub_module_name:
        #                 # fuse_modules(sub_module, [["conv", "norm"]], inplace=True)
        #                 # fuse_modules(sub_module.fc1, [['0', '1', '2']], inplace=True)
        #                 # fuse_modules(sub_module.fc2, [['0', '1', '2']], inplace=True)
        #                 for child_module_name, child_module in sub_module.named_children():
        #                     # fuse_modules(child_module, [["conv", "norm"]], inplace=True)
        #                     # fuse_modules(child_module, [[fc1[0], "norm"]], inplace=True)
        #                     print(child_module_name, type(child_module))
        #     elif ("h_a" in module_name) or ("h_s" in module_name):
        #         for sub_module_name, sub_module in module.named_children():
        #             if "cconv" in sub_module_name:
        #                 # fuse_modules(sub_module, [["conv", "norm"]], inplace=True)
        #                 # fuse_modules(sub_module.fc1, [['0', '1', '2']], inplace=True)
        #                 # fuse_modules(sub_module.fc2, [['0', '1', '2']], inplace=True)
        #                 for child_module_name, child_module in sub_module.named_children():
        #                     # fuse_modules(child_module, [["conv", "norm"]], inplace=True)
        #                     # fuse_modules(child_module, [[fc1[0], "norm"]], inplace=True)
        #                     print(child_module_name, type(child_module))

        # Fuse the model in place rather manually.
        # fused_model = torch.quantization.fuse_modules(fused_model, [["conv1", "bn1", "relu"]], inplace=True)
        # for module_name, module in fused_model.named_children():
        #     if "layer" in module_name:
        #         for basic_block_name, basic_block in module.named_children():
        #             torch.quantization.fuse_modules(basic_block, [["conv1", "bn1", "relu1"], ["conv2", "bn2"]], inplace=True)
        #             for sub_block_name, sub_block in basic_block.named_children():
        #                 if sub_block_name == "downsample":
        #                     torch.quantization.fuse_modules(sub_block, [["0", "1"]], inplace=True)
        
        # fused_model = fuse_modules(fused_model, [[], [], []], inplace=True)

        # # Print FP32 model.
        # print(model)
        # # Print fused model.
        # print(fused_model)

        # Model and fused model should be equivalent.
        assert model_equivalence(model_1=model, model_2=model, device=cpu_device, rtol=1e-03, atol=1e-06, num_tests=100, input_size=(1,3,32,32)), "Fused model is not equivalent to the original model!"

        # Prepare the model for static quantization. This inserts observers in
        # the model that will observe activation tensors during calibration.
        quantized_model = QuantizedModel(model_fp32=fused_model)
        # Using un-fused model will fail.
        # Because there is no quantized layer implementation for a single batch normalization layer.
        # quantized_model = QuantizedResNet18(model_fp32=model)
        # Select quantization schemes from 
        # https://pytorch.org/docs/stable/quantization-support.html
        quantization_config = torch.quantization.get_default_qconfig("qnnpack")            # fbgemm
        for name, module in quantized_model.named_modules():
            if isinstance(module, compressai.layers.gdn.GDN):
                module.qconfig = None
        # Custom quantization configurations
        # quantization_config = torch.quantization.default_qconfig
        # quantization_config = torch.quantization.QConfig(activation=torch.quantization.MinMaxObserver.with_args(dtype=torch.quint8), weight=torch.quantization.MinMaxObserver.with_args(dtype=torch.qint8, qscheme=torch.per_tensor_symmetric))

        quantized_model.qconfig = quantization_config
        
        # Print quantization configurations
        print(quantized_model.qconfig)

        torch.quantization.prepare(quantized_model, inplace=True)
        
        def calibrate_model(model, loader, device=torch.device("cpu")):
            model.to(device)
            model.eval()

            for inputs in tqdm(loader):
                inputs = inputs.to(device)
                _ = model(inputs)

        # Use training data for calibration.
        calibrate_model(model=quantized_model, loader=testloader, device=cpu_device)

        quantized_model = torch.quantization.convert(quantized_model, inplace=True)

        # Using high-level static quantization wrapper
        # The above steps, including torch.quantization.prepare, calibrate_model, and torch.quantization.convert, are also equivalent to
        # quantized_model = torch.quantization.quantize(model=quantized_model, run_fn=calibrate_model, run_args=[train_loader], mapping=None, inplace=False)

        quantized_model.eval()

        # Print quantized model.
        print(quantized_model)
        
        def save_torchscript_model(model, model_dir, model_filename):

            if not os.path.exists(model_dir):
                os.makedirs(model_dir)
            model_filepath = os.path.join(model_dir, model_filename)
            torch.jit.save(torch.jit.script(model), model_filepath)

        def load_torchscript_model(model_filepath, device):

            model = torch.jit.load(model_filepath, map_location=device)

            return model

        # Save quantized model.
        # save_torchscript_model(model=quantized_model, model_dir=model_dir, model_filename=quantized_model_filename)

        # # Load quantized model.
        # quantized_jit_model = load_torchscript_model(model_filepath=quantized_model_filepath, device=cpu_device)
        
        checkpoint = {'model': quantized_model.state_dict()}
        os.makedirs(f'checkpoints/Quantized', exist_ok=True)
        checkpoint_root = Path(f'checkpoints/Quantized')
        checkpoint_path = os.path.join(checkpoint_root, 'last.pt')
        torch.save(checkpoint, checkpoint_path)

        # _, fp32_eval_accuracy = evaluate_model(model=model, test_loader=test_loader, device=cpu_device, criterion=None)
        # _, int8_eval_accuracy = evaluate_model(model=quantized_jit_model, test_loader=test_loader, device=cpu_device, criterion=None)

        # # Skip this assertion since the values might deviate a lot.
        # # assert model_equivalence(model_1=model, model_2=quantized_jit_model, device=cpu_device, rtol=1e-01, atol=1e-02, num_tests=100, input_size=(1,3,32,32)), "Quantized model deviates from the original model too much!"

        # print("FP32 evaluation accuracy: {:.3f}".format(fp32_eval_accuracy))
        # print("INT8 evaluation accuracy: {:.3f}".format(int8_eval_accuracy))

        # fp32_cpu_inference_latency = measure_inference_latency(model=model, device=cpu_device, input_size=(1,3,32,32), num_samples=100)
        # int8_cpu_inference_latency = measure_inference_latency(model=quantized_model, device=cpu_device, input_size=(1,3,32,32), num_samples=100)
        # int8_jit_cpu_inference_latency = measure_inference_latency(model=quantized_jit_model, device=cpu_device, input_size=(1,3,32,32), num_samples=100)
        # fp32_gpu_inference_latency = measure_inference_latency(model=model, device=cuda_device, input_size=(1,3,32,32), num_samples=100)
        
        # print("FP32 CPU Inference Latency: {:.2f} ms / sample".format(fp32_cpu_inference_latency * 1000))
        # print("FP32 CUDA Inference Latency: {:.2f} ms / sample".format(fp32_gpu_inference_latency * 1000))
        # print("INT8 CPU Inference Latency: {:.2f} ms / sample".format(int8_cpu_inference_latency * 1000))
        # print("INT8 JIT CPU Inference Latency: {:.2f} ms / sample".format(int8_jit_cpu_inference_latency * 1000))
    
    
    
    # checkpoint = {'model': model.state_dict()}
    # if not args.finetune:
    #     os.makedirs(f'checkpoints/{checkpt_name.rsplit("_", 1)[0]}-pruned-{args.prune_type}-{args.prune_ratio}', exist_ok=True)
    #     checkpoint_root = Path(f'checkpoints/{checkpt_name.rsplit("_", 1)[0]}-pruned-{args.prune_type}-{args.prune_ratio}')
    # else:
    #     os.makedirs(f'checkpoints/{checkpt_name.rsplit("_", 1)[0]}-pruned-{args.prune_type}-{args.prune_ratio}-finetuned', exist_ok=True)
    #     checkpoint_root = Path(f'checkpoints/{checkpt_name.rsplit("_", 1)[0]}-pruned-{args.prune_type}-{args.prune_ratio}-finetuned')
    # checkpoint_path = os.path.join(checkpoint_root, 'last.pt')
    # torch.save(checkpoint, checkpoint_path)
    
    ########### Sparsity of all the parameters ########################
    # for name, module in model.named_parameters():
    #     print("1:   ", name, "     parameters: ", module.numel(), "      Sparsity: ", 100. - 100. * float(torch.count_nonzero(module))/float(module.numel()))
        # if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.Linear):
        #     print("2:   ", name)
    ########### Sparsity of all the parameters ########################



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model', type=str, default='scale_hyperprior')                 # ours_autoencoder   'ours_n0', 'ours_n4', 'ours_n8', 'ours_n0_enc', 'ours_n4_enc', 'ours_n8_enc'
    parser.add_argument('-c', '--checkpoint', type=str, default='scale_hyperprior_4_uncompressed')
    parser.add_argument('-r', '--prune_ratio', type=float, default=0.6)
    parser.add_argument('--prune_type', type=str, default="unstructured", choices=['unstructured', 'structured', 'CHIP'])
    parser.add_argument('--finetune', action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--img_size', type=int,  default=256)
    parser.add_argument('-b', '--batch_size', type=int, default=4)
    parser.add_argument('--repeat', type=int, default=10)
    # parser.add_argument('-d', '--data_root',  type=str, default='/data/home/hossai34/datasets/imagenet/val')
    # parser.add_argument('-b', '--batch_size', type=int, default=8)
    # parser.add_argument('-w', '--workers',    type=int, default=1)
    # parser.add_argument('--epochs',    type=int, default=5)
    parser.add_argument('--prune_model',  action=argparse.BooleanOptionalAction, default=False)
    parser.add_argument('--quantize_model',  action=argparse.BooleanOptionalAction, default=False)
    args = parser.parse_args()

    model_name = args.model
    compress_model(model_name, args)


if __name__ == '__main__':
    main()